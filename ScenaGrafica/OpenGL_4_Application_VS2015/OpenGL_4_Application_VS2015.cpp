//
//  main.cpp
//  OpenGL Shadows
//
//  Created by CGIS on 05/12/16.
//  Copyright � 2016 CGIS. All rights reserved.
//

#define GLEW_STATIC

#include <iostream>
#include "glm/glm.hpp"//core glm functionality
#include "glm/gtc/matrix_transform.hpp"//glm extension for generating common transformation matrices
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "GLEW/glew.h"
#include "GLFW/glfw3.h"
#include <string>
#include "Shader.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"
#define TINYOBJLOADER_IMPLEMENTATION



#include "Model3D.hpp"
#include "Mesh.hpp"

int glWindowWidth = 840;
int glWindowHeight = 680;
int retina_width, retina_height;
GLFWwindow* glWindow = NULL;

const GLuint SHADOW_WIDTH = 2048, SHADOW_HEIGHT = 2048;

glm::mat4 model;
GLuint modelLoc;
glm::mat4 view;
GLuint viewLoc;
glm::mat4 projection;
GLuint projectionLoc;
glm::mat3 normalMatrix;
GLuint normalMatrixLoc;
glm::mat3 lightDirMatrix;
GLuint lightDirMatrixLoc;
gps::SkyBox mySkyBox;
gps::Shader skyboxShader;
glm::vec3 lightDir;
GLuint lightDirLoc;
glm::vec3 lightColor;
GLuint lightColorLoc;

float angleCam=0.0f;
GLfloat angleC;

gps::Camera myCamera(glm::vec3(0.0f, 1.0f, 2.5f), glm::vec3(0.0f, 0.0f, 0.0f));
GLfloat cameraSpeed = 0.3f;

bool pressedKeys[1024];
GLfloat angle;

GLfloat lightAngle;

gps::Model3D farm;
gps::Model3D ground;
gps::Model3D lightCube;
gps::Shader myCustomShader;
gps::Shader lightShader;
gps::Shader depthMapShader;


gps::Model3D tree;
gps::Model3D fence;
gps::Model3D fence2;
gps::Model3D mausoleum;
gps::Model3D oldHouse;
gps::Model3D kangoroo;

gps::Model3D well;
gps::Model3D house;
gps::Model3D house2;
gps::Model3D straw;
gps::Model3D scen;
GLuint shadowMapFBO;
GLuint depthMapTexture;
float tr = 0.0f;
int ok = 0;

GLenum glCheckError_(const char *file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR)
	{
		std::string error;
		switch (errorCode)
		{
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)

void windowResizeCallback(GLFWwindow* window, int width, int height)
{
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO
	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	myCustomShader.useShaderProgram();

	//set projection matrix
	glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	//send matrix data to shader
	GLint projLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	lightShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//set Viewport transform
	glViewport(0, 0, retina_width, retina_height);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
			pressedKeys[key] = true;
		else if (action == GLFW_RELEASE)
			pressedKeys[key] = false;
	}
}

GLboolean firstMouse = true;
float lastX = 320, lastY = 340;
float yaw, pitch;
int nr = 0;
int nr2 = 0;
void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.05;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;
	myCamera.rotate(pitch, yaw);
}
void processMovement()
{

	if (pressedKeys[GLFW_KEY_Q]) {
		angleC += 0.6f;
		if (angleC > 360.0f)
			angleC -= 360.0f;
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angleC -= 0.6f;
		if (angleC < 0.0f)
			angleC += 360.0f;
	}
	
	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);
	}
	
	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);
	}
	

	if (pressedKeys[GLFW_KEY_C]) {
		if (nr<2) {
			tr -= 0.05f;
			nr++;
		}
	}

	if (pressedKeys[GLFW_KEY_V]) {
		if (nr2 < 30) {
			tr += 0.05f;
			nr2++;
		}
	}
	if (pressedKeys[GLFW_KEY_P])
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
	}

	if (pressedKeys[GLFW_KEY_B])
	{
		
		if (ok == 0)
			ok = 1;
		else
			ok = 0;

	}


	if (pressedKeys[GLFW_KEY_J]) {

		lightAngle += 0.3f;
		if (lightAngle > 360.0f)
			lightAngle -= 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle -= 0.3f;
		if (lightAngle < 0.0f)
			lightAngle += 360.0f;
		glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
		myCustomShader.useShaderProgram();
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDirTr));
	}

	if (pressedKeys[GLFW_KEY_I]) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (pressedKeys[GLFW_KEY_O])
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
}

bool initOpenGLWindow()
{
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		return false;
	}

	//for Mac OS X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glWindow = glfwCreateWindow(glWindowWidth, glWindowHeight, "OpenGL Shader Example", NULL, NULL);
	if (!glWindow) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
		return false;
	}

	glfwSetWindowSizeCallback(glWindow, windowResizeCallback);
	glfwMakeContextCurrent(glWindow);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte* version = glGetString(GL_VERSION); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	//for RETINA display
	glfwGetFramebufferSize(glWindow, &retina_width, &retina_height);

	glfwSetKeyCallback(glWindow, keyboardCallback);
	glfwSetCursorPosCallback(glWindow, mouseCallback);
	//glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return true;
}

void initOpenGLState()
{
	//glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
	glClearColor(0.5, 0.5, 0.5, 1.0);
	glViewport(0, 0, retina_width, retina_height);
	glfwSetInputMode(glWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
						  //glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initFBOs()
{
	//generate FBO ID
	glGenFramebuffers(1, &shadowMapFBO);

	//create depth texture for FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
		SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//attach texture to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

glm::mat4 computeLightSpaceTrMatrix()
{
	const GLfloat near_plane = 1.0f, far_plane = 10.0f;
	glm::mat4 lightProjection = glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, near_plane, far_plane);

	glm::vec3 lightDirTr = glm::vec3(glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(lightDir, 1.0f));
	glm::mat4 lightView = glm::lookAt(lightDirTr, myCamera.getCameraTarget(), glm::vec3(0.0f, 1.0f, 0.0f));

	return lightProjection * lightView;
}

void initModels()
{
	farm = gps::Model3D("objects/farmHouse/farm.obj", "objects/farmHouse/");
	ground = gps::Model3D("objects/grass/grass.obj", "objects/grass/");
	lightCube = gps::Model3D("objects/cube/cube.obj", "objects/cube/");
	well = gps::Model3D("objects/well/well.obj", "objects/well/");
	straw = gps::Model3D("objects/straw/straw.obj", "objects/straw/");
	tree= gps::Model3D("objects/forest/tree.obj", "objects/forest/");
	fence = gps::Model3D("objects/fence/fence.obj", "objects/fence/");
	fence2 = gps::Model3D("objects/fence/fence2.obj", "objects/fence/");
	kangoroo = gps::Model3D("objects/Marius/Kangaroo/cangur.obj", "objects/Marius/Kangaroo/");
	mausoleum = gps::Model3D("objects/Marius/mausoleum/Mausoleum.obj", "objects/Marius/mausoleum/");

}

void initShaders()
{
	myCustomShader.loadShader("shaders/shaderStart.vert", "shaders/shaderStart.frag");
	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	depthMapShader.loadShader("shaders/simpleDepthMap.vert", "shaders/simpleDepthMap.frag");
}

void initUniforms()
{
	myCustomShader.useShaderProgram();

	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");
	viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");

	normalMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "normalMatrix");

	lightDirMatrixLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDirMatrix");

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myCustomShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(0.0f, 3.0f, 2.0f);
	lightDirLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(lightDir));

	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myCustomShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
}
float delta = 0.0f;

void renderTrees()
{
	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -7.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -6.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -4.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -7.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -6.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -4.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.2f, -1.1f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, -1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, -0.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 0.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 4.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(8.0f, -1.1f, 6.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.5f, -1.1f, 5.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.5f, -1.1f, 5.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.5f, -1.1f, 5.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.5f, -1.1f, 5.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.5f, -1.1f, 5.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -1.1f, 5.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 5.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -7.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -6.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -6.2f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -4.8f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -4.1f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -7.1f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -6.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -5.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -4.3f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -3.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -2.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -1.1f, -2.2f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -7.1f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -6.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -5.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);
	
	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -4.3f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);
	
	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.3f, -1.1f, -3.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.6f, -1.1f, -3.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.7f, -1.1f, -3.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f, -1.1f, -3.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f, -1.1f, -2.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.6f, -1.1f, -2.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.2f, -1.1f, -2.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, -2.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.6f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.3f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.1f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.3f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.6f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.2f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.2f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.8f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.6f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -7.9f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, -3.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.8f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.6f, -1.1f, -0.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.4f, -1.1f, 0.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.2f, -1.1f, 1.2f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 1.8f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	////
	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-7.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.8f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.6f, -1.1f, -1.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.4f, -1.1f, 1.6f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 1.2f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 1.8f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	////////////////

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);


	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, -1.1f, -3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);





	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.8f, -1.1f, 2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);
	
	////////////////////////////////////////////////////////

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.8f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.8f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.8f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.8f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.8f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.7f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.7f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.7f, -1.1f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.7f, -1.1f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.7f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.7f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.9f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);

	//create model matrix for tree
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-6.0f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	tree.Draw(myCustomShader);
}

void gardCasa1()
{
	//create model matrix for fence

	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.8f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.8f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.8f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.2f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.2f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.2f, -1.0f, 5.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	

////////////////////////////////////////////////////////////////////////////////////////////////////////
	//create model matrix for fence

	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.8f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.8f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.8f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.2f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.2f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.2f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.2f, -1.0f, 1.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

/////////////////////////////////////////////////////////////////////////////////////////////////////////

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, 3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.0f, -0.9f, 5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.5f, -0.9f, 5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -0.9f, 5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.8f, -0.9f, 5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.5f, -0.9f, 5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.0f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(4.6f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(5.6f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.0f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.8f, -0.9f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.52f, -1.1f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.52f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, 5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, 3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(7.0f, -1.1f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);
}

void drawFence()
{
	

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.8f, -0.9f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.8f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.8f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.3f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.7f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.1f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.2f, -1.0f, -2.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);


	/////////////////////////////////////////////////////////////////////////////////
	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.6f, -1.0f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);


	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.58f, -1.0f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.58f, -1.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.56f, -1.0f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, 3.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, 4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, 2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, 1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, -1.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, -2.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);
}

void drawFenceBiserica()
{
	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.8f, -0.9f, -3.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.4f, -0.9f, -3.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, -0.9f, -3.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.5f, -0.9f, -3.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, -0.9f, -3.7f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);



	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.8f, -0.9f, -4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.4f, -0.9f, -4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.0f, -0.9f, -4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -0.9f, -4.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.8f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.3f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.6f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.3f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-0.3f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.6f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.2f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.7f, -0.9f, -7.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, -1.1f, -6.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, -1.1f, -6.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, -1.1f, -5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, -1.1f, -5.0f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(3.0f, -1.1f, -4.4f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, -2.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, -3.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, -1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, -0.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 0.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 1.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 2.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 3.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, -1.1f, 3.8f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, -2.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, -1.1f, -3.2f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -4.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -3.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -5.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);

	//create model matrix for fence
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 1));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.5f, -1.1f, -6.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	fence2.Draw(myCustomShader);
}


float beta = -2.3f;


void renderScene()
{	
	
	beta += 0.03f;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	processMovement();

	//render the scene to the depth buffer (first pass)

	depthMapShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	
	//create model matrix for farmHouse
	
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.50f, -1.4f, 0.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	farm.Draw(depthMapShader);

	//create model matrix for farmHouse

	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.5f, -1.4f, 4.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	farm.Draw(depthMapShader);


	model = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.2f, -6.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.009));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	mausoleum.Draw(depthMapShader);


	//create model matrix for well
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.3f, -1.1f, 0.5f));
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	well.Draw(depthMapShader);

	//create model matrix for farmHouse
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.5f, -1.3f, 0.0f));
	model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	farm.Draw(depthMapShader);

	//create model matrix for farmHouse
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -1.4f, 2.5f));
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	farm.Draw(depthMapShader);

	//create model matrix for farmHouse
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.0f, -1.35f, 3.0f));
	model = glm::rotate(model, glm::radians(260.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//send model matrix to shader6
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	farm.Draw(depthMapShader);


//111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111


	//create model matrix for ground

	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));

	//send model matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "model"),
		1,
		GL_FALSE,
		glm::value_ptr(model));
	ground.Draw(depthMapShader);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//render the scene (second pass)

	myCustomShader.useShaderProgram();

	//send lightSpace matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "lightSpaceTrMatrix"),
		1,
		GL_FALSE,
		glm::value_ptr(computeLightSpaceTrMatrix()));

	//send view matrix to shader

	view = myCamera.getViewMatrix();
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angleC), glm::vec3(0, 1, 0));
	glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"),
		1,
		GL_FALSE,
		glm::value_ptr(view*model));

	if (ok == 1) {

		angleCam -= 0.6f;
		if (angleCam < 0.0f)
			angleCam += 360.0f;

		view = myCamera.getViewMatrix();
		model = glm::rotate(glm::mat4(1.0f), glm::radians(angleCam), glm::vec3(0, 1, 0));
		glUniformMatrix4fv(glGetUniformLocation(myCustomShader.shaderProgram, "view"),
			1,
			GL_FALSE,
			glm::value_ptr(view*model));
	}
	//compute light direction transformation matrix
	lightDirMatrix = glm::mat3(glm::inverseTranspose(view));
	//send lightDir matrix data to shader
	glUniformMatrix3fv(lightDirMatrixLoc, 1, GL_FALSE, glm::value_ptr(lightDirMatrix));

	glViewport(0, 0, retina_width, retina_height);
	myCustomShader.useShaderProgram();

	//bind the depth map
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "shadowMap"), 3);

	//11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
	
	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, -1.4f, 2.5f));
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	farm.Draw(myCustomShader);


	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.5f, -1.3f, 0.0f));
	model = glm::rotate(model, glm::radians(270.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	farm.Draw(myCustomShader);
	
	
	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.5f, -1.4f, 0.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	farm.Draw(myCustomShader);

	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.5f, -1.4f, 4.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	farm.Draw(myCustomShader);

	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.2f, -6.0f));
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.009));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	mausoleum.Draw(myCustomShader);

	//create model matrix for farm
	model = glm::translate(glm::mat4(1.0f), glm::vec3(6.0f, -1.35f, 3.0f));
	model = glm::rotate(model, glm::radians(260.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.5));
	//model = glm::translate(model, glm::vec3(delta, 0, 0));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	farm.Draw(myCustomShader);

	
	


//////////////////////////////////////////////////////////////////////////////////////////////////////////	


	if (beta >= 2.7) {
		beta = -2.3f;
	}
	
	//create model matrix for kangoroo
	model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(beta, -1.2f, 0.3f));
	model = glm::scale(model, glm::vec3(0.003));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	kangoroo.Draw(myCustomShader);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	//create model matrix for well
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(1.3f, -1.2f, 0.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	well.Draw(myCustomShader);

	//create model matrix for well
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.3f, -1.2f, 4.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	well.Draw(myCustomShader);

	//create model matrix for well
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-2.3f, -1.2f, 2.5f));
	model = glm::scale(model, glm::vec3(0.3));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	well.Draw(myCustomShader);


	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.5f, -1.0f, -2.0f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);

	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-3.8f, -1.0f, -1.9f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);

	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.1f, -1.0f, -1.8f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);



	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.1f, -1.0f, -1.3f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);

	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.4f, -1.0f, -2.0f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);


	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(-4.4f, -1.0f, -1.5f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);

	//create model matrix for straw
	model = glm::rotate(glm::mat4(1.0f), glm::radians(angle), glm::vec3(0, 1, 0));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(2.4f, -1.0f, -1.0f));
	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader	
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//compute normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	straw.Draw(myCustomShader);
	



	renderTrees();
	drawFence();
	gardCasa1();
	drawFenceBiserica();
	

	//create model matrix for ground

	
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	//model = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 4.0f, 5.0f));
	model = glm::rotate(model, glm::radians(angle), glm::vec3(0, 1, 0));

//	model = glm::scale(model, glm::vec3(0.1));
	//send model matrix data to shader
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	//create normal matrix
	normalMatrix = glm::mat3(glm::inverseTranspose(view*model));
	//send normal matrix data to shader
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	ground.Draw(myCustomShader);

	//draw a white cube around the light

	lightShader.useShaderProgram();

	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

	model = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::translate(model, lightDir);
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	//lightCube.Draw(lightShader);

	mySkyBox.Draw(skyboxShader, view, projection);
}

int main(int argc, const char * argv[]) {

	initOpenGLWindow();
	initOpenGLState();
	initFBOs();
	initModels();
	initShaders();
	initUniforms();


	std::vector<const GLchar*> faces;

	faces.push_back("textures/ely_hills/hills_rt.tga");
	faces.push_back("textures/ely_hills/hills_lf.tga");
	faces.push_back("textures/ely_hills/hills_up.tga");
	faces.push_back("textures/ely_hills/hills_dn.tga");
	faces.push_back("textures/ely_hills/hills_bk.tga");
	faces.push_back("textures/ely_hills/hills_ft.tga");




	/*
	faces.push_back("textures/skybox/right.tga");
	faces.push_back("textures/skybox/left.tga");
	faces.push_back("textures/skybox/top.tga");
	faces.push_back("textures/skybox/bottom.tga");
	faces.push_back("textures/skybox/back.tga");
	faces.push_back("textures/skybox/front.tga");
	*/


	/*faces.push_back("textures/galaxie/drakeq_rt.tga");
	faces.push_back("textures/galaxie/drakeq_lf.tga");
	faces.push_back("textures/galaxie/drakeq_up.tga");
	faces.push_back("textures/galaxie/drakeq_dn.tga");
	faces.push_back("textures/galaxie/drakeq_bk.tga");
	faces.push_back("textures/galaxie/drakeq_ft.tga");


	faces.push_back("textures/ame_nebula/purplenebula_rt.tga");
	faces.push_back("textures/ame_nebula/purplenebula_lf.tga");
	faces.push_back("textures/ame_nebula/purplenebula_up.tga");
	faces.push_back("textures/ame_nebula/purplenebula_dn.tga");
	faces.push_back("textures/ame_nebula/purplenebula_bk.tga");
	faces.push_back("textures/ame_nebula/purplenebula_ft.tga");

	*/

	mySkyBox.Load(faces);
	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();
	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));

	projection = glm::perspective(glm::radians(45.0f), (float)retina_width / (float)retina_height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
		glm::value_ptr(projection));





	glCheckError();
	while (!glfwWindowShouldClose(glWindow)) {
		renderScene();

		glfwPollEvents();
		glfwSwapBuffers(glWindow);
	}

	//close GL context and any other GLFW resources
	glfwTerminate();

	return 0;
}
