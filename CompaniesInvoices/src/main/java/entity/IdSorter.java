package entity;

import java.util.Comparator;

public class IdSorter implements Comparator<Object>
{
    @Override
    public int compare(Object o1, Object o2)
    {
        Integer x=0,y=0;
        if(o1 instanceof Company && o2 instanceof Company)
        {
            x = ((Company) o1).getIdCompany();
            y = ((Company) o2).getIdCompany();
        }
        else if(o1 instanceof Product && o2 instanceof Product)
        {
            x = ((Product) o1).getProductNumber();
            y = ((Product) o2).getProductNumber();
        }

        return x.compareTo(y);
    }
}
