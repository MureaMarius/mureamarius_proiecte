package entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "companii")
public class Company
{
    @Id
    @Column(name = "idCompany")
    private int idCompany;

    @Column(name = "nameCompany")
    private String nameCompany;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    public Company()
    {

    }

    public Company(int idCompany, String nameCompany, String phoneNumber)
    {
        this.idCompany=idCompany;
        this.nameCompany=nameCompany;
        this.phoneNumber=phoneNumber;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNumberCompany() {
        return phoneNumber;
    }

    public void setNumberCompany(String numberCompany) {
        this.phoneNumber = numberCompany;
    }

    @Override
    public String toString()
    {
        return "Company{" +
                "idCompany=" + idCompany +
                ", nameCompany='" + nameCompany + '\'' +
                ", numberCompany='" + phoneNumber + '\'' +
                '}';
    }
}
