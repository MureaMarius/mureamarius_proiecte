package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

@Entity
@Table(name = "invoices")
public class Invoice
{
    @Id
    @Column(name = "invoiceNumber")
    private int invoiceNumber;

    @Column(name = "seller")
    private String seller;

    @Column(name = "products")
    private String products;

    @Column(name = "total")
    private double total;

    @Column(name = "dueDate")
    private LocalDate dueDate;

    @Column(name = "payDate")
    private LocalDate payDate;

    @Column(name = "status")
    private String status;

    @Column(name = "duplicat")
    private String duplication;

    public Invoice()
    {

    }

    public Invoice(int invoiceNumber,String seller, String products, double total, LocalDate dueDate, LocalDate payDate,String status, String duplication)
    {
        this.invoiceNumber=invoiceNumber;
        this.seller=seller;
        this.products=products;
        this.total=total;
        this.dueDate=dueDate;
        this.payDate=payDate;
        this.status=status;
        this.duplication=duplication;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getPayDate() {
        return payDate;
    }

    public void setPayDate(LocalDate payDate) {
        this.payDate = payDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDuplication() {
        return duplication;
    }

    public void setDuplication(String duplication) {
        this.duplication = duplication;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceNumber=" + invoiceNumber +
                ", seller='" + seller + '\'' +
                ", products='" + products + '\'' +
                ", total=" + total +
                ", dueDate=" + dueDate +
                ", payDate=" + payDate +
                ", status='" + status + '\'' +
                ", duplication='" + duplication + '\'' +
                '}';
    }
}
