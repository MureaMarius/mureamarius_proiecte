package design;

import entity.Company;
import entity.Invoice;
import entity.Product;
import service.InvoiceService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class GuiInvoiceTextSearch extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnBack, btnSearchByFirstWord, btnSearchBySecondWord, btnSearchByAnyContent, btnSearchProduct;
    private JTextField txtSearch;
    private JLabel lblSearch;

    private ArrayList<Invoice> invoices= new ArrayList<>();
    private ArrayList<Product> products= new ArrayList<>();
    private ArrayList<Company> companies= new ArrayList<>();

    private InvoiceService invoiceService= new InvoiceService(invoices,companies,products);

    public GuiInvoiceTextSearch()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Text search");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(500,300);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnBack= new JButton("Inapoi la meniul invoice");
        btnBack.setBounds(10,10,200,30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 15));
        cPane.add(btnBack);

        lblSearch= new JLabel("Search: ");
        lblSearch.setBounds(20,125,150,30);
        lblSearch.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(lblSearch);

        txtSearch= new JTextField();
        txtSearch.setBounds(100,125,100,30);
        cPane.add(txtSearch);

        btnSearchBySecondWord= new JButton("Search by second word");
        btnSearchBySecondWord.setBounds(250,100,180,30);
        cPane.add(btnSearchBySecondWord);

        btnSearchByAnyContent= new JButton("Search by any content");
        btnSearchByAnyContent.setBounds(250,150,170,30);
        cPane.add(btnSearchByAnyContent);

        btnSearchByFirstWord= new JButton("Search by first word");
        btnSearchByFirstWord.setBounds(250,50,150,30);
        cPane.add(btnSearchByFirstWord);

        btnSearchProduct= new JButton("Search by product");
        btnSearchProduct.setBounds(250,200,150,30);
        cPane.add(btnSearchProduct);

        btnBack.addActionListener(this);
        btnSearchProduct.addActionListener(this);
        btnSearchByFirstWord.addActionListener(this);
        btnSearchBySecondWord.addActionListener(this);
        btnSearchByAnyContent.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnBack)
        {
            new GuiInvoice();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnSearchByFirstWord)
        {
            search(1);
        }
        else if(e.getSource()==btnSearchBySecondWord)
        {
            search(2);
        }
        else if(e.getSource()==btnSearchByAnyContent)
        {
            search(3);
        }
        else if(e.getSource()==btnSearchProduct)
        {
            search(0);
        }
    }

    private void search(int OK)
    {
        String firstWord= txtSearch.getText().trim();

        System.out.println(firstWord);
        invoiceService.searchByWords(firstWord,OK);
        this.invoices=invoiceService.getInvoicesByWord();

        if(invoices.isEmpty())
        {
            JOptionPane.showMessageDialog( new JFrame(),"Nu sunt date disponibile!");
            txtSearch.setText(" ");

            return;
        }

        afisareDate();
        txtSearch.setText(" ");
    }

    private void afisareDate()
    {
        Table tableInvoices= new Table();
        tableInvoices.tableInvoices(tableInvoices.retrieveProperties(null,null,invoices,4),2);
    }
}
