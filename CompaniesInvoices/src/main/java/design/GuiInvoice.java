package design;

import entity.Company;
import entity.Invoice;
import entity.Product;
import service.CompanyService;
import service.InvoiceService;
import service.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class GuiInvoice extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnVerificareDate, btnAfisareDate, btnGenerateNewData, btnBack, btnEditing, btnTextSearch, btnOrderedInvoices, btnEquality;
    private JTextField txtNewDimension;
    private JLabel lblNewDimension;

    private ArrayList<Invoice> invoices= new ArrayList<>();
    private ArrayList<Product> products= new ArrayList<>();
    private ArrayList<Company> companies= new ArrayList<>();

    private InvoiceService invoiceService= new InvoiceService(invoices,companies,products);

    public GuiInvoice()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Invoices");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(500,400);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnBack= new JButton("Inapoi la meniul principal");
        btnBack.setBounds(10,10,200,30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 15));
        cPane.add(btnBack);

        btnOrderedInvoices= new JButton("Ordered invoices");
        btnOrderedInvoices.setBounds(10,100,200,30);
        btnOrderedInvoices.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(btnOrderedInvoices);

        btnTextSearch= new JButton("Text search");
        btnTextSearch.setBounds(250,100,200,30);
        btnTextSearch.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(btnTextSearch);

        btnEditing= new JButton("Editing invoice");
        btnEditing.setBounds(10,150,200,30);
        btnEditing.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(btnEditing);

        btnVerificareDate= new JButton("Verificare date");
        btnVerificareDate.setBounds(10,250,200,30);
        btnVerificareDate.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(btnVerificareDate);

        btnAfisareDate= new JButton("Afisare date");
        btnAfisareDate.setBounds(250,250,200,30);
        btnAfisareDate.setFont(new Font("Times New Roman",Font.BOLD,20));
        cPane.add(btnAfisareDate);

        lblNewDimension= new JLabel("Dati dimensiunea:");
        lblNewDimension.setBounds(10,300,200,30);
        lblNewDimension.setFont(new Font("Times New Roman",Font.BOLD,17));
        cPane.add(lblNewDimension);

        txtNewDimension= new JTextField();
        txtNewDimension.setBounds(150,300,50,30);
        cPane.add(txtNewDimension);

        btnEquality= new JButton("Equality vs identity");
        btnEquality.setBounds(250,150,200,30);
        btnEquality.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cPane.add(btnEquality);

        btnBack.addActionListener(this);
        btnAfisareDate.addActionListener(this);
        btnVerificareDate.addActionListener(this);
        btnEditing.addActionListener(this);
        btnTextSearch.addActionListener(this);
        btnOrderedInvoices.addActionListener(this);
        btnEquality.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnBack)
        {
            new GuiPrincipal();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnAfisareDate)
        {
            afisareDate();
        }
        else if(e.getSource()==btnVerificareDate)
        {
            verificareDate();
        }
        else if(e.getSource()==btnGenerateNewData)
        {
            generateNewData();
        }
        else if(e.getSource()==btnTextSearch)
        {
            new GuiInvoiceTextSearch();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnOrderedInvoices)
        {
            afisareOrderedInvoices();
        }
        else if(e.getSource()==btnEditing)
        {
            new GuiInvoicesEditingInvoice();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnEquality)
        {
            new GuiEquality();
            this.appFrame.setVisible(false);
        }
    }

    private void afisareOrderedInvoices()
    {
        Table tableInvoices= new Table();
        this.invoiceService.getAllInvoicesByDueDate();
        this.invoices=this.invoiceService.getInvoices();

        tableInvoices.tableInvoices(tableInvoices.retrieveProperties(null,null,invoices,3),1);
    }

    private void generateNewData()
    {
        invoiceService.deleteAllInvoices();
        int newDimension= Integer.parseInt(txtNewDimension.getText().trim());
        invoiceService.getNewInvoices(newDimension);
        invoiceService.insertInvoices();
        invoiceService.getAllInvoices();

        this.invoices=invoiceService.getInvoices();
        txtNewDimension.setText(" ");
    }

    private void verificareDate()
    {
        invoiceService.getAllInvoices();
        this.invoices= invoiceService.getInvoices();

        if(invoices.isEmpty())
        {
            JOptionPane.showMessageDialog( new JFrame(),"Nu sunt date disponibile!");

            formatBtn();
        }
        else
        {
            JOptionPane.showMessageDialog( new JFrame(),"Sunt date disponibile!");

            formatBtn();
        }
    }

    private void formatBtn()
    {
        btnGenerateNewData= new JButton("Generate New Data");
        btnGenerateNewData.setBounds(230,300,180,30);
        btnGenerateNewData.setFont(new Font("Times New Roman",Font.BOLD,17));
        cPane.add(btnGenerateNewData);

        cPane.setVisible(true);
        btnGenerateNewData.setVisible(true);

        btnGenerateNewData.addActionListener(this);
    }

    private void afisareDate()
    {
        Table tableInvoices= new Table();
        this.invoiceService.getAllInvoices();
        this.invoices=this.invoiceService.getInvoices();

        tableInvoices.tableInvoices(tableInvoices.retrieveProperties(null,null,invoices,2),0);
    }
}
