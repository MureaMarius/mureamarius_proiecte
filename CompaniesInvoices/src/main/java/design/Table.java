package design;

import entity.Company;
import entity.Invoice;
import entity.Product;

import javax.swing.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.time.temporal.ChronoUnit;

public class Table
{
    JTable companies;
    JTable products;
    JTable invoices;

    JFrame frameCompanies;
    JFrame frameProducts;
    JFrame frameInvoices;

    public Table()
    {

    }

    public String[][] retrieveProperties(ArrayList<Company> companies, ArrayList<Product> products, ArrayList<Invoice> invoices, int OK) {

        int i = 0;

        String[][] data = new String[100][100];

        if (OK == 0) {
            for (Company company : companies) {
                try {
                    data[i][0] = String.valueOf(company.getIdCompany());
                    data[i][1] = company.getNameCompany();
                    data[i][2] = company.getNumberCompany();

                    i++;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }

            return data;
        } else if (OK == 1) {
            for (Product product : products) {
                try {
                    data[i][0] = String.valueOf(product.getProductNumber());
                    data[i][1] = product.getProductName();
                    data[i][2] = String.valueOf(product.getProductPrice());

                    i++;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
            return data;
        }
        else if (OK == 2)
        {
            for (Invoice invoice : invoices)
            {
                try {
                    data[i][0] = String.valueOf(invoice.getInvoiceNumber());
                    data[i][1] = invoice.getSeller();
                    data[i][2] = invoice.getProducts();
                    data[i][3] = String.valueOf(invoice.getTotal());
                    data[i][4] = String.valueOf(invoice.getDueDate());
                    data[i][5] = String.valueOf(invoice.getPayDate());
                    data[i][6] = invoice.getStatus();
                    data[i][7] = invoice.getDuplication();

                    i++;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
            return data;
        }
        else if(OK==3)
        {
            for (Invoice invoice : invoices)
            {
                int untilDueDate=0;
                try {
                    data[i][0] = String.valueOf(invoice.getInvoiceNumber());
                    data[i][1] = invoice.getSeller();
                    data[i][2] = invoice.getProducts();
                    data[i][3] = String.valueOf(invoice.getTotal());
                    data[i][4] = String.valueOf(invoice.getDueDate());
                    data[i][5] = String.valueOf(invoice.getPayDate());
                    data[i][7] = invoice.getDuplication();

                    if(invoice.getDueDate()==null)
                    {
                        data[i][6]= "paid";
                    }
                    else
                    {
                        LocalDate data1= invoice.getDueDate();
                        LocalDate data2= LocalDate.of(2020,4,1);
                        untilDueDate= (int) ChronoUnit.DAYS.between(data2,data1);
                        data[i][6]= String.valueOf(untilDueDate);

                    }

                    i++;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
            return data;
        }
        else if(OK==4)
        {
            for (Invoice invoice : invoices)
            {
                try
                {
                    data[i][0] = invoice.getSeller();
                    data[i][1] = invoice.getProducts();
                    data[i][2] = String.valueOf(invoice.getDueDate());
                    data[i][3] = String.valueOf(invoice.getPayDate());

                    i++;
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
            }
            return data;
        }

        return data;
    }

    public void tableCompanies(String[][] dataCompanies)
    {
        frameCompanies= new JFrame();
        frameCompanies.setTitle("Tabel companies");

        String[] columnNames= {"IdCompany","NameCompany","PhoneNumber"};

        companies= new JTable(dataCompanies,columnNames);
        companies.setBounds(30, 40, 300, 300);

        JScrollPane scrollPane= new JScrollPane(companies);
        frameCompanies.add(scrollPane);
        frameCompanies.setSize(500, 200);
        frameCompanies.setVisible(true);
    }

    public void tableProducts(String[][] dataProducts)
    {
        frameProducts = new JFrame();
        frameProducts.setTitle("Tabel products");

        String[] columnNames = {"ProductNumber", "NameProduct", "PriceProduct"};

        products = new JTable(dataProducts, columnNames);
        products.setBounds(30, 40, 300, 300);

        JScrollPane scrollPane = new JScrollPane(products);
        frameProducts.add(scrollPane);
        frameProducts.setSize(500, 200);
        frameProducts.setVisible(true);
    }

    public void tableInvoices(String[][] dataInvoices,int OK)
    {
        frameInvoices = new JFrame();
        frameInvoices.setTitle("Tabel invoices");

        String[] columnNames = {" "};

        if(OK==0)
        {
            columnNames = new String[]{"InvoiceNumber", "Seller", "Products", "Total", "DueDate", "PayDate", "Status","Duplication"};
        }
        else if(OK==1)
        {
            columnNames = new String[]{"InvoiceNumber", "Seller", "Products", "Total", "DueDate", "PayDate", "DaysUntilDueDate","Duplication"};
        }
        else if(OK==2)
        {
            columnNames = new String[]{"Seller", "Products", "DueDate", "PayDate"};
        }

        invoices = new JTable(dataInvoices, columnNames);
        invoices.setBounds(30, 40, 300, 300);

        JScrollPane scrollPane = new JScrollPane(invoices);
        frameInvoices.add(scrollPane);
        frameInvoices.setSize(500, 200);
        frameInvoices.setVisible(true);
    }
}
