package design;

import entity.Company;
import entity.Invoice;
import entity.Product;
import service.InvoiceService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class GuiInvoicesEditingInvoice extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnBack, btnPay, btnAfisareDate;
    private JLabel lblIdInvoice;
    private JTextField txtIdInvoice;

    private ArrayList<Invoice> invoices= new ArrayList<>();
    private ArrayList<Product> products= new ArrayList<>();
    private ArrayList<Company> companies= new ArrayList<>();

    private InvoiceService invoiceService= new InvoiceService(invoices,companies,products);

    public GuiInvoicesEditingInvoice()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Edit invoice");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(400,300);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnBack= new JButton("Inapoi la meniul invoice");
        btnBack.setBounds(10,10,200,30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 15));
        cPane.add(btnBack);

        lblIdInvoice = new JLabel("Introduceti id-ul: ");
        lblIdInvoice.setBounds(10,80,200,50);
        lblIdInvoice.setFont(new Font("Times New Roman", Font.BOLD, 15));
        cPane.add(lblIdInvoice);

        txtIdInvoice= new JTextField();
        txtIdInvoice.setBounds(130,90,40,30);
        cPane.add(txtIdInvoice);

        btnPay= new JButton("PAY");
        btnPay.setBounds(220,88,100,30);
        btnPay.setFont(new Font("Times New Roman", Font.BOLD, 25));
        cPane.add(btnPay);

        btnAfisareDate= new JButton("Afisati datele actualizate!");
        btnAfisareDate.setBounds(30,150,320,30);
        btnAfisareDate.setFont(new Font("Times New Roman", Font.BOLD, 25));
        cPane.add(btnAfisareDate);

        btnBack.addActionListener(this);
        btnPay.addActionListener(this);
        btnAfisareDate.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnBack)
        {
            new GuiInvoice();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnPay)
        {
            payInvoice();
        }
        else if(e.getSource()==btnAfisareDate)
        {
            afisareDate();
        }
    }

    private void afisareDate()
    {
        Table tableInvoices= new Table();
        this.invoiceService.getAllInvoices();
        this.invoices=this.invoiceService.getInvoices();

        tableInvoices.tableInvoices(tableInvoices.retrieveProperties(null,null,invoices,2),0);
    }

    private void payInvoice() throws NullPointerException
    {
        int idInvoice= Integer.parseInt(txtIdInvoice.getText().trim());

        try
        {
            Invoice invoice = invoiceService.getInvoiceByID(idInvoice);

            if (invoice.getStatus().equalsIgnoreCase("paid"))
            {
                JOptionPane.showMessageDialog(new JFrame(), "Invoice-ul este platit!");

                txtIdInvoice.setText(" ");
            }
            else
            {
                invoiceService.paidInvoice(idInvoice);
                txtIdInvoice.setText(" ");
            }
        }
        catch (NullPointerException e)
        {
            JOptionPane.showMessageDialog(new JFrame(), "Nu exista acest invoice!");
        }
    }
}
