package design;

import entity.Company;
import generators.CompaniesGenerator;
import repository.CompanyRepo;
import service.CompanyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class GuiCompany extends JFrame implements ActionListener
{
    private int newDimension;
    private JFrame appFrame;
    private Container cPane;
    private JButton btnVerificareDate, btnIntroducereDate, btnAfisareDate, btnGenerateNewData, btnBack;
    private JTextField txtFirstNames,txtLastNames, txtMiddleNames, txtDimensiune;
    private JLabel lblFirstNames, lblLastNames, lblMiddleNames, lblDimensiuneGenerata;

    private ArrayList<Company> companies= new ArrayList<>();
    private CompanyService companyService= new CompanyService(companies);

    public GuiCompany()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Company");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(580,400);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        lblFirstNames= new JLabel("First names: ");
        lblFirstNames.setBounds(10,50,150,50);
        cPane.add(lblFirstNames);

        txtFirstNames= new JTextField();
        txtFirstNames.setBounds(90,60,150,30);
        cPane.add(txtFirstNames);

        lblLastNames= new JLabel("Last names: ");
        lblLastNames.setBounds(10,90,150,50);
        cPane.add(lblLastNames);

        txtLastNames= new JTextField();
        txtLastNames.setBounds(90,100,150,30);
        cPane.add(txtLastNames);

        lblMiddleNames= new JLabel("Middle names: ");
        lblMiddleNames.setBounds(10,130,150,50);
        cPane.add(lblMiddleNames);

        txtMiddleNames= new JTextField();
        txtMiddleNames.setBounds(95,140,150,30);
        cPane.add(txtMiddleNames);

        btnIntroducereDate= new JButton("Introduceti aceste date");
        btnIntroducereDate.setBounds(270,100,200,30);
        cPane.add(btnIntroducereDate);

        btnVerificareDate= new JButton("Verificare disponibilitate date");
        btnVerificareDate.setBounds(10,210,220,30);
        cPane.add(btnVerificareDate);

        btnAfisareDate= new JButton("Afisare companii");
        btnAfisareDate.setBounds(300,210,220,30);
        cPane.add(btnAfisareDate);

        btnBack= new JButton("Inapoi la meniul principal");
        btnBack.setBounds(10,10,180,30);
        cPane.add(btnBack);

        lblDimensiuneGenerata= new JLabel("Dimensiune:");
        lblDimensiuneGenerata.setBounds(170,300,180,30);
        cPane.add(lblDimensiuneGenerata);


        txtDimensiune= new JTextField();
        txtDimensiune.setBounds(250,300,30,30);
        cPane.add(txtDimensiune);

        btnVerificareDate.addActionListener(this);
        btnAfisareDate.addActionListener(this);
        btnBack.addActionListener(this);
        btnIntroducereDate.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnAfisareDate)
        {
            afisareItems();
        }
        else if(e.getSource()==btnBack)
        {
            GuiPrincipal guiPrincipal= new GuiPrincipal();

            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnVerificareDate)
        {
            verificareDisponibilitateDate();
        }
        else if(e.getSource()==btnGenerateNewData)
        {
            generateNewData();
        }
        else if(e.getSource()==btnIntroducereDate)
        {
            introducereNoiDate();
        }
    }

    private void introducereNoiDate()
    {
        String firstNames = txtFirstNames.getText();
        String lastNames= txtLastNames.getText();
        String middleNames= txtMiddleNames.getText();

        String[] firstN = firstNames.split(" ");
        String[] lastN= lastNames.split(" ");
        String[] middleN= lastNames.split(" ");

        newDimension= Integer.parseInt(txtDimensiune.getText().trim());

        companyService.getNewCompaniesByNames(firstN,lastN,middleN,newDimension);

        txtMiddleNames.setText(" ");
        txtLastNames.setText(" ");
        txtFirstNames.setText(" ");
        txtDimensiune.setText(" ");
    }

    private void generateNewData()
    {
        companyService.deleteAllCompanies();
        newDimension= Integer.parseInt(txtDimensiune.getText().trim());
        companyService.getNewCompanies(newDimension);
        companyService.insertCompanies();
        companyService.getAllCompanies();

        this.companies=companyService.getCompanies();
        txtDimensiune.setText(" ");
    }

    public void afisareItems()
    {
        Table tableCompanies= new Table();
        this.companyService.getAllCompanies();
        this.companies=this.companyService.getCompanies();

        tableCompanies.tableCompanies(tableCompanies.retrieveProperties(this.companies,null,null,0));
    }

    private void formatBtn()
    {
        btnGenerateNewData= new JButton("Generate New Data");
        btnGenerateNewData.setBounds(180,250,180,30);
        cPane.add(btnGenerateNewData);

        cPane.setVisible(true);
        btnGenerateNewData.setVisible(true);
        txtDimensiune.setVisible(true);
        lblDimensiuneGenerata.setVisible(true);

        btnGenerateNewData.addActionListener(this);
    }

    public void verificareDisponibilitateDate()
    {
        companyService.getAllCompanies();
        this.companies= companyService.getCompanies();

        if(companies.isEmpty())
        {
            JOptionPane.showMessageDialog( new JFrame(),"Nu sunt date disponibile!");

            formatBtn();
        }
        else
        {
            JOptionPane.showMessageDialog( new JFrame(),"Sunt date disponibile!");

            formatBtn();
        }
    }
}
