package design;

import entity.Company;
import entity.Invoice;
import entity.Product;
import service.InvoiceService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class GuiEquality extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnAddInvoice, btnBack, btnAfisare;
    private JTextField txtSeller, txtProducts, txtTotal, txtDueDate, txtPayDate, txtStatus, txtDuplicate;
    private JLabel lblSeller, lblProducts, lblTotal, lblDueDate, lblPayDate, lblStatus, lblDuplicate;

    private ArrayList<Invoice> invoices= new ArrayList<>();
    private ArrayList<Product> products= new ArrayList<>();
    private ArrayList<Company> companies= new ArrayList<>();

    private InvoiceService invoiceService= new InvoiceService(invoices,companies,products);

    public GuiEquality()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Equality vs Identity");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(500,400);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnBack= new JButton("Inapoi la Invoice");
        btnBack.setBounds(10,10,200,30);
        btnBack.setFont(new Font("Times New Roman",Font.BOLD,20));
        cPane.add(btnBack);

        lblSeller= new JLabel("Seller: ");
        lblSeller.setBounds(10,70,150,30);
        cPane.add(lblSeller);

        txtSeller= new JTextField();
        txtSeller.setBounds(50,70,150,30);
        cPane.add(txtSeller);

        lblProducts= new JLabel("Products:");
        lblProducts.setBounds(10,120,150,30);
        cPane.add(lblProducts);

        txtProducts= new JTextField();
        txtProducts.setBounds(70,120,150,30);
        cPane.add(txtProducts);

        lblDueDate= new JLabel("Due date:");
        lblDueDate.setBounds(10,170,150,30);
        cPane.add(lblDueDate);

        txtDueDate= new JTextField();
        txtDueDate.setBounds(70,170,150,30);
        cPane.add(txtDueDate);

        lblPayDate= new JLabel("Pay date:");
        lblPayDate.setBounds(10,220,150,30);
        cPane.add(lblPayDate);

        txtPayDate= new JTextField();
        txtPayDate.setBounds(70,220,150,30);
        cPane.add(txtPayDate);

        lblTotal= new JLabel("Total:");
        lblTotal.setBounds(10,270,150,30);
        cPane.add(lblTotal);

        txtTotal= new JTextField();
        txtTotal.setBounds(50,270,150,30);
        cPane.add(txtTotal);

        lblStatus= new JLabel("Status:");
        lblStatus.setBounds(250,70,150,30);
        cPane.add(lblStatus);

        txtStatus= new JTextField();
        txtStatus.setBounds(300,70,150,30);
        cPane.add(txtStatus);

        lblDuplicate= new JLabel("Duplicat:");
        lblDuplicate.setBounds(250,120,150,30);
        cPane.add(lblDuplicate);

        txtDuplicate= new JTextField();
        txtDuplicate.setBounds(300,120,150,30);
        cPane.add(txtDuplicate);

        btnAddInvoice= new JButton("Add invoice!");
        btnAddInvoice.setBounds(250,200,150,30);
        cPane.add(btnAddInvoice);

        btnAfisare= new JButton("Afisare!");
        btnAfisare.setBounds(250,250,150,30);
        cPane.add(btnAfisare);

        btnAfisare.addActionListener(this);
        btnAddInvoice.addActionListener(this);
        btnBack.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnBack)
        {
            new GuiInvoice();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnAddInvoice)
        {
            addInvoice();
        }
        else if(e.getSource()==btnAfisare)
        {
            afisare();
        }
    }

    private void afisare()
    {
        Table tableInvoices= new Table();
        this.invoiceService.getAllInvoices();
        this.invoices=this.invoiceService.getInvoices();

        tableInvoices.tableInvoices(tableInvoices.retrieveProperties(null,null,invoices,2),0);
    }

    private void addInvoice()
    {
        String seller= txtSeller.getText().trim();
        String product= txtProducts.getText().trim();
        double total= Double.parseDouble(txtTotal.getText().trim());
        String status= txtStatus.getText().trim();
        String duplicate= txtDuplicate.getText().trim();

        ArrayList<Invoice> inv= new ArrayList<>();

        this.invoiceService.getAllInvoices();
        inv= this.invoiceService.getInvoices();

        String data1;
        String data2;
        DateTimeFormatter formatter;
        LocalDate dueDate;
        LocalDate payDate;

        if(txtDueDate.getText().isEmpty())
        {
            dueDate=null;
        }
        else
        {
            data1 = txtDueDate.getText().trim();
            formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dueDate  = LocalDate.parse(data1);
        }

        if(txtPayDate.getText().isEmpty())
        {
            payDate=null;
        }
        else
        {
            data2 = txtPayDate.getText().trim();
            formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            payDate  = LocalDate.parse(data2);
        }

        int index= inv.size();
        Invoice invoice= new Invoice(100,seller,product,total,dueDate,payDate,status,duplicate);

        this.invoiceService.insertSingleInvoice(invoice);

        txtSeller.setText(" ");
        txtStatus.setText(" ");
        txtDuplicate.setText(" ");
        txtPayDate.setText(" ");
        txtTotal.setText(" ");
        txtProducts.setText(" ");
        txtDueDate.setText(" ");
    }
}
