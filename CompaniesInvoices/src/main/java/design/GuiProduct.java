package design;

import entity.Company;
import entity.Product;
import service.CompanyService;
import service.ProductService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class GuiProduct extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnValidareDateProduct, btnGenerateNewData, btnBack, btnAfisareData;
    private JTextField txtDimensiune;
    private JLabel lblDimensiuneGenerata;

    private ArrayList<Product> products= new ArrayList<>();
    private ProductService productService= new ProductService(products);

    private int newDimension;

    public GuiProduct()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Products");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(580,400);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnBack= new JButton("Inapoi la meniul principal");
        btnBack.setBounds(10,10,180,30);
        cPane.add(btnBack);

        btnValidareDateProduct= new JButton("Verificare disponibilitate date");
        btnValidareDateProduct.setBounds(10,100,290,50);
        btnValidareDateProduct.setFont(new Font("Times New Roman", Font.BOLD,20));
        cPane.add(btnValidareDateProduct);

        btnAfisareData= new JButton("Afisare date");
        btnAfisareData.setBounds(350,100,150,50);
        btnAfisareData.setFont(new Font("Times New Roman", Font.BOLD,20));
        cPane.add(btnAfisareData);

        lblDimensiuneGenerata= new JLabel("Dimensiune date:");
        lblDimensiuneGenerata.setBounds(100,230,150,50);
        lblDimensiuneGenerata.setFont(new Font("Times New Roman", Font.BOLD,20));
        cPane.add(lblDimensiuneGenerata);

        txtDimensiune= new JTextField();
        txtDimensiune.setBounds(250,240,50,30);
        cPane.add(txtDimensiune);

        btnBack.addActionListener(this);
        btnAfisareData.addActionListener(this);
        btnValidareDateProduct.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnBack)
        {
            new GuiPrincipal();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnAfisareData)
        {
            afisareProducts();
        }
        else if(e.getSource()==btnValidareDateProduct)
        {
            validareDate();
        }
        else if(e.getSource()==btnGenerateNewData)
        {
            generateNewData();
        }
    }

    private void generateNewData()
    {
        productService.deleteAllProducts();
        newDimension= Integer.parseInt(txtDimensiune.getText().trim());
        productService.getNewProducts(newDimension);
        productService.insertProducts();
        productService.getAllProducts();

        this.products=productService.getProducts();

        txtDimensiune.setText(" ");
    }

    private void formatBtn()
    {
        btnGenerateNewData= new JButton("Generate New Data");
        btnGenerateNewData.setBounds(310,240,210,30);
        btnGenerateNewData.setFont(new Font("Times New Roman", Font.BOLD,20));
        cPane.add(btnGenerateNewData);

        cPane.setVisible(true);
        btnGenerateNewData.setVisible(true);
        txtDimensiune.setVisible(true);
        lblDimensiuneGenerata.setVisible(true);

        btnGenerateNewData.addActionListener(this);
    }

    private void validareDate()
    {
        productService.getAllProducts();
        this.products= productService.getProducts();

        if(products.isEmpty())
        {
            JOptionPane.showMessageDialog( new JFrame(),"Nu sunt date disponibile!");

            this.formatBtn();
        }
        else
        {
            JOptionPane.showMessageDialog( new JFrame(),"Sunt date disponibile!");

            this.formatBtn();
        }
    }

    private void afisareProducts()
    {
        Table tableProducts= new Table();

        this.productService.getAllProducts();
        this.products=this.productService.getProducts();

        tableProducts.tableProducts(tableProducts.retrieveProperties(null,this.products,null,1));
    }
}
