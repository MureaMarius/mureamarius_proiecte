package design;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuiPrincipal implements ActionListener
{
    private JFrame appFrame;
    private Container cPane;
    private JButton btnCompany, btnInvoice, btnProduct;

    public GuiPrincipal()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Meniu Principal");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(580,500);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnCompany= new JButton("Companies");
        btnCompany.setBounds(10,150,200,50);
        btnCompany.setFont(new Font("Times New Roman", Font.BOLD, 30));

        btnInvoice= new JButton("Invoices");
        btnInvoice.setBounds(180,250,200,50);
        btnInvoice.setFont(new Font("Times New Roman", Font.BOLD, 30));

        btnProduct= new JButton("Products");
        btnProduct.setBounds(350,150,200,50);
        btnProduct.setFont(new Font("Times New Roman", Font.BOLD,30));

        cPane.add(btnCompany);
        cPane.add(btnInvoice);
        cPane.add(btnProduct);

        btnProduct.addActionListener(this);
        btnInvoice.addActionListener(this);
        btnCompany.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnCompany)
        {
            GuiCompany guiCompany= new GuiCompany();
            this.appFrame.setVisible(false);
        }
        else if(e.getSource()==btnProduct)
        {
            GuiProduct guiProduct= new GuiProduct();
            this.appFrame.setVisible(false);

        }
        else if(e.getSource()==btnInvoice)
        {
            GuiInvoice guiInvoice= new GuiInvoice();
            this.appFrame.setVisible(false);
        }
    }
}
