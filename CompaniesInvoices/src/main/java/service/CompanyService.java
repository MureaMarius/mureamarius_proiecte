package service;

import entity.Company;
import generators.CompaniesGenerator;
import repository.CompanyRepo;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import java.io.IOException;
import java.util.ArrayList;

public class CompanyService
{
    private CompaniesGenerator companiesGenerator;
    private CompanyRepo companyRepo;
    private ArrayList<Company> companies;

    public CompanyService()
    {

    }

    public CompanyService(ArrayList<Company> companies)
    {
        this.companies=companies;
        this.companyRepo= new CompanyRepo();
        this.companiesGenerator= new CompaniesGenerator();
    }

    public void getAllCompanies()
    {
        this.companies=this.companyRepo.getAllCompanies();
    }

    public void getNewCompaniesByNames(String[] firstNames, String[] lastNames, String[] middleNames, int dimensiuneGenerabila)
    {
        companiesGenerator= new CompaniesGenerator(firstNames, lastNames, middleNames, dimensiuneGenerabila);
        companiesGenerator.generateCompanies();
        companies= companiesGenerator.getCompanies();

        int size=companies.size();
        for(int i=size-1;i>=0;i--)
        {
            companyRepo.insert(companies.get(i));
        }
    }

    public void getNewCompanies(int dimensiuneGenerabila)
    {
        companiesGenerator= new CompaniesGenerator(dimensiuneGenerabila);
        companiesGenerator.generateCompanies();
        companies=companiesGenerator.getCompanies();
    }

    /*
        Metoda pt a insera un set nou de companii
     */
    public void insertCompanies()
    {
        int size=companies.size();
        for(int i=size-1;i>=0;i--)
        {
            companyRepo.insert(companies.get(i));
        }
    }

    public void deleteAllCompanies()
    {
        this.companies=this.companyRepo.getAllCompanies();
        companyRepo.deleteAll();
        this.companies=this.companyRepo.getAllCompanies();
    }

    public ArrayList<Company> getCompanies()
    {
        return companies;
    }

    public void setCompanies(ArrayList<Company> companies)
    {
        this.companies = companies;
    }
}
