package service;

import entity.Company;
import entity.Invoice;
import entity.Product;
import generators.InvoicesGenerator;
import org.apache.commons.lang3.StringUtils;
import repository.InvoiceRepo;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

public class InvoiceService
{
    private InvoicesGenerator invoicesGenerator;
    private InvoiceRepo invoiceRepo;

    //Voi avea nevoie de cate un array list de companii, products si invoices
    private ArrayList<Company> companies;
    private ArrayList<Product> products;
    private ArrayList<Invoice> invoices;

    private ArrayList<Invoice> invoicesByWord;

    public InvoiceService(ArrayList<Invoice> invoices,ArrayList<Company> companies,ArrayList<Product> products)
    {
        this.invoices=invoices;
        this.invoiceRepo= new InvoiceRepo();
        this.invoicesGenerator= new InvoicesGenerator(companies,products);

        this.companies=companies;
        this.products=products;
    }

    public void getAllInvoices()
    {
        this.invoices=this.invoiceRepo.getAllInvoices();
    }

    /*
            Metoda prin care generam un nou set de invoices de dimensiune "dimensiuneGenerabila"
     */
    public void getNewInvoices(int dimensiuneGenerabila)
    {
        CompanyService companyService= new CompanyService(this.companies);
        ProductService productService= new ProductService(this.products);

        companyService.getAllCompanies();
        productService.getAllProducts();

        this.companies=companyService.getCompanies();
        this.products=productService.getProducts();

        invoicesGenerator= new InvoicesGenerator(this.companies,this.products,dimensiuneGenerabila);
        invoicesGenerator.generateInvoices();

        this.invoices=invoicesGenerator.getInvoices();
        insertInvoices();
    }

    public void insertInvoices()
    {
        int size= invoices.size();
        for(int i=size-1;i>=0;i--)
        {
            invoiceRepo.insert(invoices.get(i));        ///inserez noile invoice-uri
        }
    }


    ///Sterg toate invoice-urile
    public void deleteAllInvoices()
    {
        this.invoices=this.invoiceRepo.getAllInvoices();
        invoiceRepo.deleteAll();

        this.invoices=this.invoiceRepo.getAllInvoices();
    }

    ///Iau invoice-uri dupa dueDate
    public void getAllInvoicesByDueDate()
    {
        ArrayList<Invoice> invoices= invoiceRepo.gettAllByDueDate();

        this.invoices=invoices;
    }

    ///Caut dupa cuvant
    public void searchByWords(String words, int okCerinta)
    {
        this.invoices=this.invoiceRepo.getAllInvoices();
        this.invoicesByWord= new ArrayList<Invoice>();

        for(Invoice invoice: invoices)
        {
            String[] seller=invoice.getSeller().split(" ");
            String[] product= invoice.getProducts().split(" ");

            if(okCerinta>0)
            {
               if(okCerinta==1 && seller[0].equalsIgnoreCase(words))
               {
                  invoicesByWord.add(invoice);
               }
               else if(okCerinta==2 && seller[1].equalsIgnoreCase(words))
               {
                   invoicesByWord.add(invoice);
               }
               else if(okCerinta==3)
               {
                   String sel= invoice.getSeller();

                   boolean containerContainsContent = StringUtils.containsIgnoreCase(sel, words);
                   if(containerContainsContent)
                   {
                       invoicesByWord.add(invoice);
                   }
               }
            }
            else if(okCerinta==0)
            {
                for(int i=0;i<product.length;i++)
                {
                    if(product[i].equalsIgnoreCase(words))
                    {
                        invoicesByWord.add(invoice);
                    }
                }
            }
        }
    }

    ///Inserez un singur invoice egal cu alt invoice disponibil
    public void insertSingleInvoice(Invoice invoice)
    {
        this.invoices=invoiceRepo.getAllInvoices();

        int OK=0;
        for(Invoice inv: invoices)          ///parcurg lista de invoice-uri disponibila
        {
            ////Si incep sa verific fieldurile
            if(inv.getSeller().equals(invoice.getSeller()))
            {
                String a= inv.getProducts();
                String b= invoice.getProducts();

                if(a.equals(b))
                {
                    if(inv.getTotal()==invoice.getTotal())
                    {
                        if(inv.getStatus().equals(invoice.getStatus()))
                        {
                            String d1= inv.getDueDate().toString();
                            String d1egal =invoice.getDueDate().toString();

                            String d2= inv.getPayDate().toString();
                            String d2egal= invoice.getPayDate().toString();

                            if(d1.equals(d1egal) && d2.equals(d2egal))
                            {
                                this.invoices.add(invoice);
                                OK=1;
                            }
                        }
                    }
                }
            }
        }

        if(OK==1)
        {
            invoiceRepo.insertIntoInvoiceTable(invoice);
        }

        this.invoices=invoiceRepo.getAllInvoices();
    }

    public Invoice getInvoiceByID(int idInvoice)
    {
        return invoiceRepo.getInvoiceByID(idInvoice);
    }

    public void paidInvoice(int idInvoice)
    {
        invoiceRepo.updateStatusInvoice(idInvoice);
        this.invoices=invoiceRepo.getAllInvoices();
    }

    public ArrayList<Invoice> getInvoices()
    {
        return this.invoices;
    }

    public ArrayList<Invoice> getInvoicesByWord()
    {
        return invoicesByWord;
    }

    public void afisareInvoices()
    {
        for(Invoice invoice: invoices)
        {
            System.out.println(invoice.toString());
        }
    }
}
