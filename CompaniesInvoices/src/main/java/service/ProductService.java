package service;

import entity.Company;
import entity.IdSorter;
import entity.Product;
import generators.CompaniesGenerator;
import generators.ProductsGenerator;
import repository.ProductRepo;

import java.util.ArrayList;

public class ProductService
{
    private ProductsGenerator productsGenerator;
    private ProductRepo productRepo;
    private ArrayList<Product> products;

    public ProductService()
    {

    }

    public ProductService(ArrayList<Product> products)
    {
        this.products=products;
        this.productRepo=new ProductRepo();
        this.productsGenerator= new ProductsGenerator();
    }

    public void getAllProducts()
    {
        this.products=this.productRepo.getAllProducts();
    }

    public void getNewProducts(int dimensiuneGenerabila)
    {
        productsGenerator= new ProductsGenerator(dimensiuneGenerabila);
        productsGenerator.generateProducts();
        products=productsGenerator.getProducts();
    }

    /*
        Metoda pt a insera un set nou de companii
     */
    public void insertProducts()
    {
        products.sort(new IdSorter());

        int size=products.size();
        for(int i=size-1;i>=0;i--)
        {
            productRepo.insert(products.get(i));
        }
    }

    public void deleteAllProducts()
    {
        this.products=this.productRepo.getAllProducts();
        productRepo.deleteAll();
        this.products=this.productRepo.getAllProducts();
    }

    public ArrayList<Product> getProducts()
    {
        return products;
    }

    public void setProducts(ArrayList<Product> products)
    {
        this.products = products;
    }
}
