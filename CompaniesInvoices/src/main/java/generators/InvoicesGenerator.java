package generators;

import entity.Company;
import entity.Invoice;
import entity.Product;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class InvoicesGenerator
{
    private ArrayList<Company> companies;
    private ArrayList<Product> products;
    private ArrayList<Invoice> invoices;

    private int dimension;
    private static String status=" ";

    public InvoicesGenerator(ArrayList<Company> companies, ArrayList<Product> products)
    {
        this.companies=companies;
        this.products=products;
        invoices= new ArrayList<>();

        this.dimension=50;
    }

    public InvoicesGenerator(ArrayList<Company> companies, ArrayList<Product> products, int dimension)
    {
        this.companies=companies;
        this.products=products;
        this.dimension=dimension;

        invoices= new ArrayList<>();
    }

    public void generateInvoices()
    {
        Random rnd= new Random();
        int max=50;
        int min=1;

        int i, j;

        String produseLocalePeInvoice;
        String companiePeInvoice;

        int indexCompanie;
        for(i=1;i<=dimension;i++)
        {
            double total=0;
            LocalDate dueDate=null;
            LocalDate payDate=null;

            produseLocalePeInvoice= new String();
            companiePeInvoice= new String();

            int index=rnd.nextInt(2);

            int nrProduse=rnd.nextInt(4);
            if(nrProduse==0)
                nrProduse++;

            for(j=0;j<nrProduse;j++)
            {
                int indexProduse=rnd.nextInt(this.products.size());

                if(this.products.size()==0)
                    break;

                produseLocalePeInvoice+=this.products.get(indexProduse).getProductName()+" ";

                total+=this.products.get(indexProduse).getProductPrice();
            }

            if (i%2==0)
            {
                status="unpaid";
                dueDate = createRandomDueDate();
            }
            else
            {
                status="paid";
                payDate= createRandomPayDate();
            }

            indexCompanie=i;
            if(indexCompanie>companies.size()-1)
            {
                int indexComp=rnd.nextInt(companies.size());
                indexCompanie=indexComp;
            }

            String duplication="unic";
            companiePeInvoice=companies.get(indexCompanie).getNameCompany();
            total= Double.parseDouble(String.format("%.1f",total));
            Invoice invoice= new Invoice(i,companiePeInvoice,produseLocalePeInvoice,total,dueDate,payDate,status,duplication);
            invoices.add(invoice);
        }
    }

    public int createRandomIntBetween(int start, int end)
    {
        return start+(int)Math.round(Math.random()*(end-start));
    }

    public LocalDate createRandomDueDate()
    {
        int day= createRandomIntBetween(1,5);

        return LocalDate.of(2020,4,day);
    }

    public LocalDate createRandomPayDate()
    {
        int day= createRandomIntBetween(26,31);

        return LocalDate.of(2020,3,day);
    }

    public ArrayList<Invoice> getInvoices()
    {
        return invoices;
    }

    public void afisareInvoices()
    {
        for(Invoice invoice:invoices)
        {
            System.out.println(invoice.toString());
        }
    }
}
