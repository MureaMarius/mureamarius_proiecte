package generators;

import entity.Company;

import java.util.ArrayList;
import java.util.Random;

public class CompaniesGenerator
{
    private String[] firstNames;
    private String[] lastNames;
    private String[] middleNames;
    private ArrayList<Company> companies;

    private int dimensiuneGenerabila;

    public CompaniesGenerator(int dimensiuneGenerabila)
    {
        this.firstNames= new String[]{"Romanian", "European", "Italian", "Russian","American","Bulgarian","Hungarian","Moldavian"};
        this.lastNames= new String[]{"Food","Electricity","Mechanical","Cars","Computers","IT","Buildings","Beer","Clothes"};
        this.middleNames= new String[]{"Incorporated","Engineering","Integral","Unified"};
        this.companies=new ArrayList<>();

        this.dimensiuneGenerabila=dimensiuneGenerabila;
    }

    public CompaniesGenerator(String[] firstNames, String[] lastNames, String[] middleNames, int dimensiuneGenerabila)
    {
        this.firstNames=firstNames;
        this.lastNames=lastNames;
        this.middleNames=middleNames;

        this.companies=new ArrayList<>();

        this.dimensiuneGenerabila=dimensiuneGenerabila;
    }

    public CompaniesGenerator()
    {
        this.firstNames= new String[]{"Romanian", "European", "Italian", "Russian","American","Bulgarian","Hungarian","Moldavian"};
        this.lastNames= new String[]{"Food","Electricity","Mechanical","Cars","Computers","IT","Buildings","Beer","Clothes"};
        this.middleNames= new String[]{"Incorporated","Engineering","Integral","Unified"};

        this.dimensiuneGenerabila=24;
        this.companies=new ArrayList<>();
    }

    public void generateCompanies()
    {
        Random rnd= new Random();

        int i, j;
        int lenghtFirstName, lenghtLastName, lenghtMiddleName;
        int randomFirst, randomLast, randomMiddle;

        String nameCompany;
        StringBuilder stringBuilder= new StringBuilder(14);

        String digits= "12345678912345";
        String phoneNumber;

        for(i=1;i<=dimensiuneGenerabila;i++)
        {
            stringBuilder= new StringBuilder(14);

            lenghtFirstName=firstNames[i%(firstNames.length)].length();
            lenghtLastName=lastNames[i%(lastNames.length)].length();
            lenghtMiddleName=middleNames[i%(middleNames.length)].length();

            randomFirst=(rnd.nextInt(firstNames.length)+lenghtFirstName)%(firstNames.length);
            randomLast=(rnd.nextInt(lastNames.length)+lenghtLastName)%(lastNames.length);
            randomMiddle=(rnd.nextInt(middleNames.length)+lenghtMiddleName)%(middleNames.length);

            nameCompany=firstNames[randomFirst]+" "+lastNames[randomLast]+" "+middleNames[randomMiddle];

            for(j=0;j<14;j++)
            {
                int index=(int)(digits.length()*Math.random());
                stringBuilder.append(digits.charAt(index));
            }

            phoneNumber=stringBuilder.toString();
            Company company= new Company(i,nameCompany,phoneNumber);

            companies.add(company);
        }
    }

    public ArrayList<Company> getCompanies()
    {
        return companies;
    }

    public void afisareCompanii()
    {
        for(Company company:companies)
        {
            System.out.println(company.toString());
        }
    }
}
