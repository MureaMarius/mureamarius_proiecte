package generators;

import entity.Product;

import java.util.ArrayList;
import java.util.Random;

public class ProductsGenerator
{
    private ArrayList<Product> products;
    private int newDimension;

    public ProductsGenerator(int newDimension)
    {
        this.products=new ArrayList<>();
        this.newDimension=newDimension;
    }

    public ProductsGenerator()
    {
        this.products=new ArrayList<>();
        this.newDimension=48;
    }

    public void generateProducts()
    {
        Random rnd= new Random();

        double max=999.9;
        double min=0.1;

        int productNumber;
        String nameProduct;

        double priceProduct;

        int i,j;
        StringBuilder stringBuilder;
        String characters= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for(i=1;i<=newDimension;i++)
        {
            stringBuilder= new StringBuilder(5);

            for(j=0;j<5;j++)
            {
                int index=(int)(characters.length()*Math.random());
                stringBuilder.append(characters.charAt(index));
            }

            nameProduct=stringBuilder.toString();
            priceProduct=rnd.nextDouble() * (max - min)+ min;
            priceProduct=Double.parseDouble(String.format("%.1f",priceProduct));

            Product product= new Product(i,nameProduct,priceProduct);

            products.add(product);
        }
    }

    public ArrayList<Product> getProducts()
    {
        return products;
    }

    public void afisareProduse()
    {
        for(Product product: products)
        {
            System.out.println(product.toString());
        }
    }
}
