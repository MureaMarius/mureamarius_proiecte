package repository;

import entity.Company;
import entity.Invoice;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;

public class InvoiceRepo
{
    private EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("proiectnexttech");

    public void insert(Invoice invoice)
    {
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(invoice);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public ArrayList<Invoice> getAllInvoices()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Invoice> query= entityManager.createQuery("SELECT i FROM Invoice i", Invoice.class);
        ArrayList<Invoice> invoices= (ArrayList<Invoice>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return invoices;
    }

    public void deleteAll()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createQuery("DELETE FROM Invoice ");
        query.executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public ArrayList<Invoice> gettAllByDueDate()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Invoice> query= entityManager.createQuery("SELECT i FROM Invoice i ORDER BY i.dueDate DESC, i.payDate ASC", Invoice.class);
        ArrayList<Invoice> invoices= (ArrayList<Invoice>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return invoices;
    }

    public Invoice getInvoiceByID(int idInvoice)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Invoice invoice = entityManager.find(Invoice.class, idInvoice);

        entityManager.getTransaction().commit();
        entityManager.close();

        return invoice;
    }

    public void updateStatusInvoice(int idInvoice)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        LocalDate localDate= LocalDate.of(2020,4,1);
        Invoice invoice = entityManager.find(Invoice.class, idInvoice);
        invoice.setStatus("paid");
        invoice.setDueDate(null);
        invoice.setPayDate(localDate);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /*
    public ArrayList<Invoice> searchByFirstWord(String firstWord)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Invoice> query= entityManager.createQuery("SELECT i FROM Invoice i WHERE LOCATE(firstWord, i.seller)", Invoice.class);
        ArrayList<Invoice> invoices= (ArrayList<Invoice>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return invoices;
    }

     */

    public void insertIntoInvoiceTable(Invoice invoice)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(invoice);

        entityManager.getTransaction().commit();
        entityManager.close();

    }
}
