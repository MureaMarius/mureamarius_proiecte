package repository;

import entity.Company;
import entity.Product;

import javax.persistence.*;
import java.util.ArrayList;

public class ProductRepo
{
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("proiectnexttech");

    public void insert(Product product)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(product);

        entityManager.getTransaction().commit();

        entityManager.close();
    }

    public ArrayList<Product> getAllProducts()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Product> query= entityManager.createQuery("SELECT p FROM Product p", Product.class);
        ArrayList<Product> products= (ArrayList<Product>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return products;
    }

    public void deleteAll()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createQuery("DELETE FROM Product");
        query.executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
