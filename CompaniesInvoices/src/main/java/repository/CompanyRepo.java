package repository;

import entity.Company;

import javax.persistence.*;
import javax.transaction.*;
import java.util.ArrayList;

@Transactional
public class CompanyRepo
{
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("proiectnexttech");

    ///INSERAREA
    public void insert(Company company)
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(company);

        entityManager.getTransaction().commit();

        entityManager.close();
    }

    /////SELECT ALL
    public ArrayList<Company> getAllCompanies()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Company> query= entityManager.createQuery("SELECT c FROM Company c", Company.class);
        ArrayList<Company> companies= (ArrayList<Company>) query.getResultList();

        entityManager.getTransaction().commit();
        entityManager.close();

        return companies;
    }

    ////STERGEREA
    public void deleteAll()
    {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Query query = entityManager.createQuery("DELETE FROM Company ");
        query.executeUpdate();

        entityManager.getTransaction().commit();
        entityManager.close();

    }
}
