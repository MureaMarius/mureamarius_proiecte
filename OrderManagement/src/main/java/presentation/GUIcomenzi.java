/*
 * 
 */
package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLayer.Comanda;
import businessLayer.Produs;
import dataAccessLayer.AbstractDao;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.ReflectionTehnic;
import java.io.FileNotFoundException;
import java.io.PrintWriter;


// TODO: Auto-generated Javadoc
/**
 * The Class GUIcomenzi.
 */
public class GUIcomenzi extends JFrame implements ActionListener
{
	
	/** The connection. */
	ConnectionFactory connection= new ConnectionFactory();
	
	/** The dao. */
	AbstractDao dao= new AbstractDao();
	
	/** The app frame. */
	public JFrame appFrame;
	
	/** The panel. */
	public JPanel panel;
	
	/** The jlb cantitate produs. */
	public JLabel jlbComanda,jlbIdClient, jlbIdProdus, jlbNumeProdus,jlbCantitateProdus;
	
	/** The jlb afisare comenzi. */
	public JLabel jlbAfisareComenzi;
	
	/** The cantitate produs. */
	public JTextField idClient,idProdus,numeProdus, cantitateProdus;
	
	/** The btn afisare comenzi. */
	public JButton btnComanda,btnAfisareComenzi;
	
	/** The c pane. */
	public Container cPane;
	
	/**
	 * Instantiates a new GU icomenzi.
	 */
	public GUIcomenzi()
	{
		makeGUIcomenzi();
	}
	
	/**
	 * Make GU icomenzi.
	 */
	public void makeGUIcomenzi()
	{
		appFrame= new JFrame("Comenzi");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(400,300);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
	}

	/**
	 * Arrange components.
	 */
	private void arrangeComponents()
	{
		jlbComanda= new JLabel("Faceti comanda!");
		jlbComanda.setBounds(80, 25, 300, 50);
		
		jlbIdClient= new JLabel("Id comanda: ");
		jlbIdClient.setBounds(25, 50, 300, 50);
		
		idClient= new JTextField();
		idClient.setBounds(100, 65, 25, 20);
		
		jlbIdProdus= new JLabel("Id client: ");
		jlbIdProdus.setBounds(25, 75, 300, 50);
		
		idProdus= new JTextField();
		idProdus.setBounds(85, 90, 25, 20);
		
		jlbNumeProdus= new JLabel("Nume produs: ");
		jlbNumeProdus.setBounds(25, 105, 300, 50);
		
		numeProdus= new JTextField();
		numeProdus.setBounds(105, 120, 70, 20);
		
		jlbCantitateProdus= new JLabel("Cantitate produs: ");
		jlbCantitateProdus.setBounds(25, 135, 300, 50);
		
		cantitateProdus= new JTextField();
		cantitateProdus.setBounds(125, 150, 25, 20);
		
		btnComanda= new JButton("Comanda!");
		btnComanda.setBounds(50, 200, 100, 30);
		
		jlbAfisareComenzi= new JLabel("Afisare comenzi!");
		jlbAfisareComenzi.setBounds(250, 100, 300, 50);
		
		btnAfisareComenzi= new JButton("Afisare comenzi!");
		btnAfisareComenzi.setBounds(220, 140, 150, 30);
		
		cPane.add(jlbComanda);
		cPane.add(jlbIdClient);
		cPane.add(idClient);
		cPane.add(jlbIdProdus);
		cPane.add(idProdus);
		cPane.add(jlbNumeProdus);
		cPane.add(numeProdus);
		cPane.add(jlbCantitateProdus);
		cPane.add(cantitateProdus);
		cPane.add(btnComanda);
		cPane.add(jlbAfisareComenzi);
		cPane.add(btnAfisareComenzi);
		
		btnComanda.addActionListener(this);
		btnAfisareComenzi.addActionListener(this);
	}

	/**
	 * Action performed.
	 *
	 * @param e the e
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==btnComanda)
		{
			try 
			{
				comanda();
			}
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnAfisareComenzi)
		{
			try 
			{
				afisareComenzi();
			}
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}	
	}

	/**
	 * Comanda.
	 *
	 * @throws Exception the exception
	 */
	private void comanda() throws Exception
	{
		int idComanda= Integer.parseInt(idClient.getText());
		int idClt= Integer.parseInt(idProdus.getText());
		String numePrd= numeProdus.getText();
		int cantitatePrd= Integer.parseInt(cantitateProdus.getText());
		
		int cant = 0;
		double pretTotal=0;
		double pretProdus=0;
		String querySelect=dao.createSelectQuery("produse", "cantitateProdus, pretProdus", "cantitateProdus>=0 and numeProdus='"+numePrd+"'");
		
		
		Connection con=connection.getConnection();
		PreparedStatement statement= con.prepareStatement(querySelect);
		ResultSet result= statement.executeQuery();
			
		String cantitate= new String();
		String pret= new String();
			
		while(result.next())
		{
			cantitate=result.getString("cantitateProdus");
			pret=result.getString("pretProdus");
		}
		
		if(Integer.parseInt(cantitate)!=0)
		{
			cant=Integer.parseInt(cantitate);
			pretProdus=Double.parseDouble(pret);
			
			if(cantitatePrd>cant)
			{
				JFrame frameMessage= new JFrame("Warning");
				JOptionPane.showMessageDialog(frameMessage,"Nu avem atata cantitate pe stoc!"); 
			}
			else
			{
				pretTotal=cantitatePrd*pretProdus;
				cant-=cantitatePrd;
				String queryModify=dao.createEditQuery("produse", "cantitateProdus='"+cant+"'", "numeProdus='"+numePrd+"'");
				connection.editFromTable(queryModify);

				String values="('"+idComanda+"'"+", "+"'"+idClt+"','"+numePrd+"','"+cantitatePrd+"','"+pretTotal+"')";
				String query = dao.createInsertQuery("comenzi", "(idComanda, idClient, numeProdus, cantitateProdus, pretTotal)", values);
				
				createBill(idComanda,idClt,pretTotal);
				connection.insertTable(query);
			}
		}
		else
		{		
			JFrame frameMessage= new JFrame("Warning");
			JOptionPane.showMessageDialog(frameMessage,"Produsul nu mai exista pe stoc!");
		}
		
		idClient.setText(null);
		idProdus.setText(null);
		numeProdus.setText(null);
		cantitateProdus.setText(null);
	}
	
	/**
	 * Creates the bill.
	 *
	 * @param idComanda the id comanda
	 * @param idClient the id client
	 * @param sumaDePlata the suma de plata
	 */
	public void createBill(int idComanda,int idClient, double sumaDePlata)
	{	
		try
		{
			PrintWriter output= new PrintWriter("Chitanta"+idComanda+".txt");
			output.println("CHITANTA");
			
			output.println("Comanda cu nr "+idComanda+" al clientului "+idClient+" are un pret total de "+sumaDePlata+" lei!");
			output.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e);
		}
	}

	/**
	 * Afisare comenzi.
	 *
	 * @throws Exception the exception
	 */
	private void afisareComenzi() throws Exception 
	{
		ArrayList<Comanda> comenziMgz;
		comenziMgz=connection.getComanda();
		
		ReflectionTehnic reflection= new ReflectionTehnic();
		Table tabelComenzi= new Table();
		tabelComenzi.tableComenzi();
	}
}
