/*
 * 
 */
package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import businessLayer.Client;
import businessLayer.Produs;
import dataAccessLayer.AbstractDao;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.ReflectionTehnic;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIproduse.
 */
public class GUIproduse extends JFrame implements ActionListener
{
	
	/** The connection. */
	ConnectionFactory connection= new ConnectionFactory();
	
	/** The dao. */
	AbstractDao dao= new AbstractDao();
	
	/** The app frame. */
	public JFrame appFrame;
	
	/** The panel. */
	public JPanel panel;
	
	/** The jlb id delete produs. */
	public JLabel jlbInsertProdus,jlbDeleteProdus, jlbEditProdus, jlbNumeProdus, jlbIdProdus, jlbCantitateProdus, jlbPretProdus, jlbIdProdusDelete, jlbIdEditProdus, jlbAfisareProdus, jlbIdDeleteProdus;
	
	/** The jlb pret edit. */
	public JLabel jlbIdEdit, jlbCantitateEdit, jlbPretEdit;
	
	/** The pret edit. */
	public JTextField cantitateEdit, pretEdit;
	
	/** The id produs edit. */
	public JTextField numeProdus, idProdus, cantitateProdus, pretProdus,  idProdusDelete, idProdusEdit;
	
	/** The btn afisare produs. */
	public JButton btnInsertProdus, btnDeleteProdus, btnEditProdus, btnAfisareProdus;
	
	/** The c pane. */
	public Container cPane;
	
	/** The jlb produs delete. */
	private JLabel jlbProdusDelete;
	
	/**
	 * Instantiates a new GU iproduse.
	 */
	public GUIproduse()
	{
		makeGUIclienti();
	}
	
	/**
	 * Make GU iclienti.
	 */
	public void makeGUIclienti()
	{
		appFrame= new JFrame("Produse");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(500,350);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
	}
	
	/**
	 * Arrange components.
	 */
	public void arrangeComponents() 
	{
		jlbInsertProdus= new JLabel("Introduceti datele produsului!");
		jlbInsertProdus.setBounds(50, 10, 300, 50);
		
		jlbIdProdus= new JLabel("ID: ");
		jlbIdProdus.setBounds(20, 40, 300, 50);
		
		idProdus= new JTextField();
		idProdus.setBounds(45, 55, 40, 20);
		
		jlbNumeProdus= new JLabel("Nume: ");
		jlbNumeProdus.setBounds(20, 70, 300, 50);
		
		numeProdus= new JTextField();
		numeProdus.setBounds(60, 85, 100, 20);
		
		jlbCantitateProdus= new JLabel("Cantitate: ");
		jlbCantitateProdus.setBounds(20, 100, 300, 50);
		
		cantitateProdus= new JTextField();
		cantitateProdus.setBounds(75, 115, 40, 20);
		
		jlbPretProdus= new JLabel("Pret: ");
		jlbPretProdus.setBounds(20, 130, 300, 50);
		
		pretProdus= new JTextField();
		pretProdus.setBounds(50, 145, 40, 20);
		
		jlbProdusDelete= new JLabel("Stergeti produsul!");
		jlbProdusDelete.setBounds(300, 10, 300, 50);
		
		jlbIdProdusDelete= new JLabel("ID: ");
		jlbIdProdusDelete.setBounds(250, 40, 300, 50);
		
		idProdusDelete= new JTextField();
		idProdusDelete.setBounds(270, 55, 40, 20);
		
		jlbEditProdus= new JLabel("Modificati produsul!");
		jlbEditProdus.setBounds(300, 90, 300, 50);
		
		jlbIdEdit= new JLabel("ID: ");
		jlbIdEdit.setBounds(250, 115, 300, 50);
		
		idProdusEdit= new JTextField();
		idProdusEdit.setBounds(270, 130, 40, 20);
		
		jlbCantitateEdit= new JLabel("Cantitate: ");
		jlbCantitateEdit.setBounds(320, 115, 300, 50);
		
		cantitateEdit= new JTextField();
		cantitateEdit.setBounds(380,130, 40, 20);
		
		jlbPretEdit= new JLabel("Pret: ");
		jlbPretEdit.setBounds(250, 150, 300, 50);
		
		pretEdit= new JTextField();
		pretEdit.setBounds(280, 165, 40, 20);
		
		btnInsertProdus= new JButton("Insert produs!");
		btnInsertProdus.setBounds(50, 180, 150, 30);
		
		btnDeleteProdus= new JButton("Delete produs!");
		btnDeleteProdus.setBounds(330, 52, 125, 25);
		
		btnEditProdus= new JButton("Edit produs!");
		btnEditProdus.setBounds(330, 162, 115, 25);
		
		jlbAfisareProdus= new JLabel("Afisati produsele!");
		jlbAfisareProdus.setBounds(150, 250, 300, 20);
		
		btnAfisareProdus= new JButton("Afisati produsele!");
		btnAfisareProdus.setBounds(100, 270, 200, 20);
		
		cPane.add(jlbInsertProdus);
		cPane.add(jlbIdProdus);
		cPane.add(idProdus);
		cPane.add(jlbNumeProdus);
		cPane.add(numeProdus);
		cPane.add(jlbCantitateProdus);
		cPane.add(cantitateProdus);
		cPane.add(jlbPretProdus);
		cPane.add(pretProdus);
		cPane.add(jlbProdusDelete);
		cPane.add(jlbIdProdusDelete);
		cPane.add(idProdusDelete);
		cPane.add(jlbEditProdus);
		cPane.add(jlbIdEdit);
		cPane.add(idProdusEdit);
		cPane.add(jlbCantitateEdit);
		cPane.add(cantitateEdit);
		cPane.add(jlbPretEdit);
		cPane.add(pretEdit);
		cPane.add(btnInsertProdus);
		cPane.add(btnDeleteProdus);
		cPane.add(btnEditProdus);
		cPane.add(jlbAfisareProdus);
		cPane.add(btnAfisareProdus);
		
		btnInsertProdus.addActionListener(this);
		btnDeleteProdus.addActionListener(this);
		btnEditProdus.addActionListener(this);
		btnAfisareProdus.addActionListener(this);
	}

	/**
	 * Action performed.
	 *
	 * @param e the e
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==btnInsertProdus)
		{
			try 
			{
				adaugaProdus();
			} catch (Exception e1)
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnDeleteProdus)
		{
			try 
			{
				stergeProdus();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnEditProdus)
		{
			try 
			{
				modificaProdus();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnAfisareProdus)
		{
			try 
			{
				afisareProduse();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Adauga produs.
	 *
	 * @throws Exception the exception
	 */
	private void adaugaProdus() throws Exception
	{
		int id=Integer.parseInt(idProdus.getText());
		String nume=numeProdus.getText();
		int cantitate= Integer.parseInt(cantitateProdus.getText());
		double pret= Double.parseDouble(pretProdus.getText());
		
		String values="('"+id+"'"+", "+"'"+nume+"'"+",'"+cantitate+"','"+pret+"')";
		String query = dao.createInsertQuery("produse", "(idProdus,numeProdus,cantitateProdus,pretProdus)", values);
		
		connection.insertTable(query);
		System.out.println(query);
		
		idProdus.setText(null);
		numeProdus.setText(null);
		cantitateProdus.setText(null);
		pretProdus.setText(null);
	}

	/**
	 * Sterge produs.
	 *
	 * @throws Exception the exception
	 */
	private void stergeProdus() throws Exception 
	{
		int id=Integer.parseInt(idProdusDelete.getText());
		
		String condition="idProdus='"+id+"'";
		String query=dao.createDeleteQuery("produse", condition);
		
		connection.deleteFromTable(query);	
		
		idProdusDelete.setText(null);
	}

	/**
	 * Modifica produs.
	 *
	 * @throws Exception the exception
	 */
	private void modificaProdus() throws Exception 
	{
		int id= Integer.parseInt(idProdusEdit.getText());
		int cantitate= Integer.parseInt(cantitateEdit.getText());
		double pret= Double.parseDouble(pretEdit.getText());
		
		String values="cantitateProdus='"+cantitate+"',pretProdus='"+pret+"'";
		String condition="idProdus='"+id+"'";
		
		String query=dao.createEditQuery("produse", values, condition);
		
		connection.editFromTable(query);
		
		idProdusEdit.setText(null);
		cantitateEdit.setText(null);
		pretEdit.setText(null);
	}

	/**
	 * Afisare produse.
	 *
	 * @throws Exception the exception
	 */
	private void afisareProduse() throws Exception 
	{
		ArrayList<Produs> produseMgz;
		produseMgz=connection.getProdus();
		
		ReflectionTehnic reflection= new ReflectionTehnic();
		
		JTable tabelProduse;
		Table tabel= new Table();
		
		JFrame frameClient = new JFrame();
		frameClient.setTitle("Tabel produse");
		
		tabelProduse=tabel.createTableProduse();
		JScrollPane scrollPane= new JScrollPane(tabelProduse);
		frameClient.add(scrollPane);
		frameClient.setSize(500, 200);
		frameClient.setVisible(true);
	}
}
