/*
 * 
 */
package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dataAccessLayer.ConnectionFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class GUImenu.
 */
public class GUImenu extends JFrame implements ActionListener
{
	
	/** The connection. */
	ConnectionFactory connection= new ConnectionFactory();
	
	/** The app frame. */
	public JFrame appFrame;
	
	/** The panel. */
	public JPanel panel;
	
	/** The jlb tabel comenzi. */
	public JLabel jlbTabelClienti,jlbTabelProduse,jlbTabelComenzi;
	
	/** The comenzi. */
	public JButton btnClienti,btnProduse,btnComenzi, clienti, produse, comenzi;
	
	/** The c pane. */
	public Container cPane;
	
	
	/**
	 * Instantiates a new GU imenu.
	 */
	public GUImenu()
	{
		makeGUImenu();
	}
	
	/**
	 * Make GU imenu.
	 */
	public void makeGUImenu()
	{
		appFrame= new JFrame("Meniu Principal");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(500,410);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
	}

	/**
	 * Arrange components.
	 */
	public void arrangeComponents() 
	{
		jlbTabelClienti= new JLabel("Tabel clienti: ");
		jlbTabelClienti.setBounds(25, 25, 200, 100);
		
		jlbTabelProduse= new JLabel("Tabel produse: ");
		jlbTabelProduse.setBounds(25, 75, 200, 100);
		
		jlbTabelComenzi= new JLabel("Tabel comenzi: ");
		jlbTabelComenzi.setBounds(25, 125, 200, 100);
		
		btnClienti= new JButton("Creare tabel clienti!");
		btnClienti.setBounds(100, 60, 200, 30);
		
		btnProduse= new JButton("Creare tabel produse!");
		btnProduse.setBounds(115, 110, 200, 30);
		
		btnComenzi= new JButton("Creare tabel comenzi!");
		btnComenzi.setBounds(115, 160, 200, 30);
		
		clienti= new JButton("CLIENTI");
		clienti.setBounds(25, 250, 100, 100);
		
		produse= new JButton("PRODUSE");
		produse.setBounds(175, 250, 100, 100);
		
		comenzi= new JButton("COMENZI");
		comenzi.setBounds(325, 250, 100, 100);
		
		
		cPane.add(jlbTabelClienti);
		cPane.add(jlbTabelProduse);
		cPane.add(jlbTabelComenzi);
		cPane.add(btnClienti);
		cPane.add(btnProduse);
		cPane.add(btnComenzi);
		cPane.add(clienti);
		cPane.add(produse);
		cPane.add(comenzi);
		
		btnClienti.addActionListener(this);
		btnProduse.addActionListener(this);
		btnComenzi.addActionListener(this);
		clienti.addActionListener(this);
		produse.addActionListener(this);
		comenzi.addActionListener(this);
	}

	/**
	 * Action performed.
	 *
	 * @param e the e
	 */
	public void actionPerformed(ActionEvent e) 
	{
		
		try 
		{
			if(e.getSource()==btnClienti)
			{
				createTableClienti();
			}
			if(e.getSource()==btnProduse)
			{
				createTableProduse();
			}
			if(e.getSource()==btnComenzi)
			{
				createTableComenzi();
			}
			if(e.getSource()==clienti)
			{
				new GUIclienti();
			}
			if(e.getSource()==produse)
			{
				new GUIproduse();
			}
			if(e.getSource()==comenzi)
			{
				new GUIcomenzi();
			}
		} 
		catch (Exception e1) 
		{
			e1.printStackTrace();
		}
	}

	/**
	 * Creates the table comenzi.
	 *
	 * @throws Exception the exception
	 */
	public void createTableComenzi() throws Exception 
	{
		String tableComenzi="CREATE TABLE IF NOT EXISTS comenzi(idComanda int NOT NULL, idClient int, numeProdus varchar(255), cantitateProdus int, pretTotal DOUBLE(16,2), PRIMARY KEY(idComanda))";
		connection.createTable(tableComenzi);
	}

	/**
	 * Creates the table clienti.
	 *
	 * @throws Exception the exception
	 */
	public void createTableClienti() throws Exception 
	{
		String tableClienti="CREATE TABLE IF NOT EXISTS clienti(idClient int NOT NULL, numeClient varchar(255), PRIMARY KEY(idClient))";
		connection.createTable(tableClienti);
	}

	/**
	 * Creates the table produse.
	 *
	 * @throws Exception the exception
	 */
	public void createTableProduse() throws Exception 
	{		
		String tableProduse="CREATE TABLE IF NOT EXISTS produse(idProdus int NOT NULL, numeProdus varchar(255), cantitateProdus int, pretProdus DOUBLE(16,2), PRIMARY KEY(idProdus))";
		connection.createTable(tableProduse);
	}
}
