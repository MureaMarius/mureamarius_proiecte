/*
 * 
 */
package presentation;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.Client;
import businessLayer.Comanda;
import businessLayer.Produs;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.ReflectionTehnic;

// TODO: Auto-generated Javadoc
/**
 * The Class Table.
 */
public class Table extends JFrame
{
	ConnectionFactory connection= new ConnectionFactory();
	/** The table client. */
	JTable tableClient;
	
	/** The table produs. */
	JTable tableProdus;
	
	/** The table comanda. */
	JTable tableComanda;
	
	/** The frame client. */
	JFrame frameClient;
	
	/** The frame produs. */
	JFrame frameProdus;
	
	/** The frame comanda. */
	JFrame frameComanda;
	
	JTable table;
		
	/**
	 * Instantiates a new table.
	 */
	public Table()
	{
		
	}
	
	/**
	 * Table clienti.
	 *
	 * @param dataClienti the data clienti
	 * @throws Exception 
	 */
	
	public JTable createTableClienti() throws Exception
	{
		DefaultTableModel tableModel = new DefaultTableModel(null,ReflectionTehnic.retrieveHeader(new Client()));
		tableClient= new JTable(tableModel);
		tableClient.setBounds(30, 40, 300, 300);
		
		ArrayList<Client> clientiMgz;
		clientiMgz=connection.getClient();
		
		Vector<Object> data;
		for(Client client: clientiMgz)
		{
			data = ReflectionTehnic.retrieveProperties(client);
			tableModel.addRow(data);
		}
		return tableClient;
	}
	
	public JTable createTableProduse() throws Exception
	{
		DefaultTableModel tableModel = new DefaultTableModel(null,ReflectionTehnic.retrieveHeader(new Produs()));
		tableProdus= new JTable(tableModel);
		tableProdus.setBounds(30, 40, 300, 300);
		
		ArrayList<Produs> produseMgz;
		produseMgz=connection.getProdus();
		
		Vector<Object> data;
		for(Produs produs: produseMgz)
		{
			data = ReflectionTehnic.retrieveProperties(produs);
			tableModel.addRow(data);
		}
		return tableProdus;
	}
	
	/*public void tableClienti() throws Exception
	{
		frameClient= new JFrame();
		frameClient.setTitle("Tabel clienti");
		
		DefaultTableModel tableModel = new DefaultTableModel(null,ReflectionTehnic.retrieveHeader(new Client()));
		tableClient= new JTable(tableModel);
		tableClient.setBounds(30, 40, 300, 300);
		
		ArrayList<Client> clientiMgz;
		clientiMgz=connection.getClient();
		
		Vector<Object> data;
		for(Client client: clientiMgz)
		{
			data = ReflectionTehnic.retrieveProperties(client);
			tableModel.addRow(data);
		}
		
		
		
		JScrollPane scrollPane= new JScrollPane(tableClient);
		frameClient.add(scrollPane);
		frameClient.setSize(500, 200);
		frameClient.setVisible(true);
	}*/
	
	/**
	 * Table produse.
	 *
	 * @param dataProduse the data produse
	 * @throws Exception 
	 */
	/*public void tableProduse() throws Exception
	{
		frameProdus= new JFrame();
		frameProdus.setTitle("Tabel produse");
		
		DefaultTableModel tableModel = new DefaultTableModel(null,ReflectionTehnic.retrieveHeader(new Produs()));
		tableProdus= new JTable(tableModel);
		tableProdus.setBounds(30, 40, 300, 300);
		
		ArrayList<Produs> produseMgz;
		produseMgz=connection.getProdus();
		
		Vector<Object> data;
		for(Produs produs: produseMgz)
		{
			data = ReflectionTehnic.retrieveProperties(produs);
			tableModel.addRow(data);
		}
		
		JScrollPane scrollPane= new JScrollPane(tableProdus);
		frameProdus.add(scrollPane);
		frameProdus.setSize(500, 200);
		frameProdus.setVisible(true);
	}*/
	
	/**
	 * Table comenzi.
	 *
	 * @param dataComenzi the data comenzi
	 * @throws Exception 
	 */
	public void tableComenzi() throws Exception
	{
		frameComanda= new JFrame();
		frameComanda.setTitle("Tabel comenzi");
		
		DefaultTableModel tableModel = new DefaultTableModel(null,ReflectionTehnic.retrieveHeader(new Comanda()));
		tableComanda= new JTable(tableModel);
		tableComanda.setBounds(30, 40, 300, 300);
		
		ArrayList<Comanda> comenziMgz;
		comenziMgz=connection.getComanda();
		
		Vector<Object> data;
		for(Comanda comanda: comenziMgz)
		{
			data = ReflectionTehnic.retrieveProperties(comanda);
			tableModel.addRow(data);
		}
		
		JScrollPane scrollPane= new JScrollPane(tableComanda);
		frameComanda.add(scrollPane);
		frameComanda.setSize(500, 200);
		frameComanda.setVisible(true);
	}
}
