/*
 * 
 */
package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import businessLayer.Client;

import dataAccessLayer.AbstractDao;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.ReflectionTehnic;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIclienti.
 */
public class GUIclienti extends JFrame implements ActionListener
{
	
	/** The connection. */
	ConnectionFactory connection= new ConnectionFactory();
	
	/** The dao. */
	AbstractDao dao= new AbstractDao();
	
	/** The app frame. */
	public JFrame appFrame;
	
	/** The panel. */
	public JPanel panel;
	
	/** The jlb afisare clienti. */
	public JLabel jlbInsertClient,jlbDeleteClient, jlbEditClient, jlbNumeClient, jlbIdClient, jlbIdClientDelete, jlbIdEditClient, jlbAfisareClienti;
	
	/** The id client edit. */
	public JTextField numeClient, idClient, idClientDelete, idClientEdit;
	
	/** The btn afisare clienti. */
	public JButton btnInsertClient,btnDeleteClient, btnEditClient, btnAfisareClienti;
	
	/** The c pane. */
	public Container cPane;
	
	/**
	 * Instantiates a new GU iclienti.
	 */
	public GUIclienti()
	{
		makeGUIclienti();
	}
	
	/**
	 * Make GU iclienti.
	 */
	public void makeGUIclienti()
	{
		appFrame= new JFrame("Clienti");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(500,350);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
	}
	
	/**
	 * Arrange components.
	 */
	public void arrangeComponents() 
	{
		jlbInsertClient= new JLabel("Introduceti datele clientului!");
		jlbInsertClient.setBounds(100, 10, 300, 50);
		
		jlbIdClient= new JLabel("ID: ");
		jlbIdClient.setBounds(35, 45, 100, 50);
		
		jlbNumeClient= new JLabel("Nume: ");
		jlbNumeClient.setBounds(130, 45, 100, 50);
		
		numeClient= new JTextField();
		numeClient.setBounds(175, 58, 100, 25);
		
		idClient= new JTextField();
		idClient.setBounds(60, 58, 30, 25);
		
		btnInsertClient= new JButton("Insert Client!");
		btnInsertClient.setBounds(300, 57, 150, 25);
		
		jlbDeleteClient= new JLabel("Stergeti clientul!");
		jlbDeleteClient.setBounds(100, 85, 300, 50);
		
		jlbIdClientDelete= new JLabel("ID: ");
		jlbIdClientDelete.setBounds(35, 110, 100, 50);
		
		idClientDelete= new JTextField();
		idClientDelete.setBounds(60, 125, 30, 25);
		
		btnDeleteClient= new JButton("Delete Client!");
		btnDeleteClient.setBounds(300, 124, 150, 25);
		
		jlbEditClient= new JLabel("Modificati clientul!");
		jlbEditClient.setBounds(100, 150, 300, 50);
		
		jlbIdEditClient= new JLabel("ID: ");
		jlbIdEditClient.setBounds(35, 180, 100, 50);
		
		idClientEdit= new JTextField();
		idClientEdit.setBounds(60, 193, 30, 25);
		
		btnEditClient= new JButton("Edit Client!");
		btnEditClient.setBounds(300, 192, 150, 25);
		
		jlbAfisareClienti= new JLabel("Afisati toti clientii!");
		jlbAfisareClienti.setBounds(150, 210, 300, 50);
		
		btnAfisareClienti= new JButton("Afisati clientii!");
		btnAfisareClienti.setBounds(125, 250, 150, 25);
			
		cPane.add(jlbInsertClient);
		cPane.add(jlbIdClient);
		cPane.add(jlbNumeClient);
		cPane.add(numeClient);
		cPane.add(idClient);
		cPane.add(btnInsertClient);
		cPane.add(jlbDeleteClient);
		cPane.add(jlbIdClientDelete);
		cPane.add(idClientDelete);
		cPane.add(btnDeleteClient);
		cPane.add(jlbEditClient);
		cPane.add(jlbIdEditClient);
		cPane.add(idClientEdit);
		cPane.add(btnEditClient);
		cPane.add(jlbAfisareClienti);
		cPane.add(btnAfisareClienti);
		
		btnAfisareClienti.addActionListener(this);
		btnEditClient.addActionListener(this);
		btnDeleteClient.addActionListener(this);
		btnInsertClient.addActionListener(this);
	}

	/**
	 * Action performed.
	 *
	 * @param e the e
	 */
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==btnInsertClient)
		{
			try 
			{
				adaugaClient();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnDeleteClient)
		{
			try 
			{
				stergeClient();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnEditClient)
		{
			try 
			{
				modificaClient();
			}
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		if(e.getSource()==btnAfisareClienti)
		{
			try 
			{
				afisareClienti();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Adauga client.
	 *
	 * @throws Exception the exception
	 */
	public void adaugaClient() throws Exception
	{
		Client client;
		
		int id=Integer.parseInt(idClient.getText());
		String nume=numeClient.getText();
		
		String values="('"+id+"'"+", "+"'"+nume+"')";
		String query = dao.createInsertQuery("clienti", "(idClient, numeClient)", values);
		
		connection.insertTable(query);
		System.out.println(query);
		
		idClient.setText(null);
		numeClient.setText(null);
	}
	
	/**
	 * Sterge client.
	 *
	 * @throws Exception the exception
	 */
	public void stergeClient() throws Exception
	{	
		int id=Integer.parseInt(idClientDelete.getText());
		
		String condition="idClient='"+id+"'";
		String query=dao.createDeleteQuery("clienti", condition);
		connection.deleteFromTable(query);
		
		idClientDelete.setText(null);
	}
	
	/**
	 * Modifica client.
	 *
	 * @throws Exception the exception
	 */
	public void modificaClient() throws Exception
	{
		Random ran= new Random();
		
		int id= Integer.parseInt(idClientEdit.getText());
		
		int idEdit=Math.abs(ran.nextInt()%20);
		
		String values="idClient='"+idEdit+"'";
		String condition="idClient='"+id+"'";
		String query=dao.createEditQuery("clienti", values, condition);
		
		connection.editFromTable(query);
		
		idClientEdit.setText(null);
	}
	
	/**
	 * Afisare clienti.
	 *
	 * @throws Exception the exception
	 */
	public void afisareClienti() throws Exception
	{
		ArrayList<Client> clientiMgz;
		clientiMgz=connection.getClient();
		
		ReflectionTehnic reflection= new ReflectionTehnic();
		
		JTable tabelClienti;
		Table tabel= new Table();
		
		JFrame frameClient = new JFrame();
		frameClient.setTitle("Tabel clienti");
		
		tabelClienti=tabel.createTableClienti();
		JScrollPane scrollPane= new JScrollPane(tabelClienti);
		frameClient.add(scrollPane);
		frameClient.setSize(500, 200);
		frameClient.setVisible(true);
	}
}