/*
 * 
 */
package dataAccessLayer;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractDao.
 */
public class AbstractDao
{
	
	/**
	 * Creates the insert query.
	 *
	 * @param numeTabel the nume tabel
	 * @param columns the columns
	 * @param values the values
	 * @return the string
	 */
	public String createInsertQuery(String numeTabel, String columns, String values)
	{
		StringBuilder sb= new StringBuilder();
		
		sb.append("INSERT ");
		sb.append("INTO ");
		sb.append(numeTabel+" ");
		sb.append(columns+" ");
		sb.append("VALUES ");
		sb.append(values);
		
		return sb.toString();
	}
	
	/**
	 * Creates the delete query.
	 *
	 * @param numeTabel the nume tabel
	 * @param condition the condition
	 * @return the string
	 */
	public String createDeleteQuery(String numeTabel, String condition)
	{
		StringBuilder sb= new StringBuilder();
		
		sb.append("DELETE FROM ");
		sb.append(numeTabel);
		sb.append(" WHERE ");
		sb.append(condition);
		
		return sb.toString();
	}
	
	/**
	 * Creates the edit query.
	 *
	 * @param numeTabel the nume tabel
	 * @param values the values
	 * @param condition the condition
	 * @return the string
	 */
	public String createEditQuery(String numeTabel, String values, String condition)
	{
		StringBuilder sb= new StringBuilder();
		
		sb.append("UPDATE ");
		sb.append(numeTabel);
		sb.append(" SET ");
		sb.append(values);
		sb.append(" WHERE ");
		sb.append(condition);
		
		return sb.toString();
	}
	
	/**
	 * Creates the select query.
	 *
	 * @param numeTabel the nume tabel
	 * @param values the values
	 * @param condition the condition
	 * @return the string
	 */
	public String createSelectQuery(String numeTabel, String values, String condition)
	{
		StringBuilder sb= new StringBuilder();
		
		sb.append("SELECT ");
		sb.append(values);
		sb.append(" FROM ");
		sb.append(numeTabel);
		sb.append(" WHERE ");
		sb.append(condition);
		
		return sb.toString();
	}
}
