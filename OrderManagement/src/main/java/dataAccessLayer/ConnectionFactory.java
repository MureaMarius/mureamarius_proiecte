/*
 * 
 */
package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

import businessLayer.Client;
import businessLayer.Comanda;
import businessLayer.Produs;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Connection objects.
 */
public class ConnectionFactory
{
	
	/** The driver. */
	private static String DRIVER=null;
	
	/** The dburl. */
	private static String DBURL=null;
	
	/**
	 * Creates a new Connection object.
	 *
	 * @param table the table
	 * @throws Exception the exception
	 */
	public void createTable(String table) throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement create= con.prepareStatement(table);
			create.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			System.out.println("Function complete!");
		}
	}
	
	/**
	 * Insert table.
	 *
	 * @param query the query
	 * @throws Exception the exception
	 */
	public void insertTable(String query) throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement insert= con.prepareStatement(query);
			
			insert.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			System.out.println("Insert completed!");
		}
	}
	
	/**
	 * Delete from table.
	 *
	 * @param query the query
	 * @throws Exception the exception
	 */
	public void deleteFromTable(String query) throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement deleteFromTable= con.prepareStatement(query);
			
			deleteFromTable.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			System.out.println("Delete completed!");
		}
	}
	
	/**
	 * Edits the from table.
	 *
	 * @param query the query
	 * @throws Exception the exception
	 */
	public void editFromTable(String query) throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement edit=con.prepareStatement(query);
			
			edit.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		finally
		{
			System.out.println("Edit completed!");
		}
	}
	
	/**
	 * Gets the client.
	 *
	 * @return the client
	 * @throws Exception the exception
	 */
	public ArrayList<Client> getClient() throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM clienti");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Client> clienti= new ArrayList<Client>();
		
			while(result.next())
			{
				int id=Integer.parseInt(result.getString("idClient"));
				String nume= result.getString("numeClient");
				
				Client client= new Client(nume,id);
				clienti.add(client);
				client.toString();
			}
			
			System.out.println("All records have been selected!");
			return clienti;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	
	/**
	 * Gets the produs.
	 *
	 * @return the produs
	 * @throws Exception the exception
	 */
	public ArrayList<Produs> getProdus() throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM produse");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Produs> produse= new ArrayList<Produs>();
		
			while(result.next())
			{
				int id=Integer.parseInt(result.getString("idProdus"));
				String nume= result.getString("numeProdus");
				int cantitate=Integer.parseInt(result.getString("cantitateProdus"));
				double pret= Double.parseDouble(result.getString("pretProdus"));
				
				Produs produs= new Produs(id,nume,cantitate,pret);
				produse.add(produs);

			}	
			System.out.println("All records have been selected!");
			return produse;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	
	/**
	 * Gets the comanda.
	 *
	 * @return the comanda
	 * @throws Exception the exception
	 */
	public ArrayList<Comanda> getComanda() throws Exception
	{
		try
		{
			Connection con= getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM comenzi");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Comanda> comenzi= new ArrayList<Comanda>();
		
			while(result.next())
			{
				int idComanda=Integer.parseInt(result.getString("idComanda"));
				int idClient= Integer.parseInt(result.getString("idClient"));
				String numeProdus= result.getString("numeProdus");
				int cantitate=Integer.parseInt(result.getString("cantitateProdus"));
				double pretTotal=Double.parseDouble(result.getString("pretTotal"));
				
				Comanda comanda= new Comanda(idComanda,idClient,numeProdus,cantitate,pretTotal);
				comenzi.add(comanda);

			}	
			System.out.println("All records have been selected!");
			return comenzi;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	
	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 * @throws Exception the exception
	 */
	public Connection getConnection() throws Exception
	{
		try
		{
			DRIVER="com.mysql.cj.jdbc.Driver";
			DBURL="jdbc:mysql://localhost:3306/warehouse";
			String username="root";
			String password="";
			Class.forName(DRIVER);
			
			Connection conn=DriverManager.getConnection(DBURL, username, password);
			System.out.println("Connected!");
			return conn;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
}
