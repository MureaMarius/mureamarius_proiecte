/*
 * 
 */
package businessLayer;

// TODO: Auto-generated Javadoc
/**
 * The Class Client.
 */
public class Client 
{
	
	/** The nume client. */
	protected String numeClient;
	
	/** The id client. */
	protected int idClient;
	
	/**
	 * Instantiates a new client.
	 *
	 * @param numeClient the nume client
	 * @param idClient the id client
	 */
	public Client(String numeClient, int idClient)
	{
		this.numeClient=numeClient;
		this.idClient=idClient;
	}

	public Client()
	{
		
	}
	/**
	 * Gets the nume client.
	 *
	 * @return the nume client
	 */
	public String getNumeClient() 
	{
		return numeClient;
	}

	/**
	 * Sets the nume client.
	 *
	 * @param numeClient the new nume client
	 */
	public void setNumeClient(String numeClient) 
	{
		this.numeClient = numeClient;
	}

	/**
	 * Gets the id client.
	 *
	 * @return the id client
	 */
	public String getIdClient() 
	{
		return String.valueOf(idClient);
	}

	/**
	 * Sets the id client.
	 *
	 * @param idClient the new id client
	 */
	public void setIdClient(int idClient)
	{
		this.idClient = idClient;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString()
	{
		return idClient+" "+numeClient;
	}
}