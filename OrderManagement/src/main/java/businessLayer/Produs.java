package businessLayer;

// TODO: Auto-generated Javadoc
/**
 * The Class Produs.
 */
public class Produs 
{
	
	/** The id produs. */
	protected int idProdus;
	
	/** The nume produs. */
	protected String numeProdus;
	
	/** The cantitate produs. */
	protected int cantitateProdus;
	
	/** The pret produs. */
	protected double pretProdus;
	
	/**
	 * Instantiates a new produs.
	 *
	 * @param idProdus the id produs
	 * @param numeProdus the nume produs
	 * @param cantitateProdus the cantitate produs
	 * @param pretProdus the pret produs
	 */
	public Produs()
	{
		
	}
	
	public Produs(int idProdus,String numeProdus,int cantitateProdus,double pretProdus)
	{
		this.idProdus=idProdus;
		this.numeProdus=numeProdus;
		this.cantitateProdus=cantitateProdus;
		this.pretProdus=pretProdus;
	}

	/**
	 * Gets the id produs.
	 *
	 * @return the id produs
	 */
	public String getIdProdus() 
	{
		return String.valueOf(idProdus);
	}

	/**
	 * Sets the id produs.
	 *
	 * @param idProdus the new id produs
	 */
	public void setIdProdus(int idProdus) 
	{
		this.idProdus = idProdus;
	}

	/**
	 * Gets the nume produs.
	 *
	 * @return the nume produs
	 */
	public String getNumeProdus()
	{
		return numeProdus;
	}

	/**
	 * Sets the nume produs.
	 *
	 * @param numeProdus the new nume produs
	 */
	public void setNumeProdus(String numeProdus) 
	{
		this.numeProdus = numeProdus;
	}

	/**
	 * Gets the cantitate produs.
	 *
	 * @return the cantitate produs
	 */
	public String getCantitateProdus() 
	{
		return String.valueOf(cantitateProdus);
	}

	/**
	 * Sets the cantitate produs.
	 *
	 * @param cantitateProdus the new cantitate produs
	 */
	public void setCantitateProdus(int cantitateProdus) 
	{
		this.cantitateProdus = cantitateProdus;
	}

	/**
	 * Gets the pret produs.
	 *
	 * @return the pret produs
	 */
	public String getPretProdus() 
	{
		return String.valueOf(pretProdus);
	}

	/**
	 * Sets the pret produs.
	 *
	 * @param pretProdus the new pret produs
	 */
	public void setPretProdus(double pretProdus) 
	{
		this.pretProdus = pretProdus;
	}
}