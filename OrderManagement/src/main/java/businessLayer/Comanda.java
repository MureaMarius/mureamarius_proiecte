package businessLayer;

// TODO: Auto-generated Javadoc
/**
 * The Class Comanda.
 */
public class Comanda 
{
	
	/** The id comanda. */
	protected int idComanda;
	
	/** The id client. */
	protected int idClient;
	
	/** The nume produs. */
	protected String numeProdus;
	
	/** The cantitate produs. */
	protected int cantitateProdus;
	
	/** The pret total. */
	protected double pretTotal;
	
	/**
	 * Instantiates a new comanda.
	 *
	 * @param idComanda the id comanda
	 * @param idClient the id client
	 * @param numeProdus the nume produs
	 * @param cantitateProdus the cantitate produs
	 * @param pretTotal the pret total
	 */
	public Comanda(int idComanda,int idClient, String numeProdus, int cantitateProdus,double pretTotal)
	{
		this.idComanda=idComanda;
		this.idClient=idClient;
		this.numeProdus=numeProdus;
		this.cantitateProdus=cantitateProdus;
		this.pretTotal=pretTotal;
	}
	
	public Comanda()
	{
		
	}

	/**
	 * Gets the id comanda.
	 *
	 * @return the id comanda
	 */
	public String getIdComanda() 
	{
		return String.valueOf(idComanda);
	}

	/**
	 * Sets the id comanda.
	 *
	 * @param idComanda the new id comanda
	 */
	public void setIdComanda(int idComanda) 
	{
		this.idComanda = idComanda;
	}

	/**
	 * Gets the id client.
	 *
	 * @return the id client
	 */
	public String getIdClient()
	{
		return String.valueOf(idClient);
	}

	/**
	 * Gets the pret total.
	 *
	 * @return the pret total
	 */
	public String getPretTotal() 
	{
		return String.valueOf(pretTotal);
	}

	/**
	 * Sets the pret total.
	 *
	 * @param pretTotal the new pret total
	 */
	public void setPretTotal(double pretTotal) 
	{
		this.pretTotal = pretTotal;
	}

	/**
	 * Sets the id client.
	 *
	 * @param idClient the new id client
	 */
	public void setIdClient(int idClient) 
	{
		this.idClient = idClient;
	}

	/**
	 * Gets the nume produs.
	 *
	 * @return the nume produs
	 */
	public String getNumeProdus() 
	{
		return numeProdus;
	}

	/**
	 * Sets the nume produs.
	 *
	 * @param numeProdus the new nume produs
	 */
	public void setNumeProdus(String numeProdus) 
	{
		this.numeProdus = numeProdus;
	}

	/**
	 * Gets the cantitate produs.
	 *
	 * @return the cantitate produs
	 */
	public String getCantitateProdus()
	{
		return String.valueOf(cantitateProdus);
	}

	/**
	 * Sets the cantitate produs.
	 *
	 * @param cantitateProdus the new cantitate produs
	 */
	public void setCantitateProdus(int cantitateProdus) 
	{
		this.cantitateProdus = cantitateProdus;
	}
	
	
}
