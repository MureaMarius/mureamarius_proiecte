package PT2019_Assignment_1.Assignment_1.Assignment_1;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUI extends JFrame implements ActionListener
{
	JFrame appFrame;
	JPanel panel;
	JLabel jlbFirstPolinom, jlbSecondPolinom, jlbResult; 
	JTextField polinom1,polinom2,rezultat;
	JButton jbtAdunare, jbtScadere, jbtInmultire, jbtImpartire, jbtDerivare, jbtIntegrare, jbtClear;
	Container cPane;
	
	public GUI()
	{
		makeGUI();
	}
	
	public void makeGUI()
	{
		appFrame = new JFrame("Calculator Polinoame");
   		cPane = appFrame.getContentPane();
   		cPane.setLayout(null);
   		appFrame.setLayout(null);

   		arrangeComponents();
   		
   		appFrame.pack();
   		appFrame.setSize(700, 500);
   		appFrame.setLocation(500,300);
   		appFrame.setVisible(true);
	}
	
	public void arrangeComponents()
	{
		jlbFirstPolinom= new JLabel("Primul polinom");
		jlbFirstPolinom.setBounds(130, 25, 200, 100);
		
		jlbSecondPolinom= new JLabel("Al doilea polinom");
		jlbSecondPolinom.setBounds(125, 75, 200, 100);
		
		jlbResult= new JLabel("Rezultatul");
		jlbResult.setBounds(150, 140, 200, 100);
		
		polinom1= new JTextField();
		polinom1.setBounds(250,50,350,40);
		
		polinom2= new JTextField();
		polinom2.setBounds(250, 110, 350, 40);
		
		rezultat= new JTextField();
		rezultat.setBounds(250, 170, 350, 40);
		
		jbtAdunare= new JButton("Adunare");
		jbtAdunare.setBounds(50, 250, 100, 50);
		
		jbtScadere= new JButton("Scadere");
		jbtScadere.setBounds(50, 320, 100, 50);
		
		jbtInmultire= new JButton("Inmultire");
		jbtInmultire.setBounds(200, 249, 100, 50);
		
		jbtImpartire= new JButton("Impartire");
		jbtImpartire.setBounds(200, 319, 100, 50);
		
		jbtDerivare= new JButton("Derivare");
		jbtDerivare.setBounds(350, 249, 100, 50);
		
		jbtIntegrare= new JButton("Integrare");
		jbtIntegrare.setBounds(350, 319, 100, 50);
		
		jbtClear= new JButton("Clear");
		jbtClear.setBounds(500, 285, 100, 50);
		
		cPane.add(jlbFirstPolinom);
		cPane.add(jlbSecondPolinom);
		cPane.add(jlbResult);
		
		cPane.add(polinom1);
		cPane.add(polinom2);
		cPane.add(rezultat);
		
		cPane.add(jbtAdunare);
		cPane.add(jbtScadere);
		cPane.add(jbtInmultire);
		cPane.add(jbtImpartire);
		cPane.add(jbtDerivare);
		cPane.add(jbtIntegrare);
		cPane.add(jbtClear);
		
		jbtAdunare.addActionListener(this);	
		jbtScadere.addActionListener(this);
		jbtInmultire.addActionListener(this);
		jbtDerivare.addActionListener(this);
		jbtIntegrare.addActionListener(this);
		jbtImpartire.addActionListener(this);
		jbtClear.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jbtAdunare)
		{
			adunaPolinoame();
		}
		else if(e.getSource()==jbtScadere)
		{
			scaderePolinoame();
		}
		else if(e.getSource()==jbtInmultire)
		{
			inmultirePolinoame();
		}
		else if(e.getSource()==jbtDerivare)
		{
			derivarePolinom();
		}
		else if(e.getSource()==jbtImpartire)
		{
			impartirePolinoame();
		}
		else if(e.getSource()==jbtIntegrare)
		{
			integrarePolinom();
		}
		else if(e.getSource() == jbtClear)
		{
			clearTextFielduri();
		}
		
	}

	private void impartirePolinoame() 
	{
		String primulPolinom= polinom1.getText();
		String secondPolinom= polinom2.getText();
		
		Polinom p= new Polinom(primulPolinom);
		Polinom p1= new Polinom(secondPolinom);
		
		int OK1=p.validarePolinom();
		int OK2=p1.validarePolinom();
		
		if(OK1==1 || OK2==1)
		{
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			String rezultat= p.impartire(p1);
			this.rezultat.setText(rezultat);
		}
		
	}

	private void clearTextFielduri() 
	{
		this.polinom1.setText(" ");
		this.polinom2.setText(" ");
		this.rezultat.setText(" ");
	}

	private void integrarePolinom() 
	{
		String polinom= polinom1.getText();
		Polinom p= new Polinom(polinom);
		int OK=p.validarePolinom();
		if(OK==1)
			{
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
			}
		else
			{
				String rezultat= p.integrare();
				this.rezultat.setText(rezultat);
			}
	}

	private void derivarePolinom() 
	{
		String polinom= polinom1.getText();
		Polinom p= new Polinom(polinom);
		int OK=p.validarePolinom();
		if(OK==1)
			{
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
			}
		else
			{
				String rezultat= p.derivare();
				this.rezultat.setText(rezultat);
			}
	}

	private void inmultirePolinoame() 
	{
		String primulPolinom= polinom1.getText();
		String secondPolinom= polinom2.getText();
		
		Polinom p= new Polinom(primulPolinom);
		Polinom p1= new Polinom(secondPolinom);
		
		int OK1=p.validarePolinom();
		int OK2=p1.validarePolinom();
		
		if(OK1==1 || OK2==1)
		{
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			String rezultat= p.inmultire(p1);
			this.rezultat.setText(rezultat);
		}
	}

	private void scaderePolinoame() 
	{
		String primulPolinom= polinom1.getText();
		String secondPolinom= polinom2.getText();
		
		Polinom p= new Polinom(primulPolinom);
		Polinom p1= new Polinom(secondPolinom);
		
		int OK1=p.validarePolinom();
		int OK2=p1.validarePolinom();
		
		if(OK1==1 || OK2==1)
		{
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			String rezultat= p.scadere(p1);
			this.rezultat.setText(rezultat);
		}
	}

	private void adunaPolinoame() 
	{		
		String primulPolinom= polinom1.getText();
		String secondPolinom= polinom2.getText();
		
		Polinom p= new Polinom(primulPolinom);
		Polinom p1= new Polinom(secondPolinom);
		
		int OK1=p.validarePolinom();
		int OK2=p1.validarePolinom();
		
		if(OK1==1 || OK2==1)
		{
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "Polinom invalid", "Eroare", JOptionPane.ERROR_MESSAGE);
		}
		else
		{
			String rezultat= p.adunare(p1,1);
			this.rezultat.setText(rezultat);
		}
	}
}