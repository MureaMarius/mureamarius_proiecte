package PT2019_Assignment_1.Assignment_1.Assignment_1;

public class Monom implements Comparable
{
	protected double coeficient;
	protected int grad;
	
	public Monom()
	{
		this.coeficient=0;
		this.grad=0;
	}
	
	public Monom(int coeficient,int grad)
	{
		this.coeficient=coeficient;
		this.grad=grad;
	}
	
	public Monom adunare(Monom m)
	{
		Monom rezultat= new Monom();
		
		if(this.grad==m.grad)
		{
			rezultat.coeficient=this.coeficient+m.coeficient;
			rezultat.grad=this.grad;
		}
		
		return rezultat;
	}
	
	public Monom scadere(Monom m)
	{
		Monom rezultat= new Monom();
		
		if(this.grad==m.grad)
		{
			rezultat.coeficient=this.coeficient-m.coeficient;
			rezultat.grad=this.grad;
		}
		
		return rezultat;
	}
	
	public Monom inmultire(Monom m)
	{
		Monom rezultat= new Monom();
		
		rezultat.coeficient=this.coeficient*m.coeficient;
		rezultat.grad=this.grad+m.grad;
		
		return rezultat;
	}
	
	public Monom impartire(Monom m)
	{
		Monom rezultat= new Monom();
		
		rezultat.coeficient=this.coeficient/m.coeficient;
		rezultat.grad=this.grad-m.grad;
		
		return rezultat;
		
	}
	
	public Monom derivare()
	{
		Monom rezultat= new Monom();
		
		if(this.grad==0)
		{
			rezultat.coeficient=0;
			rezultat.grad=0;
		}
		else
		{
			rezultat.coeficient=this.coeficient*this.grad;
			rezultat.grad=this.grad-1;
		}
		
		return rezultat;
	}
	
	public Monom integrare()
	{
		Monom rezultat= new Monom();
		
		if(this.grad==0)
		{
			rezultat.coeficient=this.coeficient;
			rezultat.grad=1;
		}
		else
		{
			rezultat.coeficient=this.coeficient/(this.grad+1);
			rezultat.grad=this.grad+1;
		}
		
		return rezultat;
	}	
	
	public double getCoeficient() 
	{
		return coeficient;
	}

	public int getGrad() 
	{
		return grad;
	}

	public String toString()
	{
		if(this.grad==0)
			return this.coeficient+"";
		else
			return this.coeficient+"x^"+this.grad+" ";
	}
	
	public int compareTo(Object monom) 
	{
		Monom m=(Monom)monom;
		
		if(this==m)
			return 0;
		if(this.grad<m.grad)
			return 1;
		else if(this.grad==m.grad)
			return 0;
		else 
			return -1;
	}
}