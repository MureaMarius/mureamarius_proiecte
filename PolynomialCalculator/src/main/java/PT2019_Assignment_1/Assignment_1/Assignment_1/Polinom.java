package PT2019_Assignment_1.Assignment_1.Assignment_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.Collections;

public class Polinom
{
	private String polinom;
	private ArrayList<Monom> monoame;
	private Monom[] monoms;
	private int[] grade;
	private int m=0;
	
	public Polinom(String polinom)
	{
		this.polinom=polinom;
		this.monoame= new ArrayList<Monom>();
		this.grade= new int[20];
		this.monoms= new Monom[20];
	}
	
	public Polinom()
	{
		this.monoame= new ArrayList<Monom>();
	}
	
	public int validarePolinom()
	{
		String polinom=this.polinom;
		
		int i;
		for(i=0;i<polinom.length();i++)
			if(polinom.charAt(i)<'0' || polinom.charAt(i)>'9')
				if(polinom.charAt(i)!='x')
					if(polinom.charAt(i)!='^')
						if(polinom.charAt(i)!=' ')
							if(polinom.charAt(i)!='+' )
								if(polinom.charAt(i)!='-')
										return 1;
		return 0;
	}
	
	public void extrageMonoame()
	{
		String s=this.polinom;
		int j=0;

		StringTokenizer tok= new StringTokenizer(s,"x^ ");
		
		while (tok.hasMoreElements())
		{
			
			double coef=Double.parseDouble(tok.nextElement().toString());
			int grad=Integer.parseInt(tok.nextElement().toString());
			
			Monom m= new Monom();
			
			m.coeficient=coef;
			m.grad=grad;
			
			monoame.add(m);	
			monoms[j]=m;
			j++;
			this.m++;
		}
	}
	
	public Monom[] grade(Polinom b)
	{
		int i,j,k=0;
		int OK=0;
		
		Monom[] rezultat= new Monom[15];
		
		for(i=0;i<this.m;i++)
		{
			OK=0;
			for(j=0;j<b.m;j++)
				if(this.monoms[i].grad==b.monoms[j].grad)
					OK=1;
			if(OK==0)
			{
				rezultat[k]=this.monoms[i];
				k++;	
			}
		}	
		
		OK=0;
		
		for(i=0;i<b.m;i++)
		{
			OK=0;
			for(j=0;j<this.m;j++)
				if(b.monoms[i].grad==this.monoms[j].grad)
					OK=1;
			if(OK==0)
			{
				rezultat[k]=b.monoms[i];
				k++;
			}
		}
		
		return rezultat;
	}
	
	public String adunare(Polinom b,int ok)
	{
		int OK=0,i;
		
		this.extrageMonoame();
		if(ok==1)
			b.extrageMonoame();
		
		Polinom rezultat= new Polinom();
		
		for(Monom a: this.monoame)
		{
			Monom rezultat1= new Monom();
			OK=0;
			
			for(Monom a1: b.monoame)
			{
				if(a.grad==a1.grad)
				{
					rezultat1=a.adunare(a1);
					OK=1;
				}
			}
			if((rezultat1.coeficient!=0 && rezultat1.grad!=0) || (rezultat1.coeficient!=0 && rezultat1.grad==0))
				rezultat.monoame.add(rezultat1);
		}
		
		Monom[] rez= new Monom[15];
		rez=grade(b);
		
		for(i=0;i<rez.length;i++)
		{
			if(rez[i] instanceof Monom)
			{
				rezultat.monoame.add(rez[i]);
			}
		}
		
		Collections.sort(rezultat.monoame);
		
		String r= new String();
		r=rezultat.afisare();
		
		return r;
	}
	
	public void negareCoeficienti(Polinom b)
	{
		b.extrageMonoame();
		
		for(Monom a: b.monoame)
		{
			a.coeficient=-a.coeficient;
		}
	}
	
	public String scadere(Polinom b)
	{
		b.negareCoeficienti(b);
		
		String r= new String();
		r=this.adunare(b,0);
		
		return r;
	}
	
	public String inmultire(Polinom b)
	{
		this.extrageMonoame();
		b.extrageMonoame();
		
		Polinom rezultat= new Polinom();
		
		for(Monom a: this.monoame)
		{
			for(Monom a1: b.monoame)
			{
				rezultat.monoame.add(a.inmultire(a1));
			}
		}
		
		Collections.sort(rezultat.monoame);
		
		String r= new String();
		r=rezultat.afisare();
		
		return r;
	}
	
	public String impartire(Polinom b)
	{
		this.extrageMonoame();
		b.extrageMonoame();
		
		Collections.sort(this.monoame);
		Collections.sort(b.monoame);
		
		Polinom rezultat= new Polinom();
		Polinom rest= new Polinom();
		
		ArrayList<Monom> monoameRest;
		monoameRest=(ArrayList<Monom>)this.monoame.clone();
		
		rest.monoame=monoameRest;
		
		int OK=1;
		Monom x2= b.monoame.get(0);
		Monom x1=rest.monoame.get(0);
		
		Polinom rezultat1= this;
		
		while(OK==1)
		{
			Monom rez=x1.impartire(x2);
			rezultat.monoame.add(rez);
			
			int lungimeImpartitor=b.m;
			rest.monoame.removeAll(rest.monoame);
			
			for(Monom a: b.monoame)
			{
				rest.monoame.add(rez.inmultire(a));
				lungimeImpartitor--;
				
				if(lungimeImpartitor==0)
					break;
			}
			
			for(Monom a: rest.monoame)
			{
				a.coeficient=-a.coeficient;
			}
			
			OK=0;
			
		}
		
		Collections.sort(rezultat.monoame);
		
		String r= new String();
		r=rezultat.afisare();
		
		return r;
		
	}
	
	public String derivare()
	{
		Polinom rezultat= new Polinom();
		this.extrageMonoame();
		
		for(Monom a: this.monoame)
		{
			Monom rez= new Monom();
			rez=a.derivare();
			rezultat.monoame.add(rez);
		}
		
		Collections.sort(rezultat.monoame);
		
		String r= new String();
		r=rezultat.afisare();
		
		return r;
	}
	
	public String integrare()
	{
		Polinom rezultat= new Polinom();
		this.extrageMonoame();
		
		for(Monom a: this.monoame)
		{
			Monom rez= new Monom();
			rez=a.integrare();
			rezultat.monoame.add(rez);
		}
		
		Collections.sort(rezultat.monoame);
		
		String r= new String();
		r=rezultat.afisare();
		
		return r;
	}
	
	public String afisare()
	{
		String rezultat= new String();
		
		for(Monom m:monoame)
		{
			if(m.coeficient>0)
				rezultat+="+"+m.toString();
			else
				rezultat+=m.toString();
		}
		
		return rezultat;
	}
}