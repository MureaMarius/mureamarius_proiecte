package LogicPackage;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import GuiPackage.GUI;

public class Market extends Thread 
{
	protected Coada cozi[];
	protected int numarCase;
	protected static int numarClient = 0;
	protected int numarClienti;	
	protected int timpServireMin;
	protected int timpServireMax;
	protected int sosireMinClient;
	protected int sosireMaxClient;
	protected static int timpCurent = 0;
	protected Client[] clienti;
	protected GUI gui;
	protected static int coadaAleasa;
	protected String[] detaliiCase;
	protected double avgSosireClienti;
	protected double avgServireClienti;
	protected JTextField[] textCasa;

	public Market(int numarCase, Coada cozi[], int numarClienti, int timpServireMin, int timpServireMax,
			int sosireMinClient, int sosireMaxClient, GUI gui) 
	{
		this.numarCase = numarCase;
		this.numarClienti = numarClienti;
		this.timpServireMin = timpServireMin;
		this.timpServireMax = timpServireMax;
		this.sosireMinClient = sosireMinClient;
		this.sosireMaxClient = sosireMaxClient;

		this.cozi = new Coada[numarCase];
		for (int i = 0; i < numarCase; i++) 
		{
			this.cozi[i] = cozi[i];
		}

		this.gui = gui;
		this.clienti = new Client[numarClienti];
		this.detaliiCase = new String[numarCase];
		this.textCasa= new JTextField[numarCase];
	}

	public Client[] generareClienti() 
	{
		int j = 0;
		while (j < numarClienti) 
		{
			Random random = new Random();

			int arrivalTime, serviceTime;

			arrivalTime = random.nextInt(this.sosireMaxClient) + 1;
			serviceTime = random.nextInt(this.timpServireMax) + 1;

			clienti[j] = new Client(j, arrivalTime, serviceTime);
			j++;
		}

		return clienti;
	}

	public int selectareCoadaClient()
	{
		int suma, min = 100;
		int index = 0;
		Coada casa;
		Client client;

		for (int i = 0; i < this.numarCase; i++) 
		{
			suma = 0;
			casa = cozi[i];

			for (int j = 0; j < cozi[i].lungimeCoada(); j++) 
			{
				if (casa.verificareCoada() == false) 
				{
					client = (Client) (casa.clienti).get(j);
					suma += client.getTimpServire();
				}
			}

			if (min > suma) 
			{
				min = suma;
				index = i;
			}

		}
		return index;
	}

	public void sortareClienti() 
	{
		int i, j;
		Client aux;

		for (i = 0; i < numarClienti; i++) 
		{
			for (j = i + 1; j < numarClienti; j++) 
			{
				if (clienti[i].timpSosire > clienti[j].timpSosire) 
				{
					aux = clienti[i];
					clienti[i] = clienti[j];
					clienti[j] = aux;
				}
			}
		}

		for (i = 0; i < numarClienti; i++) 
		{
			clienti[i].numarClient = i;
		}
	}

	public void run() 
	{
		generareClienti();
		sortareClienti();
		int i = 0;
		avgSosireClienti = 0;
		avgServireClienti = 0;
		formareDetaliiCase();

		while (true)
		{
			try 
			{
				while (i < numarClienti && clienti[i].timpSosire == timpCurent) 
				{
					coadaAleasa = selectareCoadaClient();
					cozi[coadaAleasa].adaugaClient(clienti[i]);
					avgSosireClienti += clienti[i].timpSosire;

					String s = "Clientul " + clienti[i].numarClient + " a fost adaugat la casa " + coadaAleasa
							+ ". Timp sosire: " + clienti[i].timpSosire + ". Timp servire: " + clienti[i].timpServire
							+ ".\n";
					gui.rezultat.append(s);

					if (cozi[coadaAleasa].clienti.size() == 1)
					{
						cozi[coadaAleasa].clienti.get(0).startServire = timpCurent;
					}

					i++;
					textCasa[coadaAleasa].setText(cozi[coadaAleasa].toString());
				}

				for (int j = 0; j < numarCase; j++)
				{
					if (cozi[j].clienti.size() != 0) 
					{
						if (cozi[j].clienti.get(0).startServire != -1 && cozi[j].clienti.get(0).startServire
								+ cozi[j].clienti.get(0).timpServire == timpCurent) 
						{
							String s = "Clientul " + cozi[j].clienti.get(0).numarClient + " a fost servit la timpul "
									+ timpCurent+" si la casa "+j+ "\n";
							gui.rezultat.append(s);
							avgServireClienti += cozi[j].clienti.get(0).timpServire;
							cozi[j].eliminareClient();

							if (cozi[j].clienti.size() != 0) 
							{
								cozi[j].clienti.get(0).startServire = timpCurent;
							}
							
							textCasa[coadaAleasa].setText(cozi[coadaAleasa].toString());
						}
					}
				}
				
				if(i==numarClienti)
				{
					int OK=0;
					int k;
					for(k=0;k<numarCase;k++)
					{
						if(cozi[k].clienti.size()!=0)
							OK=1;
					}
					
					if(OK==0)
					{
						timpCurent+=1;
						afisareDetaliiClienti();
						break;
					}
				}
				
				for(int p=0;p<numarCase;p++)
				{
					textCasa[p].setText(cozi[p].toString());
				}

				sleep(1000);
				timpCurent += 1;
				textCasa[coadaAleasa].setText(cozi[coadaAleasa].toString());

			} catch (InterruptedException e)
			{
				System.out.println(e.toString());
			}
		}
	}

	public void formareDetaliiCase() 
	{
		int i;
		int k=350;
		
		for(i=0;i<numarCase;i++)
		{
			JLabel casa= new JLabel("Casa "+i+":");
			textCasa[i]= new JTextField();
			casa.setBounds(10, k, 100, 100);
			textCasa[i].setBounds(70, k+35, 300, 30);
			k+=50;
			gui.cPane.add(casa);
			gui.cPane.add(textCasa[i]);
		}
		
		gui.appFrame.pack();
		gui.appFrame.setSize(1000, 700);
		gui.appFrame.setLocation(500,100);
		gui.appFrame.setVisible(true);
	}

	public void afisare() 
	{
		int i;
		for (i = 0; i < numarClienti; i++) 
		{
			String s = "Clientul " + i + " a fost generat. Timp sosire: " + clienti[i].timpSosire + " Timp servire: "
					+ clienti[i].timpServire + "\n";
			gui.rezultat.append(s);
		}
	}

	public void afisareDetaliiClienti() 
	{
		avgSosireClienti /= (double) numarClienti;
		avgServireClienti /= (double) numarClienti;

		String s = "Timpul mediu de sosire: " + avgSosireClienti + "\n\n\n";
		gui.detaliiClienti.append(s);

		String s2 = "Timpul mediu de servire: " + avgServireClienti + "\n\n\n";
		gui.detaliiClienti.append(s2);
	}
}
