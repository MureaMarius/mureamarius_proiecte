package LogicPackage;

import java.util.ArrayList;

public class Client 
{
	protected int numarClient;
	protected int timpSosire;
	protected int timpServire;
	protected int startServire = -1;
	
	public int getStartServire() {
		return startServire;
	}

	public void setStartServire(int startServire) {
		this.startServire = startServire;
	}

	public Client(int numarClient,int timpSosire,int timpServire)
	{
		this.numarClient=numarClient;
		this.timpSosire=timpSosire;
		this.timpServire=timpServire;
	}

	public int getNumarClient() 
	{
		return numarClient;
	}

	public void setNumarClient(int numarClient) 
	{
		this.numarClient = numarClient;
	}

	public int getTimpSosire()
	{
		return timpSosire;
	}

	public void setTimpSosire(int timpSosire) 
	{
		this.timpSosire = timpSosire;
	}

	public int getTimpServire() 
	{
		return timpServire;
	}

	public void setTimpServire(int timpServire) 
	{
		this.timpServire = timpServire;
	}
	
	public String toString()
	{
		return "Clientul "+this.numarClient+" a sosit la timpul "+this.timpSosire+" si a avut timpul de servire "+this.timpServire;
	}
}
