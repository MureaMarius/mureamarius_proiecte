package LogicPackage;

import java.util.ArrayList;
import java.util.Vector;

import GuiPackage.GUI;

public class Coada extends Thread
{
	protected  int timp=0;
	protected ArrayList<Client> clienti;
	protected GUI gui;
	
	public Coada(String name,GUI gui)
	{
		setName(name);
		clienti= new ArrayList<Client>();
		this.gui=gui;
	}
	
	public synchronized  void adaugaClient(Client c)
	{
		clienti.add(c);
		timp+=c.timpServire;
		notifyAll();
	}
	
	public synchronized  void eliminareClient() throws InterruptedException
	{
		while(clienti.size()==0)
		{
			wait();
		}
		clienti.remove(0);
		notifyAll();
	}
	
	public synchronized boolean verificareCoada()
	{
		if(clienti.isEmpty()==true)
			return true;
		return false;
	}
	
	public synchronized int timpProcesareTotal()
	{
		for(Client c: clienti)
		{
			timp+=c.timpServire;
		}
		notifyAll();
		return timp;	
	}
	
	public synchronized int lungimeCoada()
	{
		int size=clienti.size();
		notifyAll();
		return size;
	}
	
	public synchronized String toString()
	{
		String s=" ";
		
		for(Client c:clienti)
		{
			s+=c.numarClient+" ";
		}
		
		notifyAll();
		return s;
	}
}
