package GuiPackage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import LogicPackage.Coada;
import LogicPackage.Market;


public class GUI extends JFrame implements ActionListener
{
	public JFrame appFrame;
	public JPanel panel;
	public JLabel jlbNumarCase,jlbNumarClienti, jlbTimpServireMin,jlbTimpServireMax, jlbSosireMinClient, jlbSosireMaxClient;
	public JTextField numarCase,numarClienti, timpServireMin, timpServireMax, sosireMinClient,sosireMaxClient;
	public JButton start;
	public Container cPane;
	public static JTextArea rezultat;
	public static JTextArea detaliiCase;
	public static JTextArea detaliiClienti;
	public static JScrollPane scroll;
	public static JScrollPane scrollCase;
	public static JScrollPane scrollClienti;
	
	public GUI()
	{
		makeGUI();
	}
	
	public void makeGUI()
	{
		appFrame= new JFrame("Magazin");
		appFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(1000, 700);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);
	}

	public void arrangeComponents() 
	{
		jlbNumarCase= new JLabel("Numar de case: ");
		jlbNumarCase.setBounds(25, 25, 200, 100);
		
		jlbNumarClienti= new JLabel("Numar clienti: ");
		jlbNumarClienti.setBounds(25, 75, 200, 100);
		
		jlbTimpServireMin= new JLabel("Timp servire minim: ");
		jlbTimpServireMin.setBounds(25, 125, 200, 100);
		
		jlbTimpServireMax= new JLabel("Timp servire maxim: ");
		jlbTimpServireMax.setBounds(25, 175, 200, 100);
		
		jlbSosireMinClient= new JLabel("Sosire minima client: ");
		jlbSosireMinClient.setBounds(25, 225, 200, 100);
		
		jlbSosireMaxClient= new JLabel("Sosire maxima client: ");
		jlbSosireMaxClient.setBounds(25, 275, 200, 100);
		
		numarCase= new JTextField();
		numarCase.setBounds(120, 65, 25, 25);
		
		numarClienti= new JTextField();
		numarClienti.setBounds(110, 115, 25, 25);
		
		timpServireMin= new JTextField();
		timpServireMin.setBounds(140, 165, 25, 25);
		
		timpServireMax= new JTextField();
		timpServireMax.setBounds(145, 215, 25, 25);
		
		sosireMinClient= new JTextField();
		sosireMinClient.setBounds(150, 265, 25, 25);

		sosireMaxClient= new JTextField();
		sosireMaxClient.setBounds(150, 315, 25, 25);
		
		scroll= new JScrollPane(rezultat);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		scrollCase= new JScrollPane(detaliiCase);
		scrollCase.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		scrollClienti= new JScrollPane(detaliiClienti);
		scrollClienti.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		rezultat= new JTextArea();
		rezultat.setBounds(550, 10, 400, 400);
		rezultat.setLineWrap(true);
		rezultat.setWrapStyleWord(true);
		rezultat.setEditable(false);
		
		detaliiCase= new JTextArea();
		detaliiCase.setBounds(10, 420, 500, 200);
		detaliiCase.setLineWrap(true);
		detaliiCase.setWrapStyleWord(true);
		detaliiCase.setEditable(false);
		
		detaliiClienti= new JTextArea();
		detaliiClienti.setBounds(550, 420, 400, 200);
		detaliiClienti.setLineWrap(true);
		detaliiClienti.setWrapStyleWord(true);
		detaliiClienti.setEditable(false);
		
		start= new JButton("Start");
		start.setFont(new Font("Arial", Font.PLAIN, 20));
		start.setBounds(300, 200, 100, 100);
		
		scroll.setBounds(550, 10, 400, 400);
		scroll.getViewport().setBackground(Color.WHITE);
		scroll.getViewport().setOpaque(false);
		scroll.getViewport().add(rezultat);
		
		scrollCase.setBounds(10, 420, 500, 200);
		scrollCase.getViewport().setBackground(Color.WHITE);
		scrollCase.getViewport().setOpaque(false);
		scrollCase.getViewport().add(detaliiCase);
		
		scrollClienti.setBounds(550, 420, 400, 200);
		scrollClienti.getViewport().setBackground(Color.WHITE);
		scrollClienti.getViewport().setOpaque(false);
		scrollClienti.getViewport().add(detaliiClienti);
		
		cPane.add(jlbNumarCase);
		cPane.add(jlbNumarClienti);
		cPane.add(numarCase);
		cPane.add(numarClienti);
		cPane.add(jlbTimpServireMin);
		cPane.add(jlbTimpServireMax);
		cPane.add(jlbSosireMinClient);
		cPane.add(jlbSosireMaxClient);	
		cPane.add(timpServireMin);
		cPane.add(timpServireMax);
		cPane.add(sosireMinClient);
		cPane.add(sosireMaxClient);
		cPane.add(start);
		cPane.add(scroll);
		//cPane.add(scrollCase);
		cPane.add(scrollClienti);
		
		start.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==start)
		{
			startGUI();
		}
	}
	
	public void startGUI() 
	{
		int nrCase=Integer.parseInt(numarCase.getText());
		int nrClienti=Integer.parseInt(numarClienti.getText());
		int tpServireMin=Integer.parseInt(timpServireMin.getText());
		int tpServireMax=Integer.parseInt(timpServireMax.getText());
		int ssMinClient=Integer.parseInt(sosireMinClient.getText());
	    int ssMaxClient=Integer.parseInt(sosireMaxClient.getText());	
			    
	    Coada c[]=new Coada[nrCase];
	    
	    int i;
		for(i=0;i<nrCase;i++)
		{
			c[i]= new Coada(""+Integer.toString(i),this);
			//c[i].start();
		}
		
		Market p= new Market(nrCase,c,nrClienti,tpServireMin,tpServireMax,ssMinClient,ssMaxClient,this);
		p.start();
	}
}
