package dataLayer;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import businessLayer.MenuItem;
import businessLayer.Order;

public class RestaurantSerializator implements Serializable
{
	public void serialization(ArrayList<MenuItem> itm)
	{
		try
		{
			FileOutputStream fos= new FileOutputStream("itemsData.txt");
			ObjectOutputStream oos= new ObjectOutputStream(fos);
			
			oos.writeObject(itm);
			oos.flush();
			oos.close();
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public ArrayList<MenuItem> deserialization()
	{
		ArrayList<MenuItem> items= new ArrayList<MenuItem>();
		
		try
		{
			FileInputStream fis= new FileInputStream("itemsData.txt");
			ObjectInputStream ois= new ObjectInputStream(fis);
			
			items= (ArrayList<MenuItem>)ois.readObject();
			ois.close();
			fis.close();
			
			return items;
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
		catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }
		return items;
	}

	public void serializationOrder(ArrayList<Order> orders)
	{
		try
		{
			FileOutputStream fos= new FileOutputStream("ordersData.txt");
			ObjectOutputStream oos= new ObjectOutputStream(fos);

			oos.writeObject(orders);
			oos.flush();
			oos.close();
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}

	public ArrayList<Order> deserializationOrder()
	{
		ArrayList<Order> orders= new ArrayList<Order>();

		try
		{
			FileInputStream fis= new FileInputStream("ordersData.txt");
			ObjectInputStream ois= new ObjectInputStream(fis);

			orders= (ArrayList<Order>)ois.readObject();
			ois.close();
			fis.close();

			return orders;
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
			return null;
		}
		catch (ClassNotFoundException c)
		{
			System.out.println("Class not found");
			c.printStackTrace();
			return null;
		}
		return orders;
	}
}
