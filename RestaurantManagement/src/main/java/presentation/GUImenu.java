package presentation;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import businessLayer.Restaurant;

public class GUImenu extends JFrame implements ActionListener
{
	Restaurant restaurant= new Restaurant();
	public JFrame appFrame;
	public JPanel panel;
	public JLabel labelAdmin, labelWaiter;
	public JLabel labelWelcome;
	public JButton btnAdmin, btnWaiter;
	public JLabel labelChef;
	public JButton btnChef;
	public Container cPane;
	
	public GUImenu()
	{
		makeGUImenu();
	}

	private void makeGUImenu() 
	{
		appFrame= new JFrame("Meniu Principal");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(500,410);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
	}

	private void arrangeComponents() 
	{
		btnAdmin= new JButton("Administrator");
		btnAdmin.setBounds(250, 100, 150, 50);
		
		btnWaiter= new JButton("Waiter");
		btnWaiter.setBounds(250, 200, 150, 50);
		
		labelAdmin= new JLabel("Apasă pentru a intra în Administrator!");
		labelAdmin.setBounds(25, 100, 300, 50);
		
		labelWaiter= new JLabel("Apasă pentru a intra în Waiter!");
		labelWaiter.setBounds(50, 200, 300, 50);

		labelChef= new JLabel("Apasa pentru a intra in Chef!");
		labelChef.setBounds(50,300,300,50);
		cPane.add(labelChef);

		btnChef= new JButton("Chef");
		btnChef.setBounds(250,300,150,50);
		cPane.add(btnChef);
		
		labelWelcome= new JLabel("Welcome!");
		labelWelcome.setBounds(200, 10, 300, 50);
		labelWelcome.setFont(new Font("Arial", Font.PLAIN, 16));
		
		cPane.add(btnAdmin);
		cPane.add(btnWaiter);
		cPane.add(labelAdmin);
		cPane.add(labelWaiter);
		cPane.add(labelWelcome);
		
		btnAdmin.addActionListener(this);
		btnWaiter.addActionListener(this);
		btnChef.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==btnAdmin)
		{
			new GUIadmin();
		}
		if(e.getSource()==btnWaiter)
		{
			new GUIwaiter();
		}
		if(e.getSource()==btnChef)
		{
			GUIchef chef=new GUIchef();
			restaurant.addObserver(chef);
		}
	}
}
