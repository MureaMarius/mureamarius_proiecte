package presentation;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;

public class Table 
{
	JTable menuItems;
	JTable orders;

	JFrame frameItems;
	JFrame frameOrders;
	public Table()
	{
		
	}
	
	public String[][] retrieveProperties(ArrayList<MenuItem> menuItems,ArrayList<Order> orders, int OK)
	{
	
		int i=0;
		
		String[][] data= new String[100][100];

		if(OK==0)
		{
			for(MenuItem item: menuItems)
			{
				if(item instanceof BaseProduct)
				{
					try
					{
						BaseProduct product=(BaseProduct)item;
						data[i][0]=item.getClass().getSimpleName();
						data[i][1]=product.getNumeProduct();
						data[i][2]=String.valueOf(product.getCantitateProduct());
						data[i][3]=String.valueOf(product.getPretProduct());

						i++;
					}
					catch(IllegalArgumentException e)
					{
						e.printStackTrace();
					}
				}
				else if(item instanceof CompositeProduct)
				{
					try
					{
						CompositeProduct product=(CompositeProduct)item;
						data[i][0]=item.getClass().getSimpleName();
						data[i][1]=product.getNumeComposite();
						data[i][2]=String.valueOf(product.getCantitateComposite());
						data[i][3]=String.valueOf(product.getPretComposite());

						i++;
					}
					catch(IllegalArgumentException e)
					{
						e.printStackTrace();
					}
				}
			}
			return data;
		}

		if(OK==1)
		{
			for(Order order: orders)
			{
				try
				{
					Order ord= (Order)order;
					data[i][0] = ord.getComandaId();
					data[i][1] = ord.getNumeItem();
					data[i][2] = ord.getOra();
					data[i][3] = ord.getMinut();
					data[i][4] = ord.getMasa();

					i++;
				}
				catch (IllegalArgumentException e)
				{
					System.out.println(e.getMessage());
				}
			}
			return data;
		}

		return data;
	}

	public void tableItems(String[][] dataItems)
	{
		frameItems= new JFrame();
		frameItems.setTitle("Tabel items");

		String[] columnNames= {"Tip item","Nume item","Cantitate item","Pret item"};

		menuItems= new JTable(dataItems,columnNames);
		menuItems.setBounds(30, 40, 300, 300);
		
		JScrollPane scrollPane= new JScrollPane(menuItems);
		frameItems.add(scrollPane);
		frameItems.setSize(500, 200);
		frameItems.setVisible(true);
	}

	public void tableOrders(String[][] dataOrders)
	{
		frameOrders = new JFrame();
		frameOrders.setTitle("Tabel orders");

		String[] columnNames = {"Comanda ID", "Nume item", "Ora", "Minut", "Masa"};

		orders = new JTable(dataOrders, columnNames);
		orders.setBounds(30, 40, 300, 300);

		JScrollPane scrollPane = new JScrollPane(orders);
		frameOrders.add(scrollPane);
		frameOrders.setSize(500, 200);
		frameOrders.setVisible(true);
	}
}
