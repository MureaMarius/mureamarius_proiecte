package presentation;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class GUIchef extends JFrame implements ActionListener, Observer
{
    Restaurant restaurant= new Restaurant();
    RestaurantSerializator serializator= new RestaurantSerializator();
    ArrayList<MenuItem> menu= new ArrayList<MenuItem>();

    public JFrame appFrame;
    public JPanel panel;
    public JLabel labelNotify, labelItems;
    public JButton  btnUpdate, btnItems;
    public Container cPane;

    public GUIchef()
    {
        makeGUIchef();
    }

    private void makeGUIchef()
    {
        appFrame= new JFrame("Chef");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(500,300);
        appFrame.setLocation(500,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        labelNotify= new JLabel("Apasa pt a notifica chef-ul!");
        labelNotify.setBounds(50,50,300,50);
        cPane.add(labelNotify);

        btnUpdate= new JButton("Update");
        btnUpdate.setBounds(250,50,150,50);
        cPane.add(btnUpdate);

        labelItems= new JLabel("Apasa pt a vedea meniu items!");
        labelItems.setBounds(50,150,300,50);
        cPane.add(labelItems);

        btnItems= new JButton("View all items");
        btnItems.setBounds(250,150,150,50);
        cPane.add(btnItems);

        btnUpdate.addActionListener(this);
        btnItems.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==btnUpdate)
        {
            update(null,menu);
        }
        if(e.getSource()==btnItems)
        {
            afisareItems();
        }
    }

    private void afisareItems()
    {
        Table tableItems= new Table();
        ArrayList<MenuItem> itms;

        tableItems.tableItems(tableItems.retrieveProperties(this.menu,null,0));
    }

    @Override
    public void update(Observable o, Object arg)
    {
        menu= serializator.deserialization();
        System.out.println("Notify");
        ArrayList<MenuItem> men= menu;
    }
}
