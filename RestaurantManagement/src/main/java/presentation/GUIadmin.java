package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicEditorPaneUI;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class GUIadmin extends JFrame implements ActionListener, Serializable
{
	Restaurant restaurant= new Restaurant();
	RestaurantSerializator serializator= new RestaurantSerializator();
	
	public JFrame appFrame;
	public JPanel panel;
	public JLabel labelAddNewMenuItem, labelEditMenuItems, labelDeleteMenuItems, labelViewAllMenuItems, labelBaseProduct,labelCompositeProduct;
	public JLabel labelNumeProduct, labelCantitateProduct, labelPretProduct;
	public JLabel labelBase, labelComposite;
	public JLabel labelEditNumeBase, labelEditCantitateBase, labelEditPretBase;
	public JLabel labelEditBaseProduct, labelEditCompositeProduct;
	public JLabel labelEditNumeComposite, labelEditCantitateComposite, labelEditPretComposite;
	public JLabel labelNumeComposite, labelCantitateComposite, labelPretComposite;
	public JLabel labelDeleteNumeBaseProduct, labelDeleteNumeCompositeProduct;
	public JTextField txtNumeItem, txtPretItem, txtNume, txtCantitate, txtPret;
	public JTextField txtDeleteBaseProduct, txtDeleteCompositeProduct;
	public JTextField txtNumeBase, txtCantitateBase, txtPretBase;
	public JTextField txtEditNumeComposite, txtEditCantitateComposite, txtEditPretComposite;
	public JTextField txtNumeComposite, txtCantitateComposite, txtPretComposite;
	public JButton btnAddMeniuItem, btnEditMenuItems, btnDeleteMenuItem, btnViewAllItems;
	public JLabel baseProduct1, baseProduct2, compositeProduct;
	public JLabel numeBaseProduct1,numeBaseProduct2;
	public JTextField numeBP1,numeBP2;
	public Container cPane;
	
	public GUIadmin()
	{
		makeGUIadmin();
	}
	
	private void makeGUIadmin() 
	{
		appFrame= new JFrame("Administrator");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(800,650);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);			
	}

	private void arrangeComponents() 
	{
		labelAddNewMenuItem= new JLabel("Adauga un nou Menu Item:");
		labelAddNewMenuItem.setBounds(10, 10, 300, 50);
		
		labelBaseProduct= new JLabel("Daca se doreste un Base Product:");
		labelBaseProduct.setBounds(50, 50, 300, 50);
		
		labelNumeProduct= new JLabel("Nume");
		labelNumeProduct.setBounds(270, 25, 300, 50);
	
		labelCantitateProduct= new JLabel("Cantitate");
		labelCantitateProduct.setBounds(370, 25, 300, 50);
		
		labelPretProduct= new JLabel("Pret");
		labelPretProduct.setBounds(470, 25, 300, 50);
		
		labelCompositeProduct= new JLabel("Composite Product: ");
		labelCompositeProduct.setBounds(10, 90, 300, 50);

		baseProduct1= new JLabel("BaseProduct 1");
		baseProduct1.setBounds(20,115,300,50);
		cPane.add(baseProduct1);

		numeBaseProduct1= new JLabel("Nume:");
		numeBaseProduct1.setBounds(10,140,300,50);
		cPane.add(numeBaseProduct1);

		numeBP1= new JTextField();
		numeBP1.setBounds(50,155,100,20);
		cPane.add(numeBP1);

		baseProduct2= new JLabel("BaseProduct 2");
		baseProduct2.setBounds(185,115,300,50);
		cPane.add(baseProduct2);

		numeBaseProduct2= new JLabel("Nume:");
		numeBaseProduct2.setBounds(170,140,300,50);
		cPane.add(numeBaseProduct2);

		numeBP2= new JTextField();
		numeBP2.setBounds(210,155,100,20);
		cPane.add(numeBP2);

		compositeProduct= new JLabel("Composite");
		compositeProduct.setBounds(350,115,300,50);
		cPane.add(compositeProduct);

		labelNumeComposite= new JLabel("Nume:");
		labelNumeComposite.setBounds(330, 140, 300, 50);
		
		labelCantitateComposite= new JLabel("Cantitate:");
		labelCantitateComposite.setBounds(490, 140, 300, 50);
		
		labelPretComposite= new JLabel("Pret:");
		labelPretComposite.setBounds(620, 140, 300, 50);
		

		txtNume= new JTextField();
		txtNume.setBounds(245, 60, 100, 30);
		
		txtCantitate= new JTextField();
		txtCantitate.setBounds(370, 60, 50, 30);
		
		txtPret= new JTextField();
		txtPret.setBounds(455, 60, 50, 30);
		
		txtNumeComposite= new JTextField();
		txtNumeComposite.setBounds(370, 155, 100, 20);
		
		txtCantitateComposite= new JTextField();
		txtCantitateComposite.setBounds(545, 155, 50, 20);
		
		txtPretComposite= new JTextField();
		txtPretComposite.setBounds(650, 155, 50, 20);

		btnAddMeniuItem= new JButton("Adauga Menu Item!");
		btnAddMeniuItem.setBounds(520, 90, 170, 40);
		
		labelDeleteMenuItems= new JLabel("Sterge un Menu Item:");
		labelDeleteMenuItems.setBounds(10, 180, 300, 50);
		
		labelBase= new JLabel("Daca se doreste a se sterge un Base Product:");
		labelBase.setBounds(60, 220, 300, 50);
		
		labelComposite= new JLabel("Daca se doreste a se sterge un Composite Product: ");
		labelComposite.setBounds(60, 270, 300, 50);
		
		labelDeleteNumeBaseProduct= new JLabel("Nume");
		labelDeleteNumeBaseProduct.setBounds(340, 200, 300, 50);
		
		labelDeleteNumeCompositeProduct= new JLabel("Nume");
		labelDeleteNumeCompositeProduct.setBounds(370, 252, 300, 50);
		
		txtDeleteBaseProduct= new JTextField();
		txtDeleteBaseProduct.setBounds(320, 235, 100, 20);
		
		txtDeleteCompositeProduct= new JTextField();
		txtDeleteCompositeProduct.setBounds(350, 285, 100, 20);
		
		btnDeleteMenuItem= new JButton("Sterge Menu Item!");
		btnDeleteMenuItem.setBounds(470, 250, 150, 30);
		
		labelEditMenuItems= new JLabel("Modifica un Menu Item:");
		labelEditMenuItems.setBounds(10, 350, 300, 50);
		
		labelEditBaseProduct= new JLabel("Meniu Item: ");
		labelEditBaseProduct.setBounds(50, 390, 300, 50);
		
		labelEditNumeBase= new JLabel("Nume");
		labelEditNumeBase.setBounds(160, 370, 300, 50);
		
		txtNumeBase= new JTextField();
		txtNumeBase.setBounds(140, 405, 100, 20);
		
		labelEditCantitateBase= new JLabel("Cantitate");
		labelEditCantitateBase.setBounds(280, 370, 300, 50);
		
		txtCantitateBase= new JTextField();
		txtCantitateBase.setBounds(280, 405, 50, 20);
		
		labelEditPretBase= new JLabel("Pret");
		labelEditPretBase.setBounds(380, 370, 300, 50);
		
		txtPretBase= new JTextField();
		txtPretBase.setBounds(370, 405, 50, 20);
		/*
		labelEditCompositeProduct= new JLabel("Composite Product: ");
		labelEditCompositeProduct.setBounds(50, 450, 300, 50);

		labelEditNumeComposite= new JLabel("Nume");
		labelEditNumeComposite.setBounds(180, 430, 300, 50);

		labelEditCantitateComposite= new JLabel("Cantitate");
		labelEditCantitateComposite.setBounds(300, 430, 300, 50);

		txtEditNumeComposite= new JTextField();
		txtEditNumeComposite.setBounds(165, 465, 100, 20);

		txtEditCantitateComposite= new JTextField();
		txtEditCantitateComposite.setBounds(300, 465, 50, 20);

		labelEditPretComposite= new JLabel("Pret");
		labelEditPretComposite.setBounds(400, 430, 300, 50);

		txtEditPretComposite= new JTextField();
		txtEditPretComposite.setBounds(390, 465, 50, 20);
		*/
		btnEditMenuItems= new JButton("Modifica Menu Item!");
		btnEditMenuItems.setBounds(460, 400, 200, 30);
		
		labelViewAllMenuItems= new JLabel("Vizualizeaza Meniul: ");
		labelViewAllMenuItems.setBounds(10, 530, 300, 50);
		
		btnViewAllItems= new JButton("View all Menu Items");
		btnViewAllItems.setBounds(140, 540, 250, 30);
		
		cPane.add(btnViewAllItems);
		cPane.add(labelViewAllMenuItems);
		cPane.add(btnEditMenuItems);
		//cPane.add(txtEditPretComposite);
		//cPane.add(labelEditPretComposite);
		//cPane.add(txtEditCantitateComposite);
		//cPane.add(labelEditCantitateComposite);
		//cPane.add(txtEditNumeComposite);
		//cPane.add(labelEditNumeComposite);
		//cPane.add(labelEditCompositeProduct);
		cPane.add(txtPretBase);
		cPane.add(labelEditPretBase);
		cPane.add(txtCantitateBase);
		cPane.add(labelEditCantitateBase);
		cPane.add(txtNumeBase);
		cPane.add(labelEditNumeBase);
		cPane.add(labelEditBaseProduct);
		cPane.add(labelEditMenuItems);
		cPane.add(btnDeleteMenuItem);
		cPane.add(txtDeleteCompositeProduct);
		cPane.add(labelDeleteNumeCompositeProduct);
		cPane.add(labelComposite);
		cPane.add(txtDeleteBaseProduct);
		cPane.add(labelAddNewMenuItem);
		cPane.add(labelBaseProduct);
		cPane.add(labelNumeProduct);
		cPane.add(txtNume);
		cPane.add(labelCantitateProduct);
		cPane.add(txtCantitate);
		cPane.add(labelPretProduct);
		cPane.add(txtPret);
		cPane.add(labelCompositeProduct);
		cPane.add(labelNumeComposite);
		cPane.add(txtNumeComposite);
		cPane.add(labelCantitateComposite);
		cPane.add(labelPretComposite);
		cPane.add(txtCantitateComposite);
		cPane.add(txtPretComposite);
		cPane.add(btnAddMeniuItem);
		cPane.add(labelDeleteMenuItems);
		cPane.add(labelBase);
		cPane.add(labelDeleteNumeBaseProduct);
		
		btnViewAllItems.addActionListener(this);
		btnEditMenuItems.addActionListener(this);
		btnDeleteMenuItem.addActionListener(this);
		btnAddMeniuItem.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==btnViewAllItems)
		{
			afiseazaTotMeniul();
		}
		if(e.getSource()==btnEditMenuItems)
		{
			modificaMeniul();
		}
		if(e.getSource()==btnDeleteMenuItem)
		{
			stergeDinMeniuBaseProduct();
			stergeDinMeniuCompositeProduct();
		}
		if(e.getSource()==btnAddMeniuItem)
		{
			try
			{
				adaugaInMeniuBaseProduct();
				adaugaInMeniuCompositeProduct();
				adaugaInMeniuDupaBaseProducturi();
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}	
		}
	}

	private void adaugaInMeniuDupaBaseProducturi()
	{
		String numeProdus1=numeBP1.getText();
		String numeProdus2=numeBP2.getText();
		int OK1=0,OK2=0;
		int cantitate1=0, cantitate2=0;
		double pret1=0, pret2=0;

		if(numeBP1.getText().isEmpty() && numeBP2.getText().isEmpty())
			return;
		else
		{
			ArrayList<MenuItem> meniu=serializator.deserialization();
			for(MenuItem item: meniu)
			{
				if(item instanceof  BaseProduct)
				{
					BaseProduct prod=(BaseProduct)item;
					if(prod.getNumeProduct().equalsIgnoreCase(numeProdus1))
					{
						OK1=1;
						cantitate1=prod.getCantitateProduct();
						pret1=prod.getPretProduct();
					}

					if(prod.getNumeProduct().equalsIgnoreCase(numeProdus2))
					{
						OK2=1;
						cantitate2=prod.getCantitateProduct();
						pret2=prod.getPretProduct();
					}
				}
			}

			if(OK1==1 && OK2==1)
			{
				int cantitateFinala=cantitate1+cantitate2;
				double pretFinal= pret1+pret2;
				CompositeProduct compositeFinal= new CompositeProduct(numeProdus1+"+"+numeProdus2,cantitateFinala,pretFinal);

				restaurant.createNewMenuItem(compositeFinal);
				serializator.serialization(restaurant.getItems());

				numeBP1.setText(null);
				numeBP2.setText(null);
			}
		}
	}

	private void stergeDinMeniuCompositeProduct()
	{
		String numeComposite= txtDeleteCompositeProduct.getText();

		if(txtDeleteCompositeProduct.getText().isEmpty())
			return;
		else
		{
			ArrayList<MenuItem> itms= new ArrayList<MenuItem>();
			itms=serializator.deserialization();
			restaurant.setItems(itms);

			restaurant.deleteMenuItem(numeComposite,1);
			txtDeleteCompositeProduct.setText(null);
		}
	}

	private void adaugaInMeniuCompositeProduct() throws Exception
	{
		if((txtNumeComposite.getText().isEmpty() && txtCantitateComposite.getText().isEmpty() && txtPretComposite.getText().isEmpty()))
		{
			return;
		}
		else
		{
			String numeCompositeProduct=txtNumeComposite.getText();
			int cantitateComposite;
			double pretComposite;
			
			if(txtCantitateComposite.getText()=="" || txtPretComposite.getText()=="")
				return;
			else
			{
				cantitateComposite=Integer.parseInt(txtCantitateComposite.getText());
				pretComposite= Double.parseDouble(txtPretComposite.getText());
			}
			
		    if(cantitateComposite>0 && pretComposite>0)
		    {
		    	CompositeProduct compositeProduct= new CompositeProduct(numeCompositeProduct,cantitateComposite,pretComposite);

		    	restaurant.createNewMenuItem(compositeProduct);
				serializator.serialization(restaurant.getItems());

		    	compositeProduct.afisareBaseProducts();
		    }
		    	
		    txtNumeComposite.setText(null);
			txtCantitateComposite.setText(null);
			txtPretComposite.setText(null);	
		}		
	}

	private void adaugaInMeniuBaseProduct() throws Exception
	{
		if((txtNume.getText().isEmpty() && txtCantitate.getText().isEmpty() && txtPret.getText().isEmpty()))
		{
			return;
		}
		else 
		{
			String numeBaseProduct=txtNume.getText();
			
			int cantitateBaseProduct;
			double pretBaseProduct;
			
			if(txtCantitate.getText()=="" || txtPret.getText()=="")
				return;
			else
			{
				cantitateBaseProduct=Integer.parseInt(txtCantitate.getText());
				pretBaseProduct= Double.parseDouble(txtPret.getText());
			}
			
			if(cantitateBaseProduct>0 && pretBaseProduct>0)	
			{
				BaseProduct baseProduct= new BaseProduct(numeBaseProduct,cantitateBaseProduct,pretBaseProduct);
				restaurant.createNewMenuItem(baseProduct);
				//itms.add(baseProduct);
				//System.out.println(baseProduct.getNumeProduct()+" "+baseProduct.getCantitateProduct()+" "+baseProduct.getPretProduct());
				serializator.serialization(restaurant.getItems());
			}
			else
				return;
			
			txtNume.setText(null);
			txtCantitate.setText(null);
			txtPret.setText(null);
		}
	}

	private void stergeDinMeniuBaseProduct()
	{
		String numeBase= txtDeleteBaseProduct.getText();

		if(txtDeleteBaseProduct.getText().isEmpty())
			return;
		else
		{
			ArrayList<MenuItem> itms= new ArrayList<MenuItem>();
			itms=serializator.deserialization();
			restaurant.setItems(itms);

			restaurant.deleteMenuItem(numeBase,0);
			txtDeleteBaseProduct.setText(null);
		}
	}

	private void modificaMeniul() 
	{
		String numeItem= txtNumeBase.getText();
		int cantitateMoficataItem= Integer.parseInt(txtCantitateBase.getText());
		double pretItemModificat= Double.parseDouble(txtPretBase.getText());

		restaurant.editMenuItem(numeItem,cantitateMoficataItem,pretItemModificat);

		txtNumeBase.setText(null);
		txtCantitateBase.setText(null);
		txtPretBase.setText(null);
	}

	private void afiseazaTotMeniul() 
	{
		Table tableItems= new Table();

		ArrayList<MenuItem> itms;

		itms=serializator.deserialization();
		restaurant.setItems(itms);


		tableItems.tableItems(tableItems.retrieveProperties(restaurant.getItems(),null,0));
	}
}
