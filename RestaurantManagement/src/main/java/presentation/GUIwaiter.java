package presentation;

import businessLayer.*;
import businessLayer.MenuItem;
import dataLayer.RestaurantSerializator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUIwaiter extends JFrame implements ActionListener
{
	Restaurant restaurant= new Restaurant();
	RestaurantSerializator serializator= new RestaurantSerializator();
	ArrayList<Order> orders= new ArrayList<Order>();


	public JFrame appFrame;
	public JPanel panel;
	public JLabel detaliiComanda,labelFeluri;
	public JLabel labelComandaID, labelOra,labelMinut, labelMasa;
	public JLabel labelFelul1, labelFelul2;
	public JTextField txtComandaID, txtOra,txtMinut,txtMasa;
	public JLabel numeFelul1,numeFelul2;
	public JTextField numeF1,numeF2, rezultatPrice;
	public JButton btnDetaliiComanda, btnFel1,btnFel2, computePrice, generateBill, afisareComenzi;
	public JLabel compute;
	public JLabel idCompute, idBill;
	public JTextField idTextCompute, idTextBill;
	public Container cPane;
	
	public GUIwaiter()
	{
		makeGUIwaiter();
	}
	
	private void makeGUIwaiter() 
	{
		appFrame= new JFrame("Waiter");
		appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cPane= appFrame.getContentPane();
		cPane.setLayout(null);
		appFrame.setLayout(null);
		
		arrangeComponents();
		appFrame.pack();
		appFrame.setSize(550,450);
		appFrame.setLocation(500,100);
		appFrame.setVisible(true);	
		
	}

	private void arrangeComponents() 
	{
		detaliiComanda= new JLabel("Detalii comanda:");
		detaliiComanda.setBounds(10, 30, 300, 50);
		
		labelComandaID= new JLabel("ID comanda");
		labelComandaID.setBounds(40, 50, 300, 50);
		
		txtComandaID= new JTextField();
		txtComandaID.setBounds(50, 90, 30, 20);
		
		labelOra= new JLabel("Ora");
		labelOra.setBounds(140, 50, 300, 50);
		
		txtOra= new JTextField();
		txtOra.setBounds(140, 90, 30, 20);
		
		labelMinut= new JLabel("Minut");
		labelMinut.setBounds(210, 50, 300, 50);
		
		txtMinut= new JTextField();
		txtMinut.setBounds(210, 90, 30, 20);
		
		labelMasa= new JLabel("Masa");
		labelMasa.setBounds(280, 50, 300, 50);
		
		txtMasa= new JTextField();
		txtMasa.setBounds(280, 90, 30, 20);
		
		btnDetaliiComanda= new JButton("Comanda");
		btnDetaliiComanda.setBounds(350, 100, 150, 30);
		
		labelFeluri= new JLabel("Feluri mancare:");
		labelFeluri.setBounds(10, 120, 300, 50);
		
		labelFelul1= new JLabel("Felul 1:");
		labelFelul1.setBounds(40, 160, 300, 50);
		
		numeFelul1= new JLabel("Nume");
		numeFelul1.setBounds(105, 143, 300, 50);
			
		numeF1= new JTextField();
		numeF1.setBounds(85, 175, 100, 20);
		
		labelFelul2= new JLabel("Felul 2: ");
		labelFelul2.setBounds(240, 160, 300, 50);
		
		numeFelul2= new JLabel("Nume");
		numeFelul2.setBounds(305, 143, 300, 50);
		
		numeF2= new JTextField();
		numeF2.setBounds(285, 175, 100, 20);

		idCompute= new JLabel("Id comanda:");
		idCompute.setBounds(10,260,300,30);
		cPane.add(idCompute);

		idTextCompute= new JTextField();
		idTextCompute.setBounds(80,265,100,20);
		cPane.add(idTextCompute);

		computePrice= new JButton("Compute price");
		computePrice.setBounds(210, 260, 150, 30);
		
		compute= new JLabel("=>");
		compute.setBounds(365, 250, 300, 50);
		
		rezultatPrice= new JTextField();
		rezultatPrice.setBounds(385, 265, 80, 20);

		idBill= new JLabel("Id comanda:");
		idBill.setBounds(10,350,150,30);
		cPane.add(idBill);

		idTextBill= new JTextField();
		idTextBill.setBounds(80,355,50,20);
		cPane.add(idTextBill);

		generateBill= new JButton("Generate bill");
		generateBill.setBounds(150, 350, 150, 30);

		afisareComenzi= new JButton("Afisare comenzi");
		afisareComenzi.setBounds(350,350,150,30);

		cPane.add(afisareComenzi);
		cPane.add(generateBill);
		cPane.add(rezultatPrice);
		cPane.add(compute);
		cPane.add(computePrice);
		cPane.add(numeF2);
		cPane.add(numeFelul2);
		cPane.add(labelFelul2);
		cPane.add(numeF1);
		cPane.add(numeFelul1);
		cPane.add(labelFelul1);
		cPane.add(labelFeluri);
		cPane.add(btnDetaliiComanda);
		cPane.add(txtMasa);
		cPane.add(labelMasa);
		cPane.add(txtMinut);
		cPane.add(labelMinut);
		cPane.add(txtOra);
		cPane.add(labelOra);
		cPane.add(txtComandaID);
		cPane.add(labelComandaID);
		cPane.add(detaliiComanda);
		
		generateBill.addActionListener(this);
		computePrice.addActionListener(this);
		btnDetaliiComanda.addActionListener(this);
		afisareComenzi.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==generateBill)
		{
			generateBill();
		}
		if(e.getSource()==computePrice)
		{
			computePrice();
		}
		if(e.getSource()==btnDetaliiComanda)
		{
			detaliiComanda();
		}
		if(e.getSource()==afisareComenzi)
		{
			afisareComenzi();
		}
	}

	private void afisareComenzi()
	{
		Table tableOrders= new Table();

		orders=serializator.deserializationOrder();
		restaurant.setOrders(orders);

		//System.out.println(restaurant.getOrders().size());
		tableOrders.tableOrders(tableOrders.retrieveProperties(null,restaurant.getOrders(),1));
	}

	private void detaliiComanda()
	{
		int idComanda= Integer.parseInt(txtComandaID.getText());
		int oraComanda= Integer.parseInt(txtOra.getText());
		int minutComanda= Integer.parseInt(txtMinut.getText());
		int masaComanda= Integer.parseInt(txtMasa.getText());

		String numeFel1= numeF1.getText();
		String numeFel2= numeF2.getText();
		String numeFinal=null;

		MenuItem fel1=null;
		MenuItem fel2=null;

		ArrayList<Order> comenzi=serializator.deserializationOrder();
		restaurant.setItems(serializator.deserialization());
		restaurant.setOrders(comenzi);

		if(numeF1.getText().isEmpty() && numeF2.getText().isEmpty())
			return;
		else
		{
			ArrayList<MenuItem> items=serializator.deserialization();

			for(MenuItem itm: items)
			{
				if(itm instanceof  BaseProduct)
				{
					if(((BaseProduct) itm).getNumeProduct().equalsIgnoreCase(numeFel1))
						fel1=itm;
				}
				else if(itm instanceof  CompositeProduct)
				{
					if(((CompositeProduct) itm).getNumeComposite().equalsIgnoreCase(numeFel1))
						fel1=itm;
				}
			}

			for(MenuItem itm: items)
			{
				if(itm instanceof  BaseProduct)
				{
					if(((BaseProduct) itm).getNumeProduct().equalsIgnoreCase(numeFel2))
						fel2=itm;
				}
				else if(itm instanceof  CompositeProduct)
				{
					if(((CompositeProduct) itm).getNumeComposite().equalsIgnoreCase(numeFel2))
						fel2=itm;
				}
			}

			if(fel1==null || fel2==null)
				return;
			else
			{
				numeFinal=numeFel1+"+"+numeFel2;
				Order order= new Order(idComanda,oraComanda,minutComanda,masaComanda,numeFinal);
				order.addItemInOrder(fel1);
				order.addItemInOrder(fel2);

				restaurant.createNewOrder(order);
				orders.add(order);
				//System.out.println(order.toString());
				restaurant.setOrders(orders);
				serializator.serializationOrder(orders);
			}
		}

		txtComandaID.setText(null);
		txtOra.setText(null);
		txtMinut.setText(null);
		txtMasa.setText(null);
		numeF1.setText(null);
		numeF2.setText(null);
	}

	private void computePrice()
	{
		String idComanta= idTextCompute.getText();
		double pret=0;

		restaurant.setOrders(serializator.deserializationOrder());
		ArrayList<Order> orders=restaurant.getOrders();
		//System.out.println(orders.size());
		for(Order order: orders)
		{
			if(order.getComandaId().equalsIgnoreCase(idComanta))
			{
				pret=restaurant.computePriceForAnOrder(order);
			}
		}

		rezultatPrice.setText(String.valueOf(pret));
		idTextCompute.setText(null);
	}

	private void generateBill() 
	{
		String idBill= idTextBill.getText();

		restaurant.setOrders(serializator.deserializationOrder());
		ArrayList<Order> orders= restaurant.getOrders();

		for(Order order: orders)
		{
			if(order.getComandaId().equalsIgnoreCase(idBill))
			{
				restaurant.generateBill(order);
			}
		}

		idTextBill.setText(null);
	}
}
