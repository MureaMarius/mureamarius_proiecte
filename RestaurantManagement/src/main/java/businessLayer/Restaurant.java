package businessLayer;

import dataLayer.RestaurantSerializator;
import presentation.GUIchef;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Restaurant extends Observable implements RestaurantProcessing
{
	RestaurantSerializator serializator= new RestaurantSerializator();

	protected HashMap<Order, ArrayList<MenuItem>> table;
	protected ArrayList<Order> orders;
	protected ArrayList<MenuItem> items;
	protected Order order;
	//GUIchef chef= new GUIchef();

	public Restaurant()
	{
		orders= serializator.deserializationOrder();
		items= new ArrayList<MenuItem>();
	}

	public void createNewMenuItem(MenuItem item)
	{
		assert item!=null;
		assert isWellFormed();

		try
		{
			items.add(item);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

		assert isWellFormed();
		assert item!=null;
	}

	public boolean isWellFormed()
	{
		if(this instanceof  Restaurant)
		{
			return true;
		}
		else
			return false;
	}

	public void deleteMenuItem(String numeItem, int OK)
	{
		assert numeItem!="";
		assert isWellFormed();

		try
		{
			if(OK==0)
			{
				for(MenuItem itm: items)
				{
					if(itm instanceof  BaseProduct)
					{
						BaseProduct base= (BaseProduct)itm;
						if(base.getNumeProduct().equalsIgnoreCase(numeItem))
						{
							items.remove(base);
							serializator.serialization(items);
						}

					}
				}
			}
			else if(OK==1)
			{
				for(MenuItem itm: items)
				{
					if(itm instanceof  CompositeProduct)
					{
						CompositeProduct composite= (CompositeProduct)itm;
						if(composite.getNumeComposite().equalsIgnoreCase(numeItem))
						{
							items.remove(composite);
							serializator.serialization(items);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

		assert isWellFormed();
		assert numeItem!="";
	}

	public void editMenuItem(String item, int cantitate, double pret)
	{
		items=serializator.deserialization();
		assert item!="";
		assert isWellFormed();

		try
		{
			for(MenuItem itm: items)
			{
				if (itm instanceof BaseProduct)
				{
					BaseProduct base = (BaseProduct) itm;

					if (base.getNumeProduct().equalsIgnoreCase(item))
					{
						base.setCantitateProduct(cantitate);
						base.setPretProduct(pret);

						serializator.serialization(items);
						return;
					}
				}
			}

			for(MenuItem itm: items)
			{
				if (itm instanceof CompositeProduct)
				{
					CompositeProduct composite = (CompositeProduct) itm;

					if (composite.getNumeComposite().equalsIgnoreCase(item))
					{
						composite.setCantitateComposite(cantitate);
						composite.setPretComposite(pret);

						serializator.serialization(items);
						return;
					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}

		assert isWellFormed();
		assert item!="";
	}

	public void createNewOrder(Order order)
	{
		assert order!=null;
		assert isWellFormed();
		this.orders=serializator.deserializationOrder();

		try
		{
			this.items=serializator.deserialization();
			table.put(order,this.items);
			orders.add(order);

			serializator.serializationOrder(this.getOrders());
			this.setOrders(orders);
			this.order=order;
			this.orders=serializator.deserializationOrder();
			setChanged();
			notifyObservers(this.items);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public double computePriceForAnOrder(Order order)
	{
		this.orders=serializator.deserializationOrder();
		double pretFinal=0;

		pretFinal=order.price();

		return pretFinal;
	}

	public void generateBill(Order order)
	{
		try
		{
			PrintWriter output= new PrintWriter("Chitanta"+order.getComandaId()+".txt");
			output.println("CHITANTA");

			output.println("Comanda cu nr "+order.getComandaId()+" are o suma de plata "+computePriceForAnOrder(order));
			output.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public String toString()
	{
		for(MenuItem item: items)
		{
			if(item instanceof BaseProduct)
			{
				BaseProduct product=(BaseProduct)item;
				System.out.println(item.toString());
			}

			if(item instanceof CompositeProduct)
			{
				CompositeProduct product= (CompositeProduct)item;
				System.out.println(item.toString());
			}
		}

		return null;
	}

	public HashMap<Order, ArrayList<MenuItem>> getTable()
	{
		return table;
	}

	public ArrayList<Order> getOrders()
	{
		return orders;
	}

	public ArrayList<MenuItem> getItems()
	{
		return items;
	}

	public void setItems(ArrayList<MenuItem> items)
	{
		this.items = items;
	}

	public void setTable(HashMap<Order, ArrayList<MenuItem>> table)
	{
		this.table = table;
	}

	/*public void afisareOrder()
	{
		for(Order order: this.orders)
		{
			order.afisare();
		}
	}*/

	public void setOrders(ArrayList<Order> orders)
	{
		this.orders = orders;
	}

	public Order getOrder()
	{
		return order;
	}
}
