package businessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable
{
	protected String numeProduct;
	protected int cantitateProduct;
	protected double pretProduct;

	public BaseProduct(String numeProduct,int cantitateProduct,double pretProduct)
	{
		super();
		this.numeProduct=numeProduct;
		this.cantitateProduct=cantitateProduct;
		this.pretProduct=pretProduct;
	}

	public String getNumeProduct() 
	{
		return numeProduct;
	}

	public void setNumeProduct(String numeProduct) 
	{
		this.numeProduct = numeProduct;
	}

	public double getPretProduct() 
	{
		return pretProduct;
	}

	public void setPretProduct(double pretProduct) 
	{
		this.pretProduct = pretProduct;
	}

	public int getCantitateProduct() 
	{
		return cantitateProduct;
	}

	public void setCantitateProduct(int cantitateProduct) 
	{
		this.cantitateProduct = cantitateProduct;
	}

	@Override
	public int computePrice()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString()
	{
		return "BaseProduct-ul "+numeProduct+" cantitate "+cantitateProduct+" pret "+pretProduct;
	}
}
