package businessLayer;

import java.util.ArrayList;

public interface RestaurantProcessing
{
	/*
		@pre !item!=null
		@invariant isWellFormed()
		@post item!=null
	*/
	void createNewMenuItem(MenuItem item);

	/*
		@pre item!=null
		@invariant isWellFormed()
		@post item!=null
	*/
	void deleteMenuItem(String item, int OK);

	/*
		@pre item!=""
		@invariant isWellFormed()
		@post item!=""
	*/
	void editMenuItem(String item, int cantitate, double pret);

	/*
		@pre item!=""
		@invariant isWellFormed()
		@post item!=""
	*/
	void createNewOrder(Order order);

	/*
		@pre item!=NULL
		@invariant isWellFormed()
		@post item!=NULL
	*/
	double computePriceForAnOrder(Order order);

	/*
		@pre item!=NULL
		@invariant isWellFormed()
		@post item!=NULL
	*/
	void generateBill(Order order);
}
