package businessLayer;

import java.util.ArrayList;

public abstract class MenuItem 
{
	public abstract int computePrice();
}
