package businessLayer;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class Order implements Serializable
{
	protected int comandaId;
	protected String numeItem;
	protected int ora;
	protected int minut;
	protected int masa;
	protected ArrayList<MenuItem> items;
	
	public Order(int comandaId, int ora, int minut, int masa, String nume)
	{
		this.comandaId=comandaId;
		this.ora=ora;
		this.minut=minut;
		this.masa=masa;
		this.numeItem=nume;
		this.items= new ArrayList<MenuItem>();
	}

	public void addItemInOrder(MenuItem item)
	{
		items.add(item);
	}

	public String getComandaId()
	{
		return String.valueOf(comandaId);
	}

	public void setComandaId(int comandaId)
	{
		this.comandaId = comandaId;
	}

	public String getOra()
	{
		return String.valueOf(ora);
	}

	public void setOra(int ora) 
	{
		this.ora = ora;
	}

	public String getMinut()
	{
		return String.valueOf(minut);
	}

	public void setMinut(int minut) 
	{
		this.minut = minut;
	}

	public String getMasa()
	{
		return String.valueOf(masa);
	}

	public void setMasa(int masa) 
	{
		this.masa = masa;
	}

	public String getNumeItem()
	{
		return numeItem;
	}

	public int hashCode()
	{
		return masa;
	}

	public void setNumeItem(String numeItem)
	{
		this.numeItem = numeItem;
	}

	public double price()
	{
		double pret=0;

		System.out.println(this.items.size());
		for(MenuItem itm: items)
		{
			if(itm instanceof  BaseProduct)
			{
				pret+=((BaseProduct) itm).getPretProduct();
				//System.out.println(((BaseProduct)itm).toString());
			}
			else if(itm instanceof  CompositeProduct)
			{
				pret+=((CompositeProduct) itm).getPretComposite();
				//System.out.println(((CompositeProduct)itm).toString());
			}
		}

		return  pret;
	}

	public void setItems(ArrayList<MenuItem> items)
	{
		this.items = items;
	}

	public ArrayList<MenuItem> getItems()
	{
		return items;
	}

	public String toString()
	{
		return getComandaId()+" "+getNumeItem()+" "+getOra()+" "+getMinut()+" "+getMasa();
	}
}
