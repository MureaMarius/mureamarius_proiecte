package businessLayer;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable
{	
	protected String numeComposite;
	protected int cantitateComposite;
	protected double pretComposite;
	public ArrayList<MenuItem> items;
	
	public CompositeProduct(String numeComposite, int cantitateComposite, double pretComposite)
	{
		this.numeComposite=numeComposite;
		this.cantitateComposite=cantitateComposite;
		this.pretComposite=pretComposite;
		items= new ArrayList<MenuItem>();
	}
	
	@Override
	public int computePrice()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void addMenuItem(MenuItem item)
	{
		this.items.add(item);
	}
	
	
	public String getNumeComposite() 
	{
		return numeComposite;
	}

	public void setNumeComposite(String numeComposite) 
	{
		this.numeComposite = numeComposite;
	}

	public int getCantitateComposite() 
	{
		return cantitateComposite;
	}

	public void setCantitateComposite(int cantitateComposite) 
	{
		this.cantitateComposite = cantitateComposite;
	}

	public double getPretComposite() 
	{
		return pretComposite;
	}

	public void setPretComposite(double pretComposite) 
	{
		this.pretComposite = pretComposite;
	}

	public String toString()
	{
		return "CompositeProduct "+this.numeComposite+" cantitate "+cantitateComposite+" pret "+pretComposite;
	}

	public void afisareBaseProducts()
	{
		for(MenuItem item: items)
		{
			if(item instanceof  BaseProduct)
				System.out.println(((BaseProduct) item).getNumeProduct());
		}
	}
}
