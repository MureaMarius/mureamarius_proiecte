package Assignment_5.Assignment_5;

import java.util.ArrayList;

import org.joda.time.DateTime;

public class MonitoredData 
{
	protected DateTime start;
	protected DateTime end;
	protected String activity;
	protected ArrayList<MonitoredData> data;
	
	public MonitoredData(DateTime start, DateTime end, String activity)
	{
		this.start=start;
		this.end=end;
		this.activity=activity;
		data= new ArrayList<MonitoredData>();
	}

	public DateTime getStart() 
	{
		return start;
	}

	public void setStart(DateTime start) 
	{
		this.start = start;
	}

	public DateTime getEnd() 
	{
		return end;
	}

	public void setEnd(DateTime end) 
	{
		this.end = end;
	}

	public String getActivity() 
	{
		return activity;
	}

	public void setActivity(String activity) 
	{
		this.activity = activity;
	}

	public ArrayList<MonitoredData> getData() 
	{
		return data;
	}

	public void setData(ArrayList<MonitoredData> data) 
	{
		this.data = data;
	}
	
	public String toString()
	{
		return "Monitored Data: start time "+ this.start+", end time "+this.end+", activity "+this.activity+"\r\n"; 
	}
}
