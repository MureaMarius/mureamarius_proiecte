package Assignment_5.Assignment_5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Test 
{
	private static ArrayList<MonitoredData> dataMonitorizata;
	private static DateTime startTime;
	private static DateTime endTime;
	
	public Test()
	{
		dataMonitorizata= new ArrayList<MonitoredData>();
	}
	
	public static void main(String[] args) throws IOException
	{
		String fileName="output.txt";
		String inputFileName= "Activity.txt";
		
		Test test= new Test();
		
		try
		{
			readFromFile(inputFileName);
			printOnFile(fileName);
			
			System.out.println("Number of distinct days that appear in Monitoring Data is "+countDaysData());
			
			System.out.println(countActivity());
			activityCount();
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}	
	}
		
	public static void readFromFile(String fileName) throws IOException
	{
		final DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
			
		try (Stream<String>stream=Files.lines(Paths.get(fileName)))
		{
			stream.forEach(s->
			{
				String[] split= s.split("\t\t");
				startTime=format.parseDateTime(split[0]);
				endTime= format.parseDateTime(split[1]);
				MonitoredData data= new MonitoredData(startTime,endTime,split[2]);
				dataMonitorizata.add(data);
			});
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static void printOnFile(String fileName) throws IOException
	{
		try
		{
			//BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out));
			FileWriter writer=  new FileWriter(fileName);
			int size= dataMonitorizata.size();
			
			for(int i=0; i<size; i++)
			{
				String str=dataMonitorizata.get(i).toString();
				writer.write(str);
				
				if(i<size-1)
				{
					writer.write("\n");
				}
			}
			
			writer.close();
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	public static int countDaysData()
	{
		int numberOfDistinctDays = (int) dataMonitorizata.stream().map(days -> days.getStart().getDayOfMonth()).distinct().count();
		
		return numberOfDistinctDays;
	}
	
	public static Map<String, Long> countActivity()
	{
		Map<String, Long> count = dataMonitorizata.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		
		return count;
	}
	
	public static void activityCount()
	{
		Map<Integer, Map<String, Long>> activityCount = dataMonitorizata.stream().collect(Collectors.groupingBy(d -> d.getStart().getDayOfMonth(),
				Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		
		File file2 = new File("ActivityCountForEachDay.txt");
		try 
		{
			FileOutputStream s2 = new FileOutputStream(file2);
			PrintWriter pw2 = new PrintWriter(s2);

			for (Entry<Integer, Map<String, Long>> m : activityCount.entrySet()) 
			{
				pw2.println(m.getKey() + " " + m.getValue());
			}
			pw2.flush();
			pw2.close();
			s2.close();
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
	}
}
