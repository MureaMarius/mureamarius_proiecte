package ro.utcn.is.lab2.service;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import ro.utcn.is.lab2.entity.AdminAccount;
import ro.utcn.is.lab2.entity.User;
import ro.utcn.is.lab2.repository.UserRepo;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class UserService {

    private UserRepo userRepo = new UserRepo();

    public String addUser(User user) {
        List<String> missingFields = new ArrayList<String>();
        if (user == null) {
            return "User " + ro.utcn.is.lab2.messages.Messages.DEFAULT_NULL_MESSAGE;
        }
        if (user.getAge() == null) {
            missingFields.add("age");
        }
        if (StringUtils.isEmpty(user.getName())) {
            missingFields.add("name");
        }
        if (CollectionUtils.isNotEmpty(missingFields)) {
            return "This field(s) are missing: " + missingFields.stream().collect(Collectors.joining(", "));
        }
        if (user.getAge() < 18) {
            return "Invalid age. Must be greater than 18";
        }

        user.setId(UUID.randomUUID().toString());
        user.setCreatedAt(new Date());
        userRepo.insert(user);
        return "OK. New user succesfully inserted.";

    }

    public String addAdmin(AdminAccount admin) {

        List<String> missingFields = new ArrayList<String>();

        if(admin == null) {
            JOptionPane.showMessageDialog(null, "Crezi ca-ti bati joc de mine, Radu ?!");
        }

        if(StringUtils.isEmpty(admin.getNameAdmin())) {
            JOptionPane.showMessageDialog(null,"Admin needs a name.");
            return "";
        }
        if(StringUtils.isEmpty(admin.getPasswordAdmin())) {
            JOptionPane.showMessageDialog(null,"Admin needs a password.");
            return "";
        }
        if(admin.getNameAdmin().equals("")) {
            JOptionPane.showMessageDialog(null, "Admin name can't be blank.");
            return "";
        }
        if(admin.getPasswordAdmin().equals("")) {
            JOptionPane.showMessageDialog(null, "Admin password can't be blank.");
            return "";
        }
        int lenPassword = admin.getPasswordAdmin().length();
        String pass = admin.getPasswordAdmin();
        boolean hasLower = false;
        boolean hasUpper = false;
        boolean hasDigit = false;


        for(int i = 0; i < lenPassword; i++ ) {
            if(Character.isLowerCase(pass.charAt(i))) {
                hasLower = true;
            }
            if(Character.isUpperCase(pass.charAt(i))) {
                hasUpper = true;
            }
            if(Character.isDigit(pass.charAt(i))) {
                hasDigit = true;
            }
        }
        if(hasLower && hasUpper && hasDigit && lenPassword > 8) {
            System.out.println("Strong password !");
        }
        else {
            JOptionPane.showMessageDialog(null, "Weak password. Try again. It must contains upper, lower and digit.");
            return "";
        }

        userRepo.insert(admin);
        return "New Admin Account inserted.";
    }

}
