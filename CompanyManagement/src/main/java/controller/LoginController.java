package controller;

import javax.swing.JOptionPane;

import ro.utcn.is.lab2.entity.AdminAccount;
import ro.utcn.is.lab2.repository.UserRepo;

public class LoginController {
    public void login(String numeIntrodus, String parolaIntrodusa) {

        UserRepo userRep = new UserRepo();
        AdminAccount admin = new AdminAccount();
        admin = userRep.findByName(numeIntrodus);

        int wrong = 0;

        if(numeIntrodus.equals("")) {
            JOptionPane.showMessageDialog(null,"Please insert an username !");
            return;
        }

        if(parolaIntrodusa.equals("")) {
            JOptionPane.showMessageDialog(null,"Please insert a password !");
            return;
        }

        if(numeIntrodus.equals(admin.getNameAdmin())) {
            if(parolaIntrodusa.equals(admin.getPasswordAdmin())) {
                System.out.println("SUCCESS ! ");
            }
            else {
                wrong++;
            }
        }
        else{
            wrong++;
        }
        if(wrong > 0) {
            JOptionPane.showMessageDialog(null,"Wrong username or password. Try again");

        }
    }

}
