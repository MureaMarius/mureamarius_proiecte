package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIMenu extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private ImageIcon image;
    private JLabel background;
    private JButton btnLogIn, btnAngajat, btnAplicant;

    public GUIMenu()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Meniu Principal");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(1000,500);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        btnLogIn= new JButton("Log-In");
        btnLogIn.setBounds(700,100,200,50);
        btnLogIn.setBackground(Color.WHITE);
        btnLogIn.setForeground(Color.BLACK);
        btnLogIn.setFont(new Font("Times New Roman", Font.BOLD, 40));
        cPane.add(btnLogIn);

        btnAngajat= new JButton("Angajat");
        btnAngajat.setBounds(700,300,200,50);
        btnAngajat.setBackground(Color.WHITE);
        btnAngajat.setForeground(Color.BLACK);
        btnLogIn.setFont(new Font("Times New Roman", Font.BOLD, 40));
        cPane.add(btnAngajat);

        image= new ImageIcon("bits-over-atoms-peng-ong-asset-light-monk's-hill.jpg");
        background= new JLabel("",image,JLabel.CENTER);
        background.setBounds(0,0,1000,500);
        cPane.add(background);

        btnLogIn.addActionListener(this);
        btnAngajat.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        try
        {
            if (actionEvent.getSource() == btnLogIn)
            {
                guiLogIN();
                appFrame.setVisible(false);
            }
            else if(actionEvent.getSource()==btnAngajat)
            {
                //guiAngaj()
                this.guiAngaj();
                appFrame.setVisible(false);
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private void guiAngaj()
    {
        GUIAngajat angajat= new GUIAngajat();

    }

    private void guiLogIN()
    {
        GUIAdmin admin= new GUIAdmin();
    }
}
