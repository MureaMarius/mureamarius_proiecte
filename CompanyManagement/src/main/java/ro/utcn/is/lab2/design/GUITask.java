package ro.utcn.is.lab2.design;

import ro.utcn.is.lab2.entity.Task;
import ro.utcn.is.lab2.repository.TaskRepo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;

public class GUITask extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private ImageIcon image;
    private JLabel task;
    private JLabel lblNumeTask, lblDificultateTask, lblAngajatLaCareSADistribuit, lblDeadline;
    private JButton adaugareTask, eliminareTask, distribuieTask, goBack;
    private JTextField txtNume, txtDificultate, txtAngajat, txtDeadline;

    public GUITask()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("TASK");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(570,500);
        appFrame.setLocation(250,100);
        cPane.setBackground(new Color(144, 201, 255));
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        goBack= new JButton("Inapoi la Administrator");
        goBack.setBounds(20,20,200,50);
        cPane.add(goBack);

        adaugareTask= new JButton("Adaugă");
        adaugareTask.setBounds(20,350,150,50);
        cPane.add(adaugareTask);

        eliminareTask= new JButton("Elimină");
        eliminareTask.setBounds(200,350,150,50);
        cPane.add(eliminareTask);

        distribuieTask= new JButton("Distribuie");
        distribuieTask.setBounds(370,350,150,50);
        cPane.add(distribuieTask);

        lblNumeTask= new JLabel("Nume:");
        lblNumeTask.setBounds(20,170,150,50);
        cPane.add(lblNumeTask);

        txtNume= new JTextField();
        txtNume.setBounds(70,180,150,30);
        cPane.add(txtNume);

        lblDificultateTask= new JLabel("Dificultate: ");
        lblDificultateTask.setBounds(20,250,150,50);
        cPane.add(lblDificultateTask);

        txtDificultate= new JTextField();
        txtDificultate.setBounds(90,260,150,30);
        cPane.add(txtDificultate);

        lblAngajatLaCareSADistribuit= new JLabel("Distribuit la: ");
        lblAngajatLaCareSADistribuit.setBounds(300,170,150,50);
        cPane.add(lblAngajatLaCareSADistribuit);

        txtAngajat= new JTextField();
        txtAngajat.setBounds(380,180,150,30);
        cPane.add(txtAngajat);

        lblDeadline= new JLabel("Deadline: ");
        lblDeadline.setBounds(300,250,150,50);
        cPane.add(lblDeadline);

        txtDeadline= new JTextField();
        txtDeadline.setBounds(360,260,150,30);
        cPane.add(txtDeadline);

        image= new ImageIcon("task.png");
        task= new JLabel("",image,JLabel.CENTER);
        task.setBounds(350,0,150,150);
        cPane.add(task);

        goBack.addActionListener(this);
        adaugareTask.addActionListener(this);
        eliminareTask.addActionListener(this);
        distribuieTask.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        try
        {
            if(actionEvent.getSource()==goBack)
            {
                back();
            }
            if(actionEvent.getSource()==adaugareTask)
            {
                adauga();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void adauga() throws ParseException
    {
        String nume= txtNume.getText();
        String dificultate= txtDificultate.getText();
        String angajat= txtAngajat.getText();
        Date data= new SimpleDateFormat("dd/MM/yyyy").parse(txtDeadline.getText());

        TaskRepo tsk= new TaskRepo();
        Task task= new Task();

        task.setNumeTask(nume);
        task.setDificultateTask(dificultate);
        task.setAngajatDistribuitLaTask(angajat);
        task.setDeadline(data);

        tsk.insert(task);
    }

    private void back()
    {
        GUIAdmin admin= new GUIAdmin();
        appFrame.setVisible(false);
    }
}
