package ro.utcn.is.lab2.entity;

public class Document
{
    private String taskAferent;
    private String numeAngajatCeRezolvaTask;

    public Document(String taskAferent, String numeAngajatCeRezolvaTask)
    {
        this.taskAferent=taskAferent;
        this.numeAngajatCeRezolvaTask=numeAngajatCeRezolvaTask;
    }

    public String getTaskAferent()
    {
        return taskAferent;
    }

    public void setTaskAferent(String taskAferent)
    {
        this.taskAferent = taskAferent;
    }

    public String getNumeAngajatCeRezolvaTask()
    {
        return numeAngajatCeRezolvaTask;
    }

    public void setNumeAngajatCeRezolvaTask(String numeAngajatCeRezolvaTask)
    {
        this.numeAngajatCeRezolvaTask = numeAngajatCeRezolvaTask;
    }

    @Override
    public String toString()
    {
        return "Document{" +
                "taskAferent='" + taskAferent + '\'' +
                ", numeAngajatCeRezolvaTask='" + numeAngajatCeRezolvaTask + '\'' +
                '}';
    }
}
