package ro.utcn.is.lab2.entity;

import java.util.Date;

public class Task
{
    private String numeTask;
    private String dificultateTask;
    private String angajatDistribuitLaTask;
    private boolean rezolvat;
    private Date deadline;

    public Task()
    {

    }

    public Task(String numeTask, String dificultateTask, String angajatDistribuitLaTask, Date deadline)
    {
        this.numeTask=numeTask;
        this.dificultateTask=dificultateTask;
        this.angajatDistribuitLaTask=angajatDistribuitLaTask;
        this.deadline=deadline;
    }

    public String getNumeTask()
    {
        return numeTask;
    }

    public void setNumeTask(String numeTask)
    {
        this.numeTask = numeTask;
    }

    public String getDificultateTask()
    {
        return dificultateTask;
    }

    public void setDificultateTask(String dificultateTask)
    {
        this.dificultateTask = dificultateTask;
    }

    public String getAngajatDistribuitLaTask()
    {
        return angajatDistribuitLaTask;
    }

    public void setAngajatDistribuitLaTask(String angajatDistribuitLaTask)
    {
        this.angajatDistribuitLaTask = angajatDistribuitLaTask;
    }

    public boolean isRezolvat() {
        return rezolvat;
    }

    public void setRezolvat(boolean rezolvat) {
        this.rezolvat = rezolvat;
    }

    public Date getDeadline()
    {
        return deadline;
    }

    public void setDeadline(Date deadline) {

        this.deadline = deadline;
    }

    @Override
    public String toString()
    {
        return "Task{" +
                "numeTask='" + numeTask + '\'' +
                ", dificultateTask='" + dificultateTask + '\'' +
                ", angajatDistribuitLaTask='" + angajatDistribuitLaTask + '\'' +
                ", deadline=" + deadline +
                '}';
    }
}
