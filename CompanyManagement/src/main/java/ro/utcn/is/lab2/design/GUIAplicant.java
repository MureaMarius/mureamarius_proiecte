package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIAplicant
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private JLabel lblNumePost, lblNumeAplicant, lblEmail;
    private JTextField txtNume, txtAplicant, txtEmail;
    private JButton btnAplicarePost, btnBack;

    public GUIAplicant()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Aplicant Post");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(600,300);
        appFrame.setLocation(250,100);
        cPane.setBackground(new Color(144, 201, 255));
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        lblNumePost= new JLabel("Nume post:");
        lblNumePost.setBounds(50,50,150,50);;
        cPane.add(lblNumePost);

        txtNume= new JTextField();
        txtNume.setBounds(120,60,150,30);
        cPane.add(txtNume);

        lblNumeAplicant= new JLabel("Nume aplicant:");
        lblNumeAplicant.setBounds(50,120,150,50);
        cPane.add(lblNumeAplicant);

        txtAplicant= new JTextField();
        txtAplicant.setBounds(140,130,150,30);
        cPane.add(txtAplicant);

        lblEmail= new JLabel("Email: ");
        lblEmail.setBounds(50,190,150,50);
        cPane.add(lblEmail);

        txtEmail= new JTextField();
        txtEmail.setBounds(90,200,150,30);
        cPane.add(txtEmail);

        btnAplicarePost= new JButton("Aplica post!");
        btnAplicarePost.setBounds(370,130,150,30);
        cPane.add(btnAplicarePost);

        btnBack= new JButton("Back");
        btnBack.setBounds(10,10,100,30);
        cPane.add(btnBack);
    }
}
