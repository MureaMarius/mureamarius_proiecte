package ro.utcn.is.lab2.entity;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.LoginController;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import javax.swing.*;

public class GuiLogin extends JFrame {

    private JPanel contentPane;
    private JTextField textField;

    private String nume="admin";
    private String parola="admin";
    private JPasswordField passwordField;

    private LoginController loginController = new LoginController();



    /**
     * Launch the application.
     */

    /**
     * Create the frame.
     */
    public GuiLogin() {



        JLabel background;
        ImageIcon image = new ImageIcon("alex.png");
        background = new JLabel("",image,JLabel.CENTER);
        background.setBounds(0, 173,216,230);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 700, 400);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(173, 216, 230));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);


        textField = new JTextField();
        textField.setBounds(202, 96, 303, 42);
        contentPane.add(textField);
        textField.setColumns(10);


        this.setNume("admin");
        this.setParola("admin");

        JButton btnNewButton = new JButton("Login");
        btnNewButton.setBounds(202, 257, 152, 42);
        btnNewButton.addActionListener(e -> loginController.login(textField.getText(), passwordField.getText()));
        btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 16));
        contentPane.add(btnNewButton);


        JButton btnNewButton_1 = new JButton("Close");
        btnNewButton_1.setBounds(364, 257, 138, 42);
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });
        btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        contentPane.add(btnNewButton_1);

        JLabel lblNewLabel = new JLabel("New label");
        lblNewLabel.setBounds(147, 125, 45, -28);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("New label");
        lblNewLabel_1.setBounds(125, 83, 67, 66);
        lblNewLabel_1.setIcon(new ImageIcon(GuiLogin.class.getResource("/Images/Webp.net-resizeimage (2).png")));
        contentPane.add(lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setBounds(125, 159, 67, 70);
        lblNewLabel_2.setIcon(new ImageIcon(GuiLogin.class.getResource("/Images/Webp.net-resizeimage (1).png")));
        contentPane.add(lblNewLabel_2);

        JLabel lblNewLabel_3 = new JLabel("LOGIN");
        lblNewLabel_3.setBounds(305, 29, 90, 57);
        lblNewLabel_3.setForeground(new Color(255, 69, 0));
        lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 25));
        contentPane.add(lblNewLabel_3);

        passwordField = new JPasswordField();
        passwordField.setBounds(202, 172, 303, 42);
        contentPane.add(passwordField);

        JCheckBox chckbxNewCheckBox = new JCheckBox("See password");
        chckbxNewCheckBox.setBounds(202, 220, 132, 31);
        chckbxNewCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (chckbxNewCheckBox.isSelected()) {
                    passwordField.setEchoChar((char)0);
                }
                else
                {
                    passwordField.setEchoChar('\u2022');
                }
            }
        });
        chckbxNewCheckBox.setFont(new Font("Times New Roman", Font.BOLD, 14));
        contentPane.add(chckbxNewCheckBox);

        //setUndecorated(true);
        this.setVisible(true);

    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }
}
