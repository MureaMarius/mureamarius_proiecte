package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIAdmin extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private ImageIcon image;
    private JLabel background;
    private JButton btnTask, btnAngajat, btnBack, btnAplicant;

    public GUIAdmin()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("ADMINISTRATOR");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(1000,500);
        appFrame.setLocation(250,100);

    }

    private void arrangeComponents()
    {
        cPane.setLayout(null);
        appFrame.setLayout(null);

        btnTask= new JButton("TASK");
        btnTask.setBounds(700,50,250,50);
        btnTask.setBackground(Color.WHITE);
        btnTask.setForeground(Color.BLACK);
        btnTask.setFont(new Font("Times New Roman", Font.ITALIC, 40));

        btnAngajat= new JButton("ANGAJAT");
        btnAngajat.setBounds(700,120,250,50);
        btnAngajat.setVisible(true);
        btnAngajat.setBackground(Color.WHITE);
        btnAngajat.setForeground(Color.BLACK);
        btnAngajat.setFont(new Font("Times New Roman", Font.ITALIC, 40));

        btnBack= new JButton("Log-In menu");
        btnBack.setBounds(20,80,150,50);

        btnAplicant= new JButton("Aplicanti");
        btnAplicant.setBounds(700,190,250,50);
        btnTask.setBackground(Color.WHITE);
        btnTask.setForeground(Color.BLACK);
        btnTask.setFont(new Font("Times New Roman", Font.ITALIC, 40));

        cPane.add(btnAngajat);
        cPane.add(btnTask);
        cPane.add(btnBack);
        cPane.add(btnAplicant);

        image= new ImageIcon("ogimage.png");
        background= new JLabel("",image,JLabel.CENTER);
        background.setBounds(0,0,1000,500);
        cPane.add(background);

        btnBack.addActionListener(this);
        btnTask.addActionListener(this);
        btnAngajat.addActionListener(this);
        btnAplicant.addActionListener(this);

        btnAngajat.setVisible(true);
        btnBack.setVisible(true);
        appFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        try
        {
            if(actionEvent.getSource()==btnBack)
            {
                goBack();
                appFrame.setVisible(false);
            }

            if(actionEvent.getSource()==btnTask)
            {
                goTask();
            }

            if(actionEvent.getSource()==btnAngajat)
            {
                angajat();
            }
            if(actionEvent.getSource()==btnAplicant)
            {
                aplicant();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void aplicant()
    {
        GUIAdmin_Aplicant aplicant= new GUIAdmin_Aplicant();
        appFrame.setVisible(false);
    }

    private void angajat()
    {
        GUIAdmin_Angajat angajat= new GUIAdmin_Angajat();
        appFrame.setVisible(false);
    }

    private void goTask()
    {
        GUITask task= new GUITask();
        appFrame.setVisible(false);
    }

    private void goBack()
    {
        GUIMenu logInMenu= new GUIMenu();
        appFrame.setVisible(false);
    }
}
