package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIAdmin_Aplicant extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private JLabel lblNumeAplicant, lblEmail, lblNumePost;
    private JTextField txtNumeAplicant, txtEmail, txtNumePost;
    private JButton btnAngajeaza, btnBack;

    public GUIAdmin_Aplicant()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Aplicant");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(550,300);
        appFrame.setLocation(250,100);
        cPane.setBackground(new Color(144, 201, 255));
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        lblNumePost= new JLabel("Nume post:");
        lblNumePost.setBounds(50,50,150,50);;
        cPane.add(lblNumePost);

        txtNumePost= new JTextField();
        txtNumePost.setBounds(120,60,150,30);
        cPane.add(txtNumePost);

        lblNumeAplicant= new JLabel("Nume aplicant:");
        lblNumeAplicant.setBounds(50,120,150,50);
        cPane.add(lblNumeAplicant);

        txtNumeAplicant= new JTextField();
        txtNumeAplicant.setBounds(140,130,150,30);
        cPane.add(txtNumeAplicant);

        lblEmail= new JLabel("Email: ");
        lblEmail.setBounds(50,190,150,50);
        cPane.add(lblEmail);

        txtEmail= new JTextField();
        txtEmail.setBounds(90,200,150,30);
        cPane.add(txtEmail);

        btnAngajeaza= new JButton("Angajeaza!");
        btnAngajeaza.setBounds(350,100,150,30);
        cPane.add(btnAngajeaza);

        btnBack= new JButton("Back!");
        btnBack.setBounds(10,10,100,30);
        cPane.add(btnBack);

        btnBack.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        if(actionEvent.getSource()==btnBack)
        {
            back();
        }
    }

    private void back()
    {
        GUIAdmin admin= new GUIAdmin();
        appFrame.setVisible(false);
    }
}
