package ro.utcn.is.lab2.entity;

public class Admin
{
    private String numeAdmin;

    public Admin(String numeAdmin)
    {
        this.numeAdmin=numeAdmin;
    }

    public String getNumeAdmin()
    {
        return numeAdmin;
    }

    public void setNumeAdmin(String numeAdmin)
    {
        this.numeAdmin = numeAdmin;
    }

    @Override
    public String toString()
    {
        return "Admin{" +
                "numeAdmin='" + numeAdmin + '\'' +
                '}';
    }
}
