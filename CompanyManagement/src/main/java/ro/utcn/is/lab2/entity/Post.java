package ro.utcn.is.lab2.entity;

public class Post
{
    private String numePost;
    private String email;
    private boolean disponibilitate;
    private int locuriDisponibile;
    private String numeUserCeAplica;
    //private String dificultate;

    public Post(String numePost, boolean disponibilitate, int locuriDisponibile, String numeUserCeAplica, String dificultate)
    {
        this.numePost=numePost;
        this.disponibilitate=disponibilitate;
        this.locuriDisponibile=locuriDisponibile;
        this.numeUserCeAplica=numeUserCeAplica;
        //this.dificultate=dificultate;
    }

    public String getNumePost()
    {
        return numePost;
    }

    public void setNumePost(String numePost)
    {
        this.numePost = numePost;
    }

    public boolean isDisponibilitate()
    {
        return disponibilitate;
    }

    public void setDisponibilitate(boolean disponibilitate)
    {
        this.disponibilitate = disponibilitate;
    }

    public int getLocuriDisponibile()
    {
        return locuriDisponibile;
    }

    public void setLocuriDisponibile(int locuriDisponibile)
    {
        this.locuriDisponibile = locuriDisponibile;
    }

    public String getNumeAngajatCeAplica()
    {
        return numeUserCeAplica;
    }

    public void setNumeAngajatCeAplica(String numeAngajatCeAplica)
    {
        this.numeUserCeAplica = numeAngajatCeAplica;
    }

    /*public String getDificultate()
    {
        return dificultate;
    }

    public void setDificultate(String dificultate)
    {
        this.dificultate = dificultate;
    }*/

    @Override
    public String toString()
    {
        return "Post{" +
                "numePost='" + numePost + '\'' +
                ", disponibilitate=" + disponibilitate +
                ", locuriDisponibile=" + locuriDisponibile +
                ", numeAngajatCeAplica='" + numeUserCeAplica + '\''+
                '}';
    }
}
