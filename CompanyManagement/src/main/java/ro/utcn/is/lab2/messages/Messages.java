package ro.utcn.is.lab2.messages;

public class Messages {

    public static final String DEFAULT_NULL_MESSAGE = " is null";
    public static final String DEFAULT_ADMIN_MESSAGE = "Cont de admin invalid";
    public static final String NO_NAME = "Campul NAME nu poate fi gol";
    public static final String NO_PASSWORD = "Campul PASSWORD nu poate fi gol";
    public static final String SHORT_PASSWORD = "Parola este prea scurta";
}
