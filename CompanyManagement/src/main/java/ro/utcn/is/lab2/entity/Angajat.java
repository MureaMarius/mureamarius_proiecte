package ro.utcn.is.lab2.entity;

public class Angajat
{
    private int idAngajat;
    private String numeAngajat;
    private String prenumeAngajat;
    private String specialitateAngajat;
    private String taskDeRezolvat;
    private int notaAngajat;

    public Angajat(int idAngajat, String numeAngajat, String prenumeAngajat, String specialitateAngajat, String taskDeRezolvat, int notaAngajat)
    {
        this.idAngajat=idAngajat;
        this.numeAngajat=numeAngajat;
        this.prenumeAngajat=prenumeAngajat;
        this.specialitateAngajat=specialitateAngajat;
        this.taskDeRezolvat=taskDeRezolvat;
        this.notaAngajat=notaAngajat;
    }

    public int getIdAngajat()
    {
        return idAngajat;
    }

    public void setIdAngajat(int idAngajat)
    {
        this.idAngajat = idAngajat;
    }

    public String getNumeAngajat()
    {
        return numeAngajat;
    }

    public void setNumeAngajat(String numeAngajat)
    {
        this.numeAngajat = numeAngajat;
    }

    public String getPrenumeAngajat()
    {
        return prenumeAngajat;
    }

    public void setPrenumeAngajat(String prenumeAngajat)
    {
        this.prenumeAngajat = prenumeAngajat;
    }

    public String getSpecialitateAngajat()
    {
        return specialitateAngajat;
    }

    public void setSpecialitateAngajat(String specialitateAngajat)
    {
        this.specialitateAngajat = specialitateAngajat;
    }

    public String getTaskDeRezolvat()
    {
        return taskDeRezolvat;
    }

    public void setTaskDeRezolvat(String taskDeRezolvat)
    {
        this.taskDeRezolvat = taskDeRezolvat;
    }

    public int getNotaAngajat()
    {
        return notaAngajat;
    }

    public void setNotaAngajat(int notaAngajat)
    {
        this.notaAngajat = notaAngajat;
    }

    @Override
    public String toString()
    {
        return "Angajat{" +
                "idAngajat=" + idAngajat +
                ", numeAngajat='" + numeAngajat + '\'' +
                ", prenumeAngajat='" + prenumeAngajat + '\'' +
                ", specialitateAngajat='" + specialitateAngajat + '\'' +
                ", taskDeRezolvat='" + taskDeRezolvat + '\'' +
                ", notaAngajat=" + notaAngajat +
                '}';
    }
}
