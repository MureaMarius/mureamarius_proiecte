package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIAdmin_Angajat extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private ImageIcon imagine;
    private JLabel angajat;
    private JButton btnConcediere, btnEvaluare, btnBack;
    private JLabel lblIdAngajat, lblNumeAngajat, lblPrenumeAngajat, lblSpecialitateAngajat, lblTask, lblNotaAngajat;
    private JTextField txtIdAngajat, txtNumeAngajat, txtPrenumeAngajat, txtSpecialitateAngajat, txtTask, txtNotaAngajat;

    public GUIAdmin_Angajat()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("Angajat");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(600,400);
        appFrame.setLocation(250,100);
        cPane.setBackground(new Color(144, 201, 255));
        appFrame.setVisible(true);
    }

    private void arrangeComponents()
    {
        cPane.setLayout(null);
        appFrame.setLayout(null);


        imagine= new ImageIcon("employee.png");
        angajat= new JLabel("",imagine,JLabel.CENTER);
        angajat.setBounds(400,30,100,100);
        cPane.add(angajat);


        btnBack= new JButton("Inapoi la Admin");
        btnBack.setBounds(10,10,150,30);
        cPane.add(btnBack);

        lblIdAngajat= new JLabel("ID Angajat:");
        lblIdAngajat.setBounds(30,50,150,50);
        cPane.add(lblIdAngajat);

        txtIdAngajat= new JTextField();
        txtIdAngajat.setBounds(100,60,150,30);
        cPane.add(txtIdAngajat);

        lblNumeAngajat= new JLabel("Nume angajat:");
        lblNumeAngajat.setBounds(30,100,150,50);
        cPane.add(lblNumeAngajat);

        txtNumeAngajat= new JTextField();
        txtNumeAngajat.setBounds(120,110,150,30);
        cPane.add(txtNumeAngajat);

        lblPrenumeAngajat= new JLabel("Prenume angajat:");
        lblPrenumeAngajat.setBounds(30,150,150,50);
        cPane.add(lblPrenumeAngajat);

        txtPrenumeAngajat= new JTextField();
        txtPrenumeAngajat.setBounds(135,160,150,30);
        cPane.add(txtPrenumeAngajat);

        lblSpecialitateAngajat= new JLabel("Specialitate angajat:");
        lblSpecialitateAngajat.setBounds(30,200,150,50);
        cPane.add(lblSpecialitateAngajat);

        txtSpecialitateAngajat= new JTextField();
        txtSpecialitateAngajat.setBounds(150,210,150,30);
        cPane.add(txtSpecialitateAngajat);

        lblTask= new JLabel("Task angajat:");
        lblTask.setBounds(30,250,150,50);
        cPane.add(lblTask);

        txtTask= new JTextField();
        txtTask.setBounds(120,260,150,30);
        cPane.add(txtTask);

        lblNotaAngajat= new JLabel("Nota angajat:");
        lblNotaAngajat.setBounds(30,300,150,50);
        cPane.add(lblNotaAngajat);

        txtNotaAngajat= new JTextField();
        txtNotaAngajat.setBounds(130,310,150,30);
        cPane.add(txtNotaAngajat);

        /*btnAngajare= new JButton("Angajare!");
        btnAngajare.setBounds(370,210,150,30);
        cPane.add(btnAngajare);*/

        btnConcediere= new JButton("Concediere!");
        btnConcediere.setBounds(370,260,150,30);
        cPane.add(btnConcediere);

        btnEvaluare= new JButton("Evaluare!");
        btnEvaluare.setBounds(370,310,150,30);
        cPane.add(btnEvaluare);

        btnBack.addActionListener(this);
        //btnAngajare.addActionListener(this);
        btnConcediere.addActionListener(this);
        btnEvaluare.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        try
        {
            if(actionEvent.getSource()==btnBack)
            {
                back();
            }
            /*if(actionEvent.getSource()==btnAngajare)
            {
                angajare();
            }*/
            if(actionEvent.getSource()==btnConcediere)
            {
                concediere();
            }
            if(actionEvent.getSource()==btnEvaluare)
            {
                evaluare();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void evaluare()
    {

    }

    private void concediere()
    {

    }

    private void angajare()
    {

    }

    private void back()
    {
        GUIAdmin admin= new GUIAdmin();
        appFrame.setVisible(false);
    }
}
