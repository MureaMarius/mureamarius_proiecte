package ro.utcn.is.lab2.design;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GUIAngajat extends JFrame implements ActionListener
{
    private JFrame appFrame;
    private JPanel panel;
    private Container cPane;
    private ImageIcon image;
    private JLabel background;
    private JButton btnTask, btnDocument, btnBack;

    public GUIAngajat()
    {
        makeGUI();
    }

    private void makeGUI()
    {
        appFrame= new JFrame("ANGAJAT");
        appFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        cPane= appFrame.getContentPane();
        cPane.setLayout(null);
        appFrame.setLayout(null);
        appFrame.setVisible(true);

        arrangeComponents();
        appFrame.pack();
        appFrame.setSize(900,450);
        appFrame.setLocation(250,100);
        appFrame.setVisible(true);
    }
    private void arrangeComponents()
    {
        cPane.setLayout(null);
        appFrame.setLayout(null);

        btnBack= new JButton("Log-In menu");
        btnBack.setBounds(20,50,150,50);
        cPane.add(btnBack);

        btnTask= new JButton("Task");
        btnTask.setBounds(700,50,150,50);
        cPane.add(btnTask);

        btnDocument= new JButton("Document");
        btnDocument.setBounds(700,130,150,50);
        cPane.add(btnDocument);

        image= new ImageIcon("angajat.jpg");
        background= new JLabel("",image,JLabel.CENTER);
        background.setBounds(0,0,900,450);
        cPane.add(background);

        btnBack.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        if(actionEvent.getSource()==btnBack)
        {
            back();
            this.appFrame.setVisible(false);
        }
    }

    private void back()
    {
        GUIMenu menu= new GUIMenu();
    }
}
