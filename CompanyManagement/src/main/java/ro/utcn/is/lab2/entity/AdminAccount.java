package ro.utcn.is.lab2.entity;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
public class AdminAccount {

    @Id
    private String nameAdmin;

    @Column
    private String passwordAdmin;

    public String getNameAdmin() {
        return nameAdmin;
    }

    public void setNameAdmin(String nameAdmin) {
        this.nameAdmin = nameAdmin;
    }

    public String getPasswordAdmin() {
        return passwordAdmin;
    }

    public void setPasswordAdmin(String passwordAdmin) {
        this.passwordAdmin = passwordAdmin;
    }




}
