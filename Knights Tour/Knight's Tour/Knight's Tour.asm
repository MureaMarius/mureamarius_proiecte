.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc
extern printf: proc

includelib canvas.lib
extern BeginDrawing: proc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date


matrice DD 64 dup(0)
window_title DB "Knight's Tour", 0
area_width EQU 640 	    ;dimensiunea tablei de sah in pixeli
area_height EQU 480 	;dimensiunea tablei de sah in pixeli
area DD 0

patrat_width EQU 50 ;dimensiune patrat din tabel
patrat_height EQU 50 ;dimensiune patrat din tabel
ver_afara DD 0 ;variabila ce am folosit-o pe post de valoare BOOLEANA;verifica daca click-ul s-a dat in tabla sau nu
numarator DD 64		;variabila ce va fi folosita pt a contoriza numarul de mutari executate corect pe tabla maxim (tabla de sah 8 pe 8 => 64 mutari posibile)
counter DD 0
arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20
X0 DD 0
Y0 DD 50
x DD 50
Y DD 50
xpost dd 50
ypost dd 0
xpostMat dd 0
ypostMat dd 0
xVef dd 0
yVef dd 0
symbol_width EQU 10
symbol_height EQU 20
culoare_simbol DD 0
cal_height EQU 50			;dimensiunea piesei de cal
cal_width EQU 50			;dimensiunea piesei de cal
verificare_cal DD 0			;variabila care ne va ajuta sa vedem daca piesa ii cal sau nu
X_matrice DD 0
Y_matrice DD 0
constanta_zero EQU 0
poz dd 0
culoare dd 0

;bibliotecile de mai jos sunt cele ce contin litere si cifre
include digits.inc		
include letters.inc

.code
;procedura ce urmeaza, make_text, afiseaza o litera sau o cifra la coordonatele date
;arg1 va reprezenta simbolul ce va trebui afisat
;arg2 va reprezenta un pointer la matricea de pixeli
;arg3 - pos_x
;arg4 - pos_y

make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 26 sunt litere, 27 e space
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0ff1414h ;culoare scris
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 0FFFFFFh  ;fundal casute text
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

;;Un macro care ne va ajuta la scrierea titlului/nume/play again
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm


horse proc  
	;procedura pentru desenarea calului
	push ebp
	mov ebp,esp
	pusha
cal:
	;desenam pe L
	mov eax, [ebp + arg1]  ;citim simbolul de afisat
	sub eax, 'L'
	mov culoare_simbol, 04D79FFh ;selectam culoarea pentru simbolul L
	lea esi, letters
	jmp deseneaza_cal
deseneaza_cal:
	mov ebx, cal_height
	mul ebx
	mov ebx, cal_height
	mul ebx
	add esi, eax
	mov ecx, cal_height
bucla_simbol_linii_cal:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, cal_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, cal_height
bucla_simbol_coloane_cal:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb_cal
	mov edx, culoare_simbol
	mov dword ptr [edi], edx
	jmp simbol_pixel_next_cal
simbol_pixel_alb_cal:
	mov dword ptr [edi], 0FFFF5Fh ;fundal patratel cu simbol 
simbol_pixel_next_cal:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane_cal
	pop ecx
	loop bucla_simbol_linii_cal
	popa
	mov esp, ebp
	pop ebp
	ret
horse endp

;Urmatorul macro ne va ajuta sa desenam simbolul de L ce va semnifica calul
make_text_macro_cal macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call horse
	add esp, 16
endm

;Urmatoroarele doua macro-uri ne vor ajuta sa desenam tabla de sah
deseneaza_linie_verticala macro drawArea, x, y, len, culoarea
LOCAL vf, final
	push eax
	push ecx
	push ebx
	
	mov eax, 0
	mov eax, x
	mov ebx, 640
	mul ebx
	add eax, y
	shl eax, 2
	mov ecx, 0
vf:
	add eax, 2559
	mov EBX, drawArea
	add ebx, ecx
	mov dword ptr [ebx+eax], culoarea
	inc ecx
	inc ecx
	cmp ecx, len
	je final
	loop vf
final:
	pop ebx
	pop ecx
	pop eax
endm

deseneaza_linie_orizontala macro drawArea, x, y,len,culoarea
local vf, final
	pusha
	mov EAX, 0
	mov EAX, x
	mov EBX, area_width
	mul EBX
	add EAX, y
	shl EAX, 2
	mov ECX, 0
vf:
	mov EBX, drawArea
	add EBX, ECX
	mov dword ptr [EBX+EAX], culoarea
	inc ECX
	inc ECX
	cmp ECX, len
	je final
	loop vf
final:
	popa
endm
	
draw_area_macro macro drawArea, x, y, color
LOCAL repeta
	pusha
	mov eax,y
	add eax,50
repeta:
	deseneaza_linie_verticala drawArea,x,y,50,color
	inc y
	cmp y,eax
	jle repeta
	popa
endm

;;Urmatorul macro ne ajuta la validarea mutarii calului. Primeste ca si parametrii pozitiile actuale si viitoare ale calului.
;;Verifica daca se poate muta calul prin simple diferente.	
miscare_valida macro xActual, yActual, xPoz, yPoz ;xActual,yActual= pozitia calului, xPoz,yPoz= pozitia unde vrei sa mergi
	pusha
	mov eax,xActual
	sub eax,xPoz
	mov ebx,yActual
	sub ebx,yPoz
	
	cmp eax,4
	jne salt1
	cmp ebx,8
	je posibil
	salt1:
	
	cmp eax,8
	jne salt2
	cmp ebx,4
	je posibil
	salt2:
	
	cmp eax,-4
	jne salt3
	cmp ebx,8
	je posibil
	salt3:
	
	cmp eax,-8
	jne salt4
	cmp ebx,4
	je posibil
	salt4:
	
	cmp eax,-4
	jne salt5
	cmp ebx,-8
	je posibil
	salt5:
	
	cmp eax,-8
	jne salt6
	cmp ebx,-4
	je posibil
	salt6:
	
	cmp eax,4
	jne salt7
	cmp ebx,-8
	je posibil
	salt7:
	
	cmp eax,8
	jne salt8
	cmp ebx,-4
	je posibil
	salt8:
	popa
;imposibil
	mov eax,1
	jmp final
posibil:
	popa
	mov eax,0
final:
endm 
	
in_tabla proc   ;se verifica daca s-a dat click in tabla, returneaza 'boolean' (returneaza_fals daca nu s-a dat click in tabla)
	;in EBX se afla X, in EDX se afla Y
	mov esi, edx
	cmp ebx, X0
	jl returneaza_fals
	mov eax, 8
	mov ecx, patrat_width
	mul ecx
	add eax, X0
	cmp ebx, eax
	jge returneaza_fals
	mov edx, esi
	cmp edx, Y0
	jl returneaza_fals
	mov eax, 8
	mov ecx, patrat_width
	mul ecx
	add eax, Y0
	mov edx, esi
	cmp edx, eax
	jge returneaza_fals
	mov eax, 1
	mov ver_afara, eax        ; ne ajuta la pastrarea numaratorului(ajuta la pastrarea nr de click-uri, in total 64 posibile pe tabla) daca s-a click afara din casuta
	jmp afara
returneaza_fals:
	mov eax, 0
	mov ver_afara, eax
afara:
	ret
in_tabla endp

transpunere_in_matrice proc
	push ebp
	mov ebp, esp
	push ebx
	push ecx
	mov eax, [ebp+arg1]
	mov ebx, 4
	mul ebx
	mov esi, eax			; X in coloana
	mov eax, [ebp+arg2]; !!! linia si coloana se inverseaza mai jos, daca X=1, Y=0 atunci vom pune in 0,1
	mul ebx
	mov ebx, eax
	
	mov xVef,esi
	mov yVef,ebx
	miscare_valida  xpostMat, ypostMat,xVef,yVef 
	cmp eax,0
	jne iesire_transpunere_in_matrice
	mov eax,xVef
	mov xpostMat,eax
	mov eax,yVef
	mov ypostMat,eax
	
	mov eax, 8		; Y in linie
	mul ebx
	
	;Urmatoarele 4 linii de cod sunt pt a ne asigura ca patratul pe care urmeaza sa fie calul ii ocupat sau nu. 
	;Daca ii ocupat nu mai pot continua daca nu se continua pana la finalul jocului
	;Daca se vrea a se reveni la pozitia anterioara in tabla se vor sterge aceste 4 linii
	mov ebx, eax
	mov eax, matrice[ebx+esi]	;matrice[pozitie]; pozitie = ebx * 8 + esi
	cmp eax, 0 ;VERIFICA DACA E SAU NU OCUPATA POZITIA
	jne iesire_transpunere_in_matrice
	mov ecx, [ebp+arg3] ;ia simbolul de afisat de pe stiva
	cmp ecx, 0 ;verifica ce simbol are de afisat
este_cal:
	mov eax, 1
	mov matrice[ebx+esi], eax
	mov eax, 0
	jmp aici
iesire_transpunere_in_matrice:
	mov edx, 1				;evitam sa mai desenam o data simbolul peste altul daca e ocupata casuta
	sub edx, verificare_cal
	mov verificare_cal, edx
aici:
	pop ecx
	pop ebx
	mov esp, ebp
	pop ebp
	ret
transpunere_in_matrice endp

joc proc
	push ecx
	mov ebx,[ebp+arg2]		;verificare daca click-ul s-a dat pe tabla de joc
	mov edx,[ebp+arg3]
	push edx				;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	push ebx				;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	call in_tabla  ;returneaza in verifica_afara 1 daca click-ul a fost pe tabla sau 0 daca a fost afara
	add esp, 8
	cmp eax, 0
	je final_player
	mov edx, 1				
	;verificam ce simbol urmeaza scris
	sub edx, verificare_cal
	mov verificare_cal, edx 
	;aflarea coordonatei X a casutei unde s-a dat click + 1
	xor edx, edx
	mov ecx, patrat_width
	mov eax, [ebp+arg2]		
	sub eax, X0
	div ecx
	mov X_matrice, eax
	mov ecx, patrat_width
	mul ecx
	mov ebx, eax
	add ebx, X0
	add ebx, 1
	;aflarea coordonatei Y a casutei unde s-a dat click + 1
	xor edx, edx
	mov eax, [ebp+arg3]
	sub eax, Y0
	div ecx
	mov Y_matrice, eax
	mov ecx, patrat_width
	mul ecx
	add eax, Y0
	add eax, 1
	;facem tranzitia la modelul matematic al tabelei, in matricea "matrice"
	mov ecx, eax			;plus verificare accesare casuta ocupata
	push verificare_cal		
	push Y_matrice			
	push X_matrice			
	call transpunere_in_matrice
	add esp, 12
	cmp eax, 0				; rezultatul proc transpunere_in_matrice, 0 - casuta libera, 1 - ocupat cu L
	jne final_player
	cmp verificare_cal, 1 ;1-deseneaza L
	
	sub ypost,50
	draw_area_macro area,  xpost, ypost,  0ff1414h
	mov xpost,ecx
	mov ypost,ebx
	draw_area_macro area, xpost, ypost,   0ff1414h
	
	add ebx,20
	add ecx,15
	make_text_macro 'L',area , ebx, ecx
	jmp final_player
final_player:
	pop ecx
	ret
joc endp

play_again proc 
	pusha
	cmp ebx, 480 ;verifica daca s-a dat click pe casuta "play again"
	jl afara
	cmp ebx, 580
	jg afara
	cmp edx, 215
	jl afara
	cmp edx, 230
	jg afara
parcurgem_matrice:
	mov ecx, 64
	mov edi, 0
bucla:
		mov esi, ecx
		dec esi
		mov matrice[esi * 4], edi ;reseteaza campurile din matrice, care e de fapt un vector
loop bucla
	mov ebx, 0
	mov verificare_cal, ebx ;
	mov ebx, 64
	mov numarator, ebx ;reseteaza numaratorul de miscari valide ramase la 64 
	mov xpost , 50
	mov	ypost , 0
	popa
	ret
afara:
	popa
	mov eax, 999
	ret
play_again endp

draw proc
	push EBP
	mov EBP, ESP
	pusha
	mov EAX, [EBP+arg1]
	cmp EAX, 1
	jz evt_click
	cmp EAX, 2
	jz evt_timer ; nu s-a efectuat click pe nimic
	
clear:
	mov EAX, area_width
	mov EBX, area_height
	mul EBX
	shl EAX, 2
	push EAX
	push 0FFFFFFh   ;culoare fundal
	push area
	call memset
	add ESP, 12
	deseneaza_linie_orizontala area,50,0 ,1600,0h
	deseneaza_linie_orizontala area,100,0,1600 ,0h
	deseneaza_linie_orizontala area,150,0,1600 ,0h
	deseneaza_linie_orizontala area,200,0,1600 ,0h
	deseneaza_linie_orizontala area,250,0,1600 ,0h
	deseneaza_linie_orizontala area,300,0,1600 ,0h
	deseneaza_linie_orizontala area,350,0,1600 ,0h
	deseneaza_linie_orizontala area,400,0,1600 ,0h
	deseneaza_linie_orizontala area,450,0,1600 ,0h
	deseneaza_linie_verticala area,50,0,400,0h

;;Urmatoarele seturi de instructiuni cu etichete ET1,ET2... s-au folosit pt a umple patratelele din tabla
;;cu culoarea negru
	mov x,100    
	mov y,0
ET1:
	deseneaza_linie_verticala area,x,y,50,0h
	inc y
	cmp y,50
	jle ET1
	mov y,0
	add x,100
	cmp x,400
	jle ET1
	mov x,50
	mov y,50
ET2:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,100
	jle ET2
	mov y,50
	add x,100
	cmp x,400
	jle ET2
	mov x,100
	mov y,100
ET3:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,150
	jle ET3
	mov y,100
	add x,100
	cmp x,400
	jle ET3
	mov x,50
	mov y,150
ET4:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,200
	jle ET4
	mov y,150
	add x,100
	cmp x,400
	jle ET4
	mov x,100
	mov y,200
ET5:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,250
	jle ET5
	mov y,200
	add x,100
	cmp x,400
	jle ET5
	mov x,50
	mov y,250
ET6:
	deseneaza_linie_verticala area, x,y,50 ,0h
	inc y
	cmp y,300
	jle ET6
	mov y,250
	add x,100
	cmp x,400
	jle ET6
	mov x,100
	mov y,300
ET7:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,350
	jle ET7
	mov y,300
	add x,100
	cmp x,400
	jle ET7
	mov x,50
	mov y,350
ET8:
	deseneaza_linie_verticala area,x,y,50 ,0h
	inc y
	cmp y,400
	jle ET8
	mov y,350
	add x,100
	cmp x,400
	jle ET8
	deseneaza_linie_verticala area,50,400,400 ,0h
	make_text_macro 'K', area,190, 0
	make_text_macro 'N', area,200, 0
	make_text_macro 'I', area,210, 0
	make_text_macro 'G', area,220, 0
	make_text_macro 'H', area,230, 0
	make_text_macro 'T', area,240, 0
	make_text_macro 'S', area,250, 0
	make_text_macro 'T', area,280, 0
	make_text_macro 'O', area,290, 0
	make_text_macro 'U', area,300, 0
	make_text_macro 'R', area,310, 0
	make_text_macro 'P', area,480,210
	make_text_macro 'L', area,490,210
	make_text_macro 'A', area,500,210
	make_text_macro 'Y', area,510,210
	make_text_macro 'A', area,530,210
	make_text_macro 'G', area,540,210
	make_text_macro 'A', area,550,210
	make_text_macro 'I', area,560,210
	make_text_macro 'N', area,570,210
	make_text_macro 'M', area,440,450
	make_text_macro 'U', area,450,450
	make_text_macro 'R', area,460,450
	make_text_macro 'E', area,470,450
	make_text_macro 'A', area,480,450
	make_text_macro 'M', area,510,450
	make_text_macro 'A', area,520,450
	make_text_macro 'R', area,530,450
	make_text_macro 'I', area,540,450
	make_text_macro 'U', area,550,450
	make_text_macro 'S', area,560,450
		
	draw_area_macro area, xpost, ypost, 0ff1414h
	make_text_macro 'L', area,20, 60
		
evt_click:
	mov ebx,[ebp+arg2]
	mov edx,[ebp+arg3]	
	push edx
	push ebx
	add esp, 8
	mov ecx, numarator ;numarator e 64 la inceput, pentru ca sunt 64 casute
	jecxz label_play_again ;daca ecx ii 0 se asteapta PLAY AGAIN, daca nu, se intra in bucla
	bucla:
		call joc ;procesul de joc
		cmp eax, 0
		jne pastram_numarator				;daca am apasat in casuta ocupata, numarator se pastreaza
		cmp ver_afara, constanta_zero	;daca click s-a efectuat afara din tabela, la fel pastram
		je pastram_numarator
		dec numarator
	pastram_numarator:
		mov ecx, numarator
	label_play_again:
		mov ebx,[ebp+arg2]	;unde dai click pe play again
		mov edx,[ebp+arg3]
		push edx
		push ebx
		call play_again ;se reseteaza tabla de joc
		add esp, 8
		cmp eax, 999
		je evt_timer
		call draw
evt_timer:
	inc counter

final_draw:
	popa
	mov ESP, EBP
	pop EBP
	ret

draw endp
	
start:
	;aici se scrie codul
	;terminarea programului
	mov EAX, area_width   		;in EAX punem latimea 
	mov EBX, area_height		;in EBX punem lungimea
	mul EBX
	shl EAX, 2
	push EAX
	call malloc
	add ESP, 4
	mov area, EAX
	
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add ESP, 20
	
	;;;terminarea programului
	push 0
	call exit
end start
