import pandas as pd
import numpy as np
import math as mth
import re
from numpy import loadtxt
from keras.models import Sequential
from keras.layers import Dense
import category_encoders as cat_en

df = pd.read_csv('proiectCovid.csv', sep='\t')

df.head()

##Stergeri de coloane
df.drop('DateOfAdmissionHospital',axis=1,inplace=True)
df.drop('LivesInWuhanComment',axis=1,inplace=True)
df.drop('TravelHistoryDates',axis=1,inplace=True)
df.drop('ReportedMarketExposure',axis=1,inplace=True)
df.drop('ReportedMarketExposureComment',axis=1,inplace=True)
df.drop('SequenceAvailable',axis=1,inplace=True)
df.drop('DateOfDischarge',axis=1,inplace=True)
df.drop('ChronicDiseases',axis=1,inplace=True)

##Replace-uri de date
sir=df['DateOfConfirmation'][0]
df['LivesInWuhan']=df['LivesInWuhan'].replace(sir,False)
df['DischargedQ']=df['DischargedQ'].replace(sir,False)
df['DeathQ']=df['DeathQ'].replace(sir,False)
df=df.replace(sir,np.nan)

##Pentru clean-ul coloanei Age
ageColumn=df['Age']
dimension=df.shape[0]
for i in range(dimension):
    x=ageColumn.at[i]
    media=0
    if isinstance(x, float):
        ageColumn.at[i]=0
    elif 'Interval' in x:
        listaNumere=[]
        temp1 = re.findall(r'\d+', x) 
        listaNumere = list(map(int, temp1))
        if len(listaNumere)==2:
            media=int((listaNumere[0]+listaNumere[1])/2)
            ageColumn.at[i]=media
        elif len(listaNumere)==1:
            media=int(listaNumere[0])
            ageColumn.at[i]=media   

df['Age']=ageColumn

##Pentru clean-ul coloanei Sex
sexColumn=df['Sex']

dimension=df.shape[0]
for i in range(dimension):
    x=sexColumn.at[i]
    if isinstance(x, float):
        continue
    if 'Entity' in x:
        start = 0
        stop = 17
        if len(x) > stop :
            x = x[0: start:] + x[stop + 1::]
        x=x.replace(']',' ')
        x=x.replace('"',' ')
    sexColumn.at[i]=x
df['Sex']=sexColumn

##Pentru clean-ul coloanei City
cityColumn=df['City']

dimension=df.shape[0]
for i in range(dimension):
    x=cityColumn.at[i]
    if isinstance(x, float):
        continue
    if 'Entity' in x:
        if 'City' in x:
            start = 0
            stop = 15
            if len(x) > stop :
                x = x[0: start:] + x[stop + 1::]
            x=x.replace(']',' ')
            x=x.replace('}',' ')
            cityColumn.at[i]=x
df['City']=cityColumn

##Pentru clean-ul coloanei AdministrativeDivision
administrativeDivision=df['AdministrativeDivision']

dimension=df.shape[0]
for i in range(dimension):
    x=administrativeDivision.at[i]
    if isinstance(x, float):
        continue
    if 'Entity' in x:
        if 'AdministrativeDivision' in x:
            start = 0
            stop = 33
            if len(x) > stop :
                x = x[0: start:] + x[stop + 1::]
            x=x.replace(']',' ')
            x=x.replace('}',' ')
            administrativeDivision.at[i]=x
df['AdministrativeDivision']=administrativeDivision


##Pentru clean-ul coloanei Country
countryColumn=df['Country']

dimension=df.shape[0]
for i in range(dimension):
    x=countryColumn.at[i]
    if isinstance(x, float):
        continue
    if 'Entity' in x:
        if 'Country' in x:
            start = 0
            stop = 17
            if len(x) > stop :
                x = x[0: start:] + x[stop + 1::]
            x=x.replace(']',' ')
            countryColumn.at[i]=x
df['Country']=countryColumn


##Pentru clean-ul coloanei GeoPosition
geoPositionColumn=df['GeoPosition']

dimension=df.shape[0]
for i in range(dimension):
    x=geoPositionColumn.at[i]
    if isinstance(x, float):
        continue
    if 'GeoPosition' in x:
        start = 0
        stop = 12
        if len(x) > stop :
            x = x[0: start:] + x[stop + 1::]
        x=x.replace(']',' ')
        x=x.replace('}',' ')
        geoPositionColumn.at[i]=x
df['GeoPosition']=geoPositionColumn

df2=df

df2['sym'] = df2['Symptoms'].str.replace('"',' ').str.replace("{",' ').str.replace('}',' ')
marius = df2['sym'].str.get_dummies(sep = ' ')

marius.head()


marius.columns

df2.reset_index(drop = True, inplace = True)
marius.reset_index(drop = True, inplace = True)
final = pd.concat([df2,marius], axis = 1)

final.head()

final['DischargedQ']=final['DischargedQ'].str.replace("True",'1').str.replace("False",'0')
final['DeathQ']=final['DeathQ'].str.replace("True",'1').str.replace("False",'0')
final['LivesInWuhan']=final['LivesInWuhan'].str.replace("True",'1').str.replace("False",'0')
final['ChronicDiseaseQ']=final['ChronicDiseaseQ'].astype(int)

final['DischargedQ'] = final['DischargedQ'].fillna(0)
final['DeathQ'] = final['DeathQ'].fillna(0)
final['LivesInWuhan'] = final['LivesInWuhan'].fillna(0)

final.drop('sym',axis=1,inplace=True)
final.drop('Symptoms',axis=1,inplace=True)

final.to_csv('/home/mariusica/Desktop/SI/ProiectSI/Mariusica/final.csv',index = False)

Xcolumns = final[final.columns.difference(['DischargedQ','DateOfDischarge'])]
Y = final[['DeathQ']]
Xcolumns
Y
cols = Xcolumns.columns
num_cols = Xcolumns._get_numeric_data().columns
cat_cols = list(set(cols) - set(num_cols)) # gaseste coloanele ce trebuie convertite folosind OHE
   
        
onehot = cat_en.OneHotEncoder(cols= cat_cols, handle_unknown='ignore', use_cat_names=True)
df_oh = onehot.fit_transform(Xcolumns)  # datasetul final 


#######RETEAUA
model = Sequential()
model.add(Dense(12, input_dim=4234, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(3,activation='softmax'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(df_oh,Y,test_size=0.33,shuffle = False)

ctree=model.fit(X_train, y_train, epochs=35, batch_size=10)

_, accuracy = model.evaluate(X_train, y_train)
print('Accuracy: %.2f' % (accuracy*100))