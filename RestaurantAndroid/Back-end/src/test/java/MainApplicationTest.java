import entity.Client;
import facade_pattern.FacadeClient;
import facade_pattern.FacadeRestaurant;
import entity.Restaurant;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import repository.ClientRepository;
import repository.RestaurantRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainApplicationTest
{
    @Mock
    RestaurantRepository restaurantRepository;

    @Mock
    ClientRepository clientRepository;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    private FacadeRestaurant facadeRestaurant;

    @Rule
    public MockitoRule mockitoRule1 = MockitoJUnit.rule();
    private FacadeClient facadeClient;

    @Before
    public void init()
    {
        facadeRestaurant = new FacadeRestaurant(restaurantRepository);
    }

    @Before
    public void init2()
    {
        facadeClient= new FacadeClient(clientRepository);
    }


    @Test
    public void testRestaurant()
    {
        facadeRestaurant.getRestaurantId(5);
        verify(restaurantRepository).findById(5);
    }

    @Test
    public void testClient()
    {
        facadeClient.getClientById(3);
        verify(clientRepository).findById(3);
    }

    @Test
    public void testAssert()
    {
        Restaurant expectedRestaurant = new Restaurant(5,"Luca","StefanCelMare",6,3,"Sandu");

        when(restaurantRepository.findById(5)).thenReturn(java.util.Optional.of(expectedRestaurant));
        Optional<Restaurant> x = facadeRestaurant.getRestaurantId(5);

        assertEquals(x,expectedRestaurant);

    }

    @Test
    public void testAssert2()
    {

        Restaurant x = new Restaurant();
        Restaurant expectedRestaurant = new Restaurant(2,"Toskana","Minerva",5,5,"Sandu");
        when(restaurantRepository.findASpecificRestaurant("Toskana","Minerva")).thenReturn(expectedRestaurant);

        try
        {
            x = facadeRestaurant.getRestaurantByNameAndLocation("Toskana","Minerva");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        assertEquals(x, expectedRestaurant);

    }

    @Test
    public void testAssert3()
    {
        List<Restaurant> restaurant= new ArrayList<>();
        List<Restaurant> expectedRestaurant= new ArrayList<>();
        expectedRestaurant.add(new Restaurant(1,"Hanovra","Narcisa",3,4,"Sandu"));
        expectedRestaurant.add(new Restaurant(2,"Hanovra","Bicaz",3,8,"Sandu"));

        when(restaurantRepository.findRestaurantByName("Hanovra")).thenReturn(expectedRestaurant);

        try
        {
            restaurant= facadeRestaurant.getByName("Hanovra");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        assertEquals(restaurant,expectedRestaurant);
    }

    @Test
    public void testAssert4()
    {
        List<Client> clients= new ArrayList<>();
        List<Client> expectedClients= new ArrayList<>();
        expectedClients.add(new Client(1,"Marius","rezervat",true,100,"marius1234"));

        when(clientRepository.findClientsByAchitat(true)).thenReturn(expectedClients);

        try
        {
            clients= facadeClient.getClients(true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        assertEquals(clients,expectedClients);
    }
}
