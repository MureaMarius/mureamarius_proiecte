package entity;

import factory_pattern.User;
import observer.Observer;

import javax.persistence.*;

@Entity
@Table(name = "clienti")
public class Client implements User
{
    @Id
    @Column(name = "idClient")
    private int idClient;

    @Column(name = "numeRestaurant")
    private String restaurant;

    @Column(name = "numeClient")
    private String numeClient;

    @Column(name = "aRezervat")
    private String aRezervat;

    @Column(name = "achitatMasa")
    private boolean achitatMasa;

    @Column(name = "sumaAchitata")
    private int sumaAchitata;

    @Column(name = "parolaClient")
    private String parolaClient;

    public Client()
    {

    }

    public Client(int idClient, String numeClient, String aRezervat, boolean achitatMasa, int sumaAchitata, String parola)
    {
        this.idClient=idClient;
        this.numeClient=numeClient;
        this.aRezervat=aRezervat;
        this.achitatMasa=achitatMasa;
        this.sumaAchitata=sumaAchitata;
        this.parolaClient=parola;
    }



    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getaRezervat() {
        return aRezervat;
    }

    public void setaRezervat(String aRezervat) {
        this.aRezervat = aRezervat;
    }

    public boolean getAchitatMasa() {
        return achitatMasa;
    }

    public void setAchitatMasa(boolean achitatMasa) {
        this.achitatMasa = achitatMasa;
    }

    public int getSumaAchitata() {
        return sumaAchitata;
    }

    public void setSumaAchitata(int sumaAchitata) {
        this.sumaAchitata = sumaAchitata;
    }

    public boolean isAchitatMasa() {
        return achitatMasa;
    }

    public String getParolaClient() {
        return parolaClient;
    }

    public void setParolaClient(String parolaClient) {
        this.parolaClient = parolaClient;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idClient=" + idClient +
                ", numeClient='" + numeClient + '\'' +
                ", aRezervat='" + aRezervat + '\'' +
                ", achitatMasa=" + achitatMasa +
                ", sumaAchitata=" + sumaAchitata +
                ", parolaClient='" + parolaClient + '\'' +
                '}';
    }
}
