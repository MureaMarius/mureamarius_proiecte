package entity;

import factory_pattern.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admins")
public class Admin implements User
{
    @Id
    @Column(name = "idAdmin")
    private int idAdmin;

    @Column(name = "numeAdmin")
    private String numeAdmin;

    @Column(name = "restauranteAdministrate")
    private int restauranteAdministrate;

    @Column(name = "parolaAdmin")
    private String parolaAdmin;

    public Admin()
    {

    }

    public Admin(int idAdmin, String numeAdmin, int restauranteAdministrate, String parolaAdmin)
    {
        this.idAdmin=idAdmin;
        this.numeAdmin=numeAdmin;
        this.restauranteAdministrate=restauranteAdministrate;
        this.parolaAdmin=parolaAdmin;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getNumeAdmin() {
        return numeAdmin;
    }

    public void setNumeAdmin(String numeAdmin) {
        this.numeAdmin = numeAdmin;
    }

    public int getRestauranteAdministrate() {
        return restauranteAdministrate;
    }

    public void setRestauranteAdministrate(int restauranteAdministrate) {
        this.restauranteAdministrate = restauranteAdministrate;
    }

    public String getParolaAdmin() {
        return parolaAdmin;
    }

    public void setParolaAdmin(String parolaAdmin) {
        this.parolaAdmin = parolaAdmin;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "idAdmin=" + idAdmin +
                ", numeAdmin='" + numeAdmin + '\'' +
                ", restauranteAdministrate=" + restauranteAdministrate +
                ", parolaAdmin=" + parolaAdmin +
                '}';
    }
}
