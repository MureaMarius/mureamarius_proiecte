package entity;

import observer.Observer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "restaurant")
public class Restaurant implements Observer
{
    @Id
    @Column(name = "idRestaurant")
    private int idRestaurant;

    @Column(name = "numeRestaurant")
    private String numeRestaurant;

    @Column(name = "locatieRestaurant")
    private String locatieRestaurant;

    @Column(name = "notaRestaurant")
    private int notaRestaurant;

    @Column(name = "meseLibere")
    private int meseLibere;

    @Column(name = "numeAdmin")
    private String numeAdmin;

    public Restaurant()
    {

    }

    public Restaurant(int idRestaurant, String numeRestaurant, String locatieRestaurant, int notaRestaurant, int meseLibere, String numeAdmin)
    {
        this.idRestaurant=idRestaurant;
        this.numeRestaurant=numeRestaurant;
        this.locatieRestaurant=locatieRestaurant;
        this.notaRestaurant=notaRestaurant;
        this.meseLibere=meseLibere;
        this.numeAdmin=numeAdmin;
    }

    public int getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public String getNumeRestaurant() {
        return numeRestaurant;
    }

    public void setNumeRestaurant(String numeRestaurant) {
        this.numeRestaurant = numeRestaurant;
    }

    public String getLocatieRestaurant() {
        return locatieRestaurant;
    }

    public void setLocatieRestaurant(String locatieRestaurant) {
        this.locatieRestaurant = locatieRestaurant;
    }

    public int getNumarStele() {
        return notaRestaurant;
    }

    public void setNumarStele(int numarStele) {
        this.notaRestaurant = numarStele;
    }

    public int getMeseLibere() {
        return meseLibere;
    }

    public void setMeseLibere(int meseLibere) {
        this.meseLibere = meseLibere;
    }

    public int getNotaRestaurant() {
        return notaRestaurant;
    }

    public void setNotaRestaurant(int notaRestaurant) {
        this.notaRestaurant = notaRestaurant;
    }

    public String getNumeAdmin() {
        return numeAdmin;
    }

    public void setNumeAdmin(String numeAdmin) {
        this.numeAdmin = numeAdmin;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "idRestaurant=" + idRestaurant +
                ", numeRestaurant='" + numeRestaurant + '\'' +
                ", locatieRestaurant='" + locatieRestaurant + '\'' +
                ", notaRestaurant=" + notaRestaurant +
                ", meseLibere=" + meseLibere +
                ", numeAdmin='" + numeAdmin + '\'' +
                '}';
    }

    @Override
    public void update(int meseLibere)
    {
        this.setMeseLibere(meseLibere);
        System.out.println(this.getMeseLibere());
    }
}
