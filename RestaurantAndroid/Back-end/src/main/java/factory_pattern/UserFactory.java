package factory_pattern;

import entity.Admin;
import entity.Client;

public class UserFactory implements User
{
    public User getUser(String userType)
    {
        if(userType==null)
        {
            return null;
        }

        if(userType.equalsIgnoreCase("Admin"))
        {
            return new Admin();
        }

        if(userType.equalsIgnoreCase("Client"))
        {
            return new Client();
        }

        return null;
    }
}
