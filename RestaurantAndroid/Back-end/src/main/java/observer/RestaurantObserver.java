package observer;

import observer.Observer;

import java.util.ArrayList;

public class RestaurantObserver
{
    private int meseLibere;
    private ArrayList<Observer> observers= new ArrayList<>();

    public void addObserver(Observer o)
    {
        this.observers.add(o);
    }

    public void removeObsever(Observer o)
    {
        this.observers.remove(o);
    }

    public void setMeseLibere(int meseLibere)
    {
        this.meseLibere=meseLibere;
        for(Observer observer: observers)
        {
            observer.update(meseLibere);
        }
    }
}
