package controller;

import entity.Admin;
import entity.Client;
import entity.Restaurant;
import factory_pattern.User;
import factory_pattern.UserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import repository.ClientRepository;
import repository.RestaurantRepository;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@RestController
public class ClientController
{
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    private UserFactory userFactory;

    /**
     * metoda ne returneaza un client dupa id-ul dat ca si parametru
     * @param idClient reprezinta id-ul clientului
     * @return tipul returnat este un Client
     * @throws Exception pt cazul in care nu avem acel client in baza de date
     */
    @GetMapping("/clients/{idClient}")
    public Optional<Client> getClientById(@PathVariable int idClient) throws Exception
    {
        Optional<Client> client=clientRepository.findById(idClient);

        if(!client.isPresent())
        {
            throw new Exception("Clientul nu este prezent!");
        }

        return client;
    }

    /**
     * metoda ne creeaza un client cu datele respective date ca si parametru
     * @param idClient reprezinta id-ul clientului
     * @param numeClient reprezinta numele clientului
     * @param parolaClient reprezinta parola clientului
     * @param aRezervat reprezinta campul care ne specifica daca a rezervat sau nu o masa la un restaurant
     * @param achitatMasa reprezinta daca a achitat sau nu masa la restaurantul la care s-a dus
     * @param sumaAchitata reprezinta suma achitata
     * @return ne returneaza un raspuns prin care se creeaza clientul
     */
    @PostMapping("/createClient/{idClient}/{numeClient}/{parolaClient}/{aRezervat}/{achitatMasa}/{sumaAchitata}")
    public ResponseEntity<Object> createClient(@PathVariable int idClient, @PathVariable String numeClient,@PathVariable String parolaClient, @PathVariable String aRezervat, @PathVariable boolean achitatMasa, @PathVariable int sumaAchitata)
    {
        userFactory= new UserFactory();
        User user= userFactory.getUser("Client");

        if(user instanceof Client)
        {
            ((Client) user).setIdClient(idClient);
            ((Client) user).setNumeClient(numeClient);
            ((Client) user).setParolaClient(parolaClient);
            ((Client) user).setSumaAchitata(sumaAchitata);
            ((Client) user).setaRezervat(aRezervat);
            ((Client) user).setAchitatMasa(achitatMasa);

            Client userSave= clientRepository.save((Client) user);

            URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{idAdmin}").buildAndExpand(userSave.getIdClient()).toUri();

            return ResponseEntity.created(location).build();
        }

        return null;
    }

    /**
     * metoda ne returneaza toti clientii din baza de date care au achitat masa
     * @param achitatMasa reprezinta campul care ne spune daca s-a achitat masa sau nu
     * @return tipul returnat este o lista de clienti
     * @throws Exception pt cazul in care nu exista clienti cu masa achitata sau neachitata
     */
    @GetMapping("/getClientsByAchitat/{achitatMasa}")
    public List<Client> getClients(@PathVariable boolean achitatMasa) throws Exception
    {
        List<Client> clients= clientRepository.findClientsByAchitat(achitatMasa);

        if(clients.isEmpty())
        {
            if(achitatMasa)
                throw new Exception("Nu exista clienti cu masa achitata!");
            else
                throw new Exception("Nu exista clienti cu masa neachitata!");
        }

        return clients;
    }

    /**
     * metoda ne sterge toti clientii care au achitat masa
     * @throws Exception pt cazul in care nu avem clienti ce au achitat masa
     */
    @DeleteMapping("/deleteByAchitat")
    public void deleteByAchitat() throws Exception
    {
        List<Client> clients= clientRepository.findClientsByAchitat(true);

        if(clients.isEmpty())
        {
           throw new Exception("Nu exista clienti cu masa achitata!");
        }
        else
        {
            for(Client client: clients)
            {
                clientRepository.deleteById(client.getIdClient());
            }
        }
    }

    /**
     * metoda ne returneaza suma de bani achitata de toti clientii
     * @return ne returneaza o valoare de tipul int ce reprezinta suma de bani
     */
    @GetMapping("/getAmountOfMoney")
    public int getAmountOfMoney()
    {
        List<Client> clients=clientRepository.findClientsByAmountMoney();
        int sumaAchitata=0;

        for(Client client: clients)
        {
            sumaAchitata+=client.getSumaAchitata();
        }

        return sumaAchitata;
    }

    /**
     * metoda face ca sa se rezerve o masa la un restaurant daca acel restaurant exista
     * @param idClient  reprezinta id-ul clientului
     */
    @PostMapping("/goToRestaurant/{idClient}/{numeRestaurant}/{locatieRestaurant}")
    public void goToRestaurant(@PathVariable int idClient, @PathVariable String numeRestaurant, @PathVariable String locatieRestaurant)
    {
        Restaurant restaurant= restaurantRepository.findASpecificRestaurant(numeRestaurant,locatieRestaurant);
        Optional<Client> client= clientRepository.findById(idClient);

        if(restaurant==null)
        {
            System.out.println("Nu exista acest restaurant!");
            return;
        }
        else if(!client.isPresent())
        {
            System.out.println("Nu exista acest client!");
            return;
        }

        if(restaurant!=null && client.isPresent())
        {
            int meseLibere= restaurant.getMeseLibere();
            if(meseLibere>0)
            {
                client.get().setRestaurant(restaurant.getNumeRestaurant());
                clientRepository.save(client.get());

                meseLibere--;
                restaurant.setMeseLibere(meseLibere);
                restaurantRepository.save(restaurant);
            }
            else
            {
                System.out.println("Nu exista mese disponibile!");
            }
        }
    }

    @GetMapping("/getClientByNameAndPass/{numeClient}/{parolaClient}")
    public Client getClientByNameAndPass(@PathVariable String numeClient, @PathVariable String parolaClient) throws Exception
    {
        Client client=clientRepository.findClientByNameAndPass(numeClient, parolaClient);

        if(client==null)
        {
            throw new Exception("Clientul nu este prezent!");
        }

        return client;
    }

    @GetMapping("getAllClients")
    public List<Client> getAllClients()
    {
        List<Client> clients;
        clients=clientRepository.findAll();

        for(Client client: clients)
        {
            System.out.println(client.toString());
        }

        return clients;
    }

    @RequestMapping(method = RequestMethod.POST, value="addClient/{numeClient}/{parolaClient}")
    public void addClient(@PathVariable String numeClient, @PathVariable String parolaClient)
    {
        List<Client> clients;
        clients= clientRepository.findAll();

        int size= clients.size();
        Client client= new Client(size+1,numeClient,"nerezervat",false,0,parolaClient);

        clientRepository.save(client);
    }
}
