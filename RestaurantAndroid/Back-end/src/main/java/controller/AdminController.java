package controller;

import entity.Admin;
import entity.Client;
import entity.Restaurant;
import factory_pattern.User;
import factory_pattern.UserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import repository.AdminRepository;
import repository.RestaurantRepository;

import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * Clasa AdminController care va contine implementarile interogarile si fluxul de date de admin din baza de date spre program si invers
 */
@RestController
public class AdminController
{
    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    private UserFactory userFactory;

    /**
     * metoda cauta un admin dupa id-ul dat ca parametru
     * @param idAdmin reprezinta id-ul adminului
     * @return se returneaza o instanta de admin
     * @throws Exception se arunca o exceptie pt cazul in care nu exista admin in baza de date
     */
    @GetMapping("/admin/{id}")
    public Optional<Admin> getAdminById(@PathVariable int idAdmin) throws Exception
    {
        Optional<Admin> admin=adminRepository.findById(idAdmin);

        if(!admin.isPresent())
        {
            throw new Exception("Adminul nu este prezent!");
        }

        return admin;
    }

    /**
     * metoda creeaza un admin si il salveaza in baza de date in tabela aferenta adminului
     * @param idAdmin reprezinta id-ul adminului
     * @param numeAdmin reprezinta numele adminului
     * @param parolaAdmin reprezinta parola adminului
     * @param restauranteAdministrate reprezinta cate restaurante administreaza adminului
     * @return
     */
    @PostMapping("/createAdmin/{idAdmin}/{numeAdmin}/{parolaAdmin}/{restauranteAdministrate}")
    public ResponseEntity<Object> createAdmin(@PathVariable int idAdmin, @PathVariable String numeAdmin,@PathVariable String parolaAdmin, @PathVariable int restauranteAdministrate)
    {
        userFactory= new UserFactory();
        User user= userFactory.getUser("Admin");

        if(user instanceof Admin)
        {
            ((Admin) user).setIdAdmin(idAdmin);
            ((Admin) user).setNumeAdmin(numeAdmin);
            ((Admin) user).setParolaAdmin(parolaAdmin);
            ((Admin) user).setRestauranteAdministrate(restauranteAdministrate);

            Admin userSave= adminRepository.save((Admin)user);

            URI location= ServletUriComponentsBuilder.fromCurrentRequest().path("/{idAdmin}").buildAndExpand(userSave.getIdAdmin()).toUri();

            return ResponseEntity.created(location).build();
        }

        return null;
    }

    /**
     * metoda modifica numarul de restaurante administrate de un admin dupa numarul de restaurante existentecare au ca nume admin numele adminului din db
     * @param idAdmin reprezinta id-ul adminului
     */
    @PostMapping("/modifyNumberRestaurants/{idAdmin}")
    public void modifyRestauranteAdministrate(@PathVariable int idAdmin)
    {
        int numberOfRestaurants= (int)restaurantRepository.count();

        Optional<Admin> admin= adminRepository.findById(idAdmin);
        admin.get().setRestauranteAdministrate(numberOfRestaurants);

        adminRepository.save(admin.get());
    }

    @GetMapping("/getAdminByNameAndPass/{numeAdmin}/{parolaAdmin}")
    public Admin getAdminByNameAndPass(@PathVariable String numeAdmin, @PathVariable String parolaAdmin) throws Exception
    {
        Admin admin=adminRepository.findAdminByNameAndPass(numeAdmin, parolaAdmin);

        if(admin==null)
        {
            throw new Exception("Admin-ul nu este prezent!");
        }

        return admin;
    }

    @GetMapping("getAllAdmins")
    public List<Admin> getAllAdmins()
    {
        List<Admin> admins;
        admins=adminRepository.findAll();

        for(Admin admin: admins)
        {
            System.out.println(admin.toString());
        }

        return admins;
    }

    @RequestMapping(method = RequestMethod.POST, value="addAdmin/{numeAdmin}/{parolaAdmin}")
    public void addAdmin(@PathVariable String numeAdmin, @PathVariable String parolaAdmin)
    {
        List<Admin> admins;
        admins= adminRepository.findAll();

        int size= admins.size();
        Admin admin= new Admin(size+1,numeAdmin,0,parolaAdmin);

        adminRepository.save(admin);
    }
}
