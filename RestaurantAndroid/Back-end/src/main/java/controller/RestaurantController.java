package controller;


import entity.Restaurant;
import observer.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import repository.ClientRepository;
import repository.RestaurantRepository;

import javax.persistence.criteria.CriteriaBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class RestaurantController
{
    private ArrayList<Observer> observers;
    
    public RestaurantController(ArrayList<Observer> observers)
    {
        this.observers=observers;
    }

    @Autowired
    private RestaurantRepository restaurantRepository;


    /**
     * metoda cauta un restaurant dupa id
     * @param id id-ul unui restaurant
     * @return metoda returneaza un restaurant ca si entitate
     * @throws Exception exceptie pt cazul in care restaurantul cu id-ul id nu este prezent in baza de date
     */
    @GetMapping("/restaurant/{id}")
    public Optional<Restaurant> getRestaurantById(@PathVariable String id) throws Exception
    {
        int idInt = Integer.parseInt(id);
        Optional<Restaurant> restaurant = restaurantRepository.findById(idInt);

        if(!restaurant.isPresent())
            throw new Exception("This restaurant isn't present ! id : " + id);

        return restaurant;
    }

    /**
     * metoda creeaza un restaurant cu datele trimise ca si parametru
     * @param idRestaurant reprezinta id-ul restaurantului creat
     * @param numeRestaurant reprezinta numele restaurantului creat
     * @param locatieRestaurant reprezinta locatia restaurantului creat
     * @param numarStele reprezinta numarul de stele apartinute de restaurantul creat
     * @param meseLibere reprezinta numarul de mese libere ale restaurantului creat
     * @param numeAdmin reprezinta numele adminului restaurantului creat
     * @return ne returneaza un raspuns prin care actualizam tabela din baza de date
     */
    @PostMapping("/createRestaurant/{idRestaurant}/{numeRestaurant}/{locatieRestaurant}/{numarStele}/{meseLibere}/{numeAdmin}")
    public ResponseEntity<Object> createRestaurant(@PathVariable int idRestaurant, @PathVariable String numeRestaurant, @PathVariable String locatieRestaurant, @PathVariable int numarStele, @PathVariable int meseLibere, @PathVariable String numeAdmin)
    {
        Restaurant restaurant = new Restaurant(idRestaurant, numeRestaurant, locatieRestaurant, numarStele, meseLibere,numeAdmin);
        Restaurant saveRestaurant = restaurantRepository.save(restaurant);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idRestaurant}").buildAndExpand(saveRestaurant.getIdRestaurant()).toUri();

        return ResponseEntity.created(location).build();
    }

    /**
     * metoda ne returneaza toate restaurantele din baza de date
     * @return tipul returnat este o lista de restaurante
     */
    @GetMapping("getAllRestaurants")
    public List<Restaurant> getAllRestaurants()
    {
        List<Restaurant> restaurants;
        restaurants=restaurantRepository.findAll();

        for(Restaurant restaurant: restaurants)
        {
            System.out.println(restaurant.toString());
        }

        return restaurants;
    }

    /**
     * metoda ne returneaza toate restaurante dupa disponibilitate, adica unde campul mese libere este mai mare ca 0
     * @return tipul returnat este o lista de restaurante
     * @throws Exception arunca o exceptie pt cazul in care nu se gasesc restaurante disponibile
     */
    @GetMapping("getAllRestaurantsByDisponibility")
    public List<Restaurant> getAllRestaurantsByDisponibility() throws Exception
    {
        List<Restaurant> restaurants;
        restaurants=restaurantRepository.findRestaurantByDisponibility();

        if(restaurants.isEmpty())
        {
            throw new Exception("Nu exista restaurante disponibile!");
        }

        return restaurants;
    }

    /**
     * metoda ne returneaza toate restaurantele dupa o anumita nota data ca parametru
     * @param nota reprezinta nota restaurantelor
     * @return tipul returnat este o lista de restaurante
     * @throws Exception ne arunca o exceptie daca nu s-au gasit restaurante
     */
    @GetMapping("getRestaurantsByStar/{nota}")
    public List<Restaurant> getAllRestaurantsByNote(@PathVariable int nota) throws Exception
    {

        List<Restaurant> restaurants;
        restaurants=restaurantRepository.findRestaurantsByStars(nota);

        if(restaurants.isEmpty())
        {
            throw new Exception("Nu exista restaurante cu aceasta nota!");
        }

        return restaurants;
    }

    /**
     * metoda ne cauta un restaurant dupa o anumita locatie si un nume
     * @param nume reprezinta numele restaurantului cautat
     * @param locatie reprezinta locatia restaurantului cautat
     * @return ne returneaza un restaurant
     * @throws Exception pt cazul in care nu s-a gasit nici un restaurant
     */
    @GetMapping("findARestaurant/{nume}/{locatie}")
    public Restaurant getRestaurant(@PathVariable String nume, @PathVariable String locatie) throws Exception
    {
        Restaurant restaurant=restaurantRepository.findASpecificRestaurant(nume,locatie);

        if(restaurant==null)
        {
            throw new Exception("Nu exista acest restaurant!");
        }
        else
        {
            return restaurant;
        }
    }

    /**
     * metoda ne sterge un restaurant dupa un id dat ca parametru
     * @param idRestaurant reprezinta id-ul restaurantului care se vrea a fi sters
     * @throws Exception pt cazul in care nu s-a gasit acel restaurant care se vrea sters
     */
    @DeleteMapping("deleteRestaurantById/{idRestaurant}")
    public void deleteRestaurantById(@PathVariable int idRestaurant) throws Exception
    {
        Optional<Restaurant> restaurant= restaurantRepository.findById(idRestaurant);

        if(!restaurant.isPresent())
        {
            throw new Exception("Nu se poate sterge pt ca nu exista");
        }
        else
        {
            restaurantRepository.delete(restaurant.get());
        }
    }

    /**
     * metoda ne sterge toate restaurantele din baza de date
     * @throws Exception daca nu avem restaurante in baza de date
     */
    @DeleteMapping("deleteAllRestaurants")
    public void deleteAllRestaurants() throws Exception
    {
        List<Restaurant> restaurants=restaurantRepository.findAll();

        if(restaurants.isEmpty())
        {
            throw new Exception("Nu exista restaurante in baza de date!");
        }
        else
        {
            restaurantRepository.deleteAll();
        }
    }

    /**
     * metoda ne cauta restaurantele dupa un anumit nume
     * @param name reprezinta numele restaurantelor
     * @return se va returna o lista de restaurante
     * @throws Exception pt cazul in care nu avem restaurante in baza de date cu acel nume dat ca parametru
     */
    @GetMapping("findByName/{name}")
    public List<Restaurant> findByName(@PathVariable String name) throws Exception
    {
        List<Restaurant> restaurants;
        restaurants=restaurantRepository.findRestaurantByName(name);

        if(restaurants.isEmpty())
        {
            throw new Exception("Nu exista restaurante in baza de date!");
        }

        return restaurants;
    }

    /**
     * metoda modifica nota unui restaurant
     * @param numeRestaurant reprezinta numele restaurantului
     * @param locatieRestaurant reprezinta locatia restaurantului
     * @param nota reprezinta nota noua care se va da acelui restaurant
     * @throws Exception pt cazul in care nu am gasit acel restaurant in baza de date
     */
    @PostMapping("/modifyNota/{numeRestaurant}/{locatieRestaurant}/{nota}")
    public void modifyNoteRestaurant(@PathVariable String numeRestaurant, @PathVariable String locatieRestaurant, @PathVariable int nota) throws Exception
    {
        Restaurant restaurant=restaurantRepository.findASpecificRestaurant(numeRestaurant,locatieRestaurant);

        if(restaurant==null)
        {
            throw new Exception("Nu exista acest restaurant!");
        }

        restaurant.setNumarStele(nota);
        restaurantRepository.save(restaurant);
    }
}
