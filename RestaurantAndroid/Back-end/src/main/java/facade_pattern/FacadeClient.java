package facade_pattern;

import entity.Client;
import org.springframework.web.bind.annotation.PathVariable;
import repository.ClientRepository;

import java.util.List;
import java.util.Optional;

public class FacadeClient
{
    private ClientRepository clientRepository;

    public FacadeClient(ClientRepository clientRepository)
    {
        this.clientRepository= clientRepository;
    }

    public List<Client> getClients(@PathVariable boolean achitatMasa)
    {
        return clientRepository.findClientsByAchitat(achitatMasa);
    }

    public Optional<Client> getClientById(@PathVariable int idClient)
    {
        return clientRepository.findById(idClient);
    }
}
