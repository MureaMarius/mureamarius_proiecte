package facade_pattern;

import entity.Restaurant;
import org.springframework.web.bind.annotation.PathVariable;
import repository.RestaurantRepository;

import java.util.List;
import java.util.Optional;

public class FacadeRestaurant
{
    private RestaurantRepository restaurantRepository;

    public FacadeRestaurant(RestaurantRepository restaurantRepository)
    {
        this.restaurantRepository = restaurantRepository;
    }

    public Optional<Restaurant> getRestaurantId(int id)
    {
        return restaurantRepository.findById(id);

    }

    public Restaurant getRestaurantByNameAndLocation(@PathVariable String nameRestaurant, @PathVariable String locatie) throws Exception
    {
        return restaurantRepository.findASpecificRestaurant(nameRestaurant,locatie);

    }

    public List<Restaurant> getByName(@PathVariable String name)
    {
        return restaurantRepository.findRestaurantByName(name);
    }
}
