package testare;

import controller.RestaurantController;
import entity.Restaurant;
import org.mockito.Mock;
import repository.RestaurantRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import service.Service;

@SpringBootApplication
@ComponentScan(basePackageClasses = RestaurantController.class)
@EntityScan(basePackageClasses = Restaurant.class)
@EnableJpaRepositories(basePackageClasses = RestaurantRepository.class)

public class MainApplication extends SpringBootServletInitializer
{
    private static RestaurantRepository restaurantRepository;

    public static void main(String[] args)
    {
        SpringApplication.run(MainApplication.class, args);
    }

}
