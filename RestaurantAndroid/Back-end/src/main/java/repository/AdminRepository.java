package repository;

import entity.Admin;
import factory_pattern.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Integer>
{
    @Query("SELECT a FROM Admin a WHERE a.numeAdmin = :numeAdmin AND a.parolaAdmin = :parolaAdmin")
    Admin findAdminByNameAndPass(@Param("numeAdmin") String nume, @Param("parolaAdmin") String parola);
}
