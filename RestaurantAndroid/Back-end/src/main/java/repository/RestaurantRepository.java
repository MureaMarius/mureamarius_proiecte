package repository;

import entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RestaurantRepository extends JpaRepository<Restaurant, Integer>
{
    @Query("SELECT r FROM Restaurant r WHERE r.meseLibere > 0")
    List<Restaurant> findRestaurantByDisponibility();

    @Query("SELECT r FROM Restaurant r WHERE r.notaRestaurant = :nota")
    List<Restaurant> findRestaurantsByStars(@Param("nota") int nota);

    @Query("SELECT r FROM Restaurant r WHERE r.numeRestaurant = :nume AND r.locatieRestaurant = :locatie")
    Restaurant findASpecificRestaurant(@Param("nume") String nume, @Param("locatie") String locatie);

    @Query("SELECT r FROM Restaurant r WHERE r.numeRestaurant = :nume")
    List<Restaurant> findRestaurantByName(@Param("nume") String nume);
}
