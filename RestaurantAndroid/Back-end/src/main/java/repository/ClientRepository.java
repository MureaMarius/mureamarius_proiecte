package repository;

import entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Integer>
{
    @Query("SELECT c FROM Client c WHERE c.achitatMasa = :achitat")
    List<Client> findClientsByAchitat(@Param("achitat") boolean achitat);

    @Query("SELECT c FROM Client c WHERE c.sumaAchitata > 0")
    List<Client> findClientsByAmountMoney();

    @Query("SELECT c FROM Client c WHERE c.numeClient = :nume AND c.parolaClient = :parola")
    Client findClientByNameAndPass(@Param("nume") String nume, @Param("parola") String parola);
}
