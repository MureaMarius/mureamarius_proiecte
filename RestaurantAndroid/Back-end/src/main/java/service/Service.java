package service;

import controller.AdminController;
import entity.Restaurant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import repository.AdminRepository;
import repository.RestaurantRepository;

public class Service
{
    RestaurantRepository restaurantRepository;

    public Service(RestaurantRepository restaurantRepository)
    {
        this.restaurantRepository=restaurantRepository;
    }

    public void findRestaurant(@PathVariable String numeRestaurant, @PathVariable String locatieRestaurant)
    {
        Restaurant restaurant= restaurantRepository.findASpecificRestaurant(numeRestaurant,locatieRestaurant);
        System.out.println(restaurant.toString());
    }
}
