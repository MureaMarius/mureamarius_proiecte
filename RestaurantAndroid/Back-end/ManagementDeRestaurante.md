# Management de Restaurante

# Descrierea Temei

Aplicatia este conceputa pentru a administra restaurante. Va avea in spate o baza de date cu 3 tabele, cate una pt fiecare entitate si pt restaurante. Ea va cuprinde doi useri:

1) Administratorul
2) Clientul

Ideea este de a putea folosi aplicatia ca si User (si aici luam userii de mai sus, Client si Administrator) si in functie de User-ul ce foloseste aplicatia sa se poate testa anumite functionalitati disponibile. Daca suntem Administrator sa putem adauga restaurante, sa le stergem din baza de date, sa observam clientii etc, iar ca si Client putem rezerva o masa la un restaurant, sa vedem toate restaurantele disponibile, sa achitam masa la restaurantul la care s-a rezervat masa, etc.

# Structura aplicatiei

Pentru partea de cod in limbajul Java s-a folosit Intellij Idea, pentru baza de date MySqlWorkbench iar Advanced Rest Client s-a folosit cu scopul de a face comunicare client-server.
Proiectul este structurat pe 8 pachete, fiecare pachet continand clase ce au o anumita functionalitate. Proiectul este de tip Maven, iar pentru a putea face legatura cu baza de date s-au creat doua fisiere: un fisier pom.xml ce va contine dependinte si un fisier application.properties ce va contine proprietatile serverului de baza de date.
Pachetul controller va contine clasele de tip controller pt fiecare entitate in parte (AdminController, ClientController, RestaurantController). In entity avem clasele ce vor reprezenta entitatile pe care se va lucra in proiect, aici putem aminti: Admin, Client si Restaurant. S-a ales sa se faca pachete speciale pt partea de observer, facade si factory, acestea reprezentand design pattern-urile cerute de enuntul proiectului. In pachetul de repository avem cate o clasa de repository pentru fiecare entitate in parte.

# Solutia aplicatiei

Solutia oferita pentru realizarea aplicatiei este de a avea o baza de date prin care sa stocam administratorii, clientii si restaurantele. In partea de back-end sa se poata manipula datele din aceasta baza de date, iar in partea de front-end sa putem executa aceste interogari pe tabele. Pentru partea de front-end se va folosi Android, iar ca si back-end Java cu Sql, folosindu-se framework-ul Spring.

# Implementarea aplicatiei

In aceasta sectiune se va descrie partea de implementare a aplicatiei. Se va lua pentru inceput pachetul controller unde se gasesc clasele Controller aferente entitatilor.

#### AdminController, ClientController, RestaurantController
In aceste clase se vor face implementarile metodelor din interfetele din pachetul de repository si implicit se vor manipula datele din tabelele din baza de date. Avem metode prin care se creeaza instante de entitati(Admin, Client si Restaurant) si se vor pune in tabelele aferente. Alte metode de mentionat ar fi cele prin care se fac interogari de cautare dupa id, dupa nume, de stergere etc. Verificarile acestor metode si testele se vor face prin rute specifice date server-ului prin aplicatia Advanced Rest Client. Legaturile dintre aceasta aplicatie si codul Java se vor face prin argumentele date adnotarilor aflate in clasele de tip Controller.

In pachetul entity se regasesc clasele ce reprezinta entitatile aplicatiei, ele existand si in baza de date sub forma de tabele.

#### Admin, Client, Restaurant
Aceste clase nu au altceva decat variabile instanta, constructori, getere si setere si o metoda toString. S-a specificat sub forma de adnotare (@Table) ca aceste clase reprezinta tabele in baza de date iar prin adnotarile @Column am atribuit variabilelor instanta din ele coloane aferente lor. Ca si constructori avem cate unul cu parametri si fara parametri.

Pachetele facade_pattern si factory_pattern, implicit clasele din interiorul lor s-au folosit pt partea de design pattern (Facade si Factory). Prin Facade intelegem partea de teste, de JUnit tests unde ne vom folosi de clasele FacadeClient si FacadeRestaurant. La design pattern-ul Factory am creat doua clase, User si User Factory. User este o interfata ce va fi implementata de UserFactory si de entitatile Admin si Client. Ideea aici este ca atunci cand se creeaza instante de User pentru a se introduce in tabelele Admin si Client sa se creeze instante de interfata User, aplicatia dandu-si seama ce fel de user va reprezenta acea instanta prin metoda getUser() din clasa UserFactory.

Pachetul observer contine o interfata Observer si o clasa RestaurantObserver iar el va reprezenta partea de design pattern Observer.

In pachetul de repository avem urmatoarele interfete:

#### AdminRepository, ClientRepository, RestaurantRepositoy
Aceste interfete vor contine metodele ce vor reprezenta interogarile pe baza de date. Interfetele extinzand clasa JpaRepository asta inseamna ca exista by default cateva metode deja implementate. Pentru interogari in plus se vor folosi query-uri.

####Interfata grafica
Pentru UI am ales sa fac in Android folosind tool-ul Android Studio. Nu este ceva complex, folosindu-se butoane personalizate, text view-uri si edit texte. 
Pentru inceput s-a creat un pachet service ce va contine 3 clase (2 clase si o interfata). Clasele ApiUtils si RetrofitClient fac legatura cu localhost-ul prin URL-ul "http://192.168.1.7:8080". S-a folosit ip-ul local al laptop-ului pentru a functiona corect aplicatia, altfel aparea o eroare cu mesaj de conexiune incorecta. Interfata WebApiService va contine toate rutele care se vor apela cu ajutorul interfetei. Aici s-au pus rutele catre apeluri de Get, Delete si Post. Este evidentiat fiecare apel de ruta prin termenul de Call al fiecarei metode. Toate aceste apeluri se vor face atunci cand utilizatorul apasa pe diferite butoane aferente fiecarei functionalitati ale proiectului. 

In pachetul model s-au descris toate clasele ce vor reprezenta entitati in proiect. Clasa Admin este pentru entitatea admin, clasa Client pentru entitatea client iar clasa Restaurant pentru restaurant. Pentru ca datele de pe localhost manipulate de interfata Android sunt in format json variabilele si campurile din entitati o sa trebuiasca sa fie serializate, de asta punandu-se adnotarea de serialized. 

Cum se lucreaza cu interfata Android panel-urile vor fi defapt Activities. Pentru asta s-a facut un pachet activities unde s-au implementat toate activity-urile principale ale aplicatiei. Dintr-un Activity principal in care s-a pus o poza si un buton de Start se va porni aplicatia prin apasarea acelui buton. Se va deschide un nou activity pentru partea de log-in. Aici se vor introduce date corespunzatoare clientului sau adminului, adica un username si o parola. Daca nu exista aceste date in baza de date se va putea crea un cont nou cu acele date si pentru acea entitate. In schimb, daca exista cont se va apasa log-in si se va deschide partea de Activity corespunzatoare entitatii de pe care s-a dat log-in.

Daca s-a facut log-in ca si Admin se va deschide un Activity cu toate functionalitatile corespunzatoare admin-ului. Vor aparea 3 butoane in partea centrala ce vor reprezenta optiunile principale (delete, show si get). In coltul din stanga jos s-au afisat datele corespunzatoare admin-ului ce a dat log-in in aplicatie. Daca se apasa pe butonul show vor aparea cateva optiuni si anume, afisare tuturor restaurantelor, afisarea tuturor administratorilor sau afisarea clientilor in functie daca au achitat sau nu masa la restaurantul ales. Daca s-a apasat butonul de delete vor aparea optiuni aferente tuturor operatiilor de stergere din aplicatie (stergerea restaurantelor ca de exemplu). Daca s-a apasat butonul de get putem vedea suma totala de bani de la toti clientii ce au achitat masa de la restaurant.

Daca s-a facut log-in ca si Client se va deschide un Activity cu toate functionalitatile corespunzatoare clientului. Vor aparea 5 butoane in partea centrala ce vor reprezenta optiunile principale (afisarea restaurantelor dupa disponibilitate, dupa nota data de alti clienti, putem cauta chiar un restaurant anume dupa locatie si nume, sa afisam toate restaurantele). In coltul din stanga jos s-au afisat datele corespunzatoare clientului ce a dat log-in in aplicatie. Daca se apasa pe butonul de rezerva masa clientul curent va putea rezerva o masa la un restaurant dupa numele si locatia restaurantului si dupa id-ul clientului.

####Concluzie

Prin aceasta aplicatie s-a incercat a se ajuta acei oameni ce detin restaurante sau chiar pe clientii care vor sa rezerve o masa la un anumit restaurant. Nu este o aplicatie complexa dar detine o baza de date in spate ce poate fi folosita pentru a depozita date despre administratori, clienti si chiar restaurante. Partea de back-end a fost realizata folosind limbajul Java cu framework-ul Spring iar partea de front-end cu ajutorul unei interfete Android. Dupa cum s-a specificat mai sus, aplicatia se foloseste de o baza de date.



