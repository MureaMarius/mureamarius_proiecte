package com.example.proiectps.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.proiectps.R;
import com.example.proiectps.controller.ClientController;
import com.example.proiectps.model.Client;

public class ClientActivity extends AppCompatActivity
{
    private Button btnAfisareRestaurante, btnRestaurantsByDisponibility, btnRestaurantsByStar, btnCautaRestaurant, btnGoToRestaurant, btnCauta, btnRezerva, btnInapoi;
    private Button btnCautaRestaurantSpecific;
    private TextView txtClient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client);

        Intent i= getIntent();
        Client client=(Client)i.getSerializableExtra("object");
        txtClient= findViewById(R.id.txtClient);
        txtClient.setText(client.afisare());

        btnAfisareRestaurante= findViewById(R.id.btnAfisareRestaurante);
        btnRestaurantsByDisponibility= findViewById(R.id.btnRestaurantsByDisponibility);
        btnRestaurantsByStar= findViewById(R.id.btnRestaurantsByNote);
        btnCautaRestaurant= findViewById(R.id.btnSearchASpecificRestaurant);
        btnGoToRestaurant= findViewById(R.id.btnGoToRestaurant);
        btnCauta= findViewById(R.id.btnCauta);
        btnRezerva=findViewById(R.id.btnRezerva);
        btnInapoi=findViewById(R.id.btnInapoi);
        btnCautaRestaurantSpecific= findViewById(R.id.btnCautaSpecificRestaurant);

        btnAfisareRestaurante.setOnClickListener(new ClientController(this));
        btnRestaurantsByDisponibility.setOnClickListener(new ClientController(this));
        btnRestaurantsByStar.setOnClickListener(new ClientController(this));
        btnCautaRestaurant.setOnClickListener(new ClientController(this));
        btnGoToRestaurant.setOnClickListener(new ClientController(this));
        btnCauta.setOnClickListener(new ClientController(this));
        btnRezerva.setOnClickListener(new ClientController(this));
        btnInapoi.setOnClickListener(new ClientController(this));
        btnCautaRestaurantSpecific.setOnClickListener(new ClientController(this));
    }
}
