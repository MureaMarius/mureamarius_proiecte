package com.example.proiectps.logIn;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proiectps.R;
import com.example.proiectps.activities.AdminActivity;
import com.example.proiectps.model.Admin;
import com.example.proiectps.model.Client;
import com.example.proiectps.service.ApiUtils;
import com.example.proiectps.service.WebApiService;
import com.example.proiectps.activities.ClientActivity;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogIn implements View.OnClickListener
{
    private ArrayList<Admin> admins= new ArrayList<>();
    private ArrayList<Client> clients= new ArrayList<>();

    private Button btnAdmin, btnClient, btnCreare, btnLogIn;
    private EditText editTextNume, editTextParola;

    private static boolean isAdmin=false;
    private static boolean isClient=false;

    private Context Activity;
    private WebApiService webApiService;

    private static Admin finalAdmin= null;

    public LogIn(Context ctx)
    {
        Activity=ctx;

        this.btnAdmin=(Button) ((Activity) Activity).findViewById(R.id.btnAdmin);
        this.btnClient=(Button) ((Activity) Activity).findViewById(R.id.btnClient);
        this.btnCreare=(Button) ((Activity) Activity).findViewById(R.id.btnCreare);
        this.btnLogIn=(Button) ((Activity) Activity).findViewById(R.id.btnLogIn);
        this.editTextNume=(EditText) ((Activity) Activity).findViewById(R.id.editTextNume);
        this.editTextParola=(EditText) ((Activity) Activity).findViewById(R.id.editTextParola);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnAdmin:
            {
                btnAdmin.setVisibility(View.INVISIBLE);
                btnClient.setVisibility(View.INVISIBLE);
                btnLogIn.setVisibility(View.VISIBLE);
                btnCreare.setVisibility(View.VISIBLE);
                editTextNume.setVisibility(View.VISIBLE);
                editTextParola.setVisibility(View.VISIBLE);

                isAdmin=true;
                isClient=false;
                break;
            }
            case R.id.btnClient:
            {
                btnClient.setVisibility(View.INVISIBLE);
                btnAdmin.setVisibility(View.INVISIBLE);
                btnLogIn.setVisibility(View.VISIBLE);
                btnCreare.setVisibility(View.VISIBLE);
                editTextNume.setVisibility(View.VISIBLE);
                editTextParola.setVisibility(View.VISIBLE);

                isAdmin=false;
                isClient=true;
                break;
            }
            case R.id.btnLogIn:
            {
                String nume= editTextNume.getText().toString();
                String parola= editTextParola.getText().toString();

                if(validateLogin(nume,parola))
                {
                    webApiService= ApiUtils.getUserService();
                    doLogin(nume,parola);

                    editTextNume.setText("");
                    editTextParola.setText("");
                }

                break;
            }
            case R.id.btnCreare:
            {
                String nume= editTextNume.getText().toString();
                String parola= editTextParola.getText().toString();

                if(validateLogin(nume,parola))
                {
                    webApiService= ApiUtils.getUserService();
                    doCreate(nume,parola);

                    editTextNume.setText("");
                    editTextParola.setText("");
                }
                break;
            }
            default:
            {
                System.out.println("Eroare!");
                break;
            }
        }
    }

    private boolean validateLogin(String username, String password)
    {
        if(username == null || username.trim().length() == 0)
        {
            Toast.makeText(Activity, "Username is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password == null || password.trim().length() == 0)
        {
            Toast.makeText(Activity, "Password is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void doLogin(final String username,final String password)
    {
        if(isAdmin && !isClient)
        {
            Call call = webApiService.getAdminByNameAndPass(username, password);
            call.enqueue(new Callback<Admin>() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.isSuccessful()) {
                        Admin admin = (Admin) response.body();
                        if (admin != null)
                        {
                            Toast.makeText(Activity, "This admin exist!", Toast.LENGTH_SHORT).show();

                            Intent intent= new Intent(Activity, AdminActivity.class);
                            intent.putExtra("object",admin);
                            Activity.startActivity(intent);
                        }
                    } else {
                        Toast.makeText(Activity, "The username or password is incorrect", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(Activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if(isClient && !isAdmin)
        {
            Call call = webApiService.getClientByNameAndPass(username, password);
            call.enqueue(new Callback<Client>() {
                @Override
                public void onResponse(Call call, Response response) {
                    if (response.isSuccessful()) {
                        Client client = (Client) response.body();
                        if (client != null)
                        {
                            Toast.makeText(Activity, "This client exist!", Toast.LENGTH_SHORT).show();
                            Intent intent= new Intent(Activity, ClientActivity.class);
                            intent.putExtra("object",client);
                            Activity.startActivity(intent);
                        }
                    } else
                        {
                        Toast.makeText(Activity, "The username or password is incorrect", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(Activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void doCreate(String nume, String parola)
    {
        if(isAdmin && !isClient)
        {
            Call call = webApiService.addAdmin(nume, parola);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call call, Response response)
                {
                    if (response.isSuccessful())
                    {
                        Toast.makeText(Activity, "A fost creat cu succes!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(Activity, "Ups!", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(Activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if(isClient && !isAdmin)
        {
            Call call = webApiService.addClient(nume, parola);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call call, Response response)
                {
                    if (response.isSuccessful())
                    {
                        Toast.makeText(Activity, "A fost creat cu succes!", Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(Activity, "Ups!", Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(Activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
