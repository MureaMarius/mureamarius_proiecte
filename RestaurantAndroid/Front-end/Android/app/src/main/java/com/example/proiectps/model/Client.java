package com.example.proiectps.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Client implements Serializable
{
    @SerializedName("idClient")
    private int idClient;

    @SerializedName("numeClient")
    private String numeClient;

    @SerializedName("restaurantAles")
    private Restaurant restaurantAles;

    @SerializedName("aRezervat")
    private String aRezervat;

    @SerializedName("achitatMasa")
    private boolean achitatMasa;

    @SerializedName("sumaAchitata")
    private int sumaAchitata;

    @SerializedName("parolaClient")
    private String parolaClient;

    public Client()
    {

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public Restaurant getRestaurantAles() {
        return restaurantAles;
    }

    public void setRestaurantAles(Restaurant restaurantAles) {
        this.restaurantAles = restaurantAles;
    }

    public String getaRezervat() {
        return aRezervat;
    }

    public void setaRezervat(String aRezervat) {
        this.aRezervat = aRezervat;
    }

    public boolean isAchitatMasa() {
        return achitatMasa;
    }

    public void setAchitatMasa(boolean achitatMasa) {
        this.achitatMasa = achitatMasa;
    }

    public int getSumaAchitata() {
        return sumaAchitata;
    }

    public void setSumaAchitata(int sumaAchitata) {
        this.sumaAchitata = sumaAchitata;
    }

    public String getParolaClient() {
        return parolaClient;
    }

    public void setParolaClient(String parolaClient) {
        this.parolaClient = parolaClient;
    }

    public String afisare()
    {
        return "Id: "+idClient+'\n'+"Nume: "+numeClient;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idClient=" + idClient +
                ", numeClient='" + numeClient + '\'' +
                ", restaurantAles=" + restaurantAles +
                ", aRezervat='" + aRezervat + '\'' +
                ", achitatMasa=" + achitatMasa +
                ", sumaAchitata=" + sumaAchitata +
                ", parolaClient='" + parolaClient + '\'' +
                '}';
    }
}
