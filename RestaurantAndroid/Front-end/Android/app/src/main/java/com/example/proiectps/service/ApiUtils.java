package com.example.proiectps.service;

public class ApiUtils
{
    public static final String BASE_URL = "http://192.168.1.7:8080";

    public static WebApiService getUserService()
    {
        return RetrofitClient.getClient(BASE_URL).create(WebApiService.class);
    }
}
