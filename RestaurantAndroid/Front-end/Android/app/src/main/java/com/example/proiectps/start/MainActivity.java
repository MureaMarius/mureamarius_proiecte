package com.example.proiectps.start;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.example.proiectps.R;
import com.example.proiectps.activities.Activity_LogIn;

public class MainActivity extends AppCompatActivity
{
    private Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        btnStart=findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openMainLogare();
            }
        });
    }

    private void openMainLogare()
    {
        Intent intent= new Intent(this, Activity_LogIn.class);
        startActivity(intent);
    }
}
