package com.example.proiectps.controller;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proiectps.R;
import com.example.proiectps.model.Admin;
import com.example.proiectps.model.Client;
import com.example.proiectps.model.Restaurant;
import com.example.proiectps.service.ApiUtils;
import com.example.proiectps.service.WebApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminController implements View.OnClickListener
{
    private Button btnAfisareRestaurante, btnAfisareAdmins, btnAfisareClients, btnShow;
    private Button btnDeleteRestaurants, btnDeleteClient, btnDelete;
    private Button btnGetMoney, btnModifyStar, btnGetModify;
    private Button btnInapoi;
    private Button btnGetByAchitat;
    private Button btnModifyNota;

    private TextView textView;
    private EditText txtData1;
    private EditText txtData2, txtData3, txtData4;

    private Context Activity;
    private WebApiService webApiService;

    public AdminController(Context ctx) {
        Activity = ctx;

        this.btnAfisareRestaurante = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnAfisareRestaurants);
        this.btnAfisareAdmins = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnAfisareAdmin);
        this.btnAfisareClients = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnAfisareClients);
        this.btnShow = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnShow);
        this.btnDeleteRestaurants = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnDeleteAllRestaurants);
        this.btnDeleteClient = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnDeleteByAchitat);
        this.btnDelete = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnDelete);
        this.btnGetMoney = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnGetMoney);
        this.btnModifyStar = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnModifyStar);
        this.btnGetModify = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnGetModify);
        this.btnInapoi = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnInapoiAdmin);
        this.btnGetByAchitat = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnGetClientByAchitat);
        this.btnModifyNota = (Button) ((android.app.Activity) Activity).findViewById(R.id.btnModifyNota);

        this.textView = (TextView) ((android.app.Activity) Activity).findViewById(R.id.txtViewAdmin);
        this.textView.setMovementMethod(new ScrollingMovementMethod());

        this.txtData1 = (EditText) ((android.app.Activity) Activity).findViewById(R.id.editTextClientsByAchitat);
        this.txtData2 = (EditText) ((android.app.Activity) Activity).findViewById(R.id.txtNumeRestaurant);
        this.txtData3 = (EditText) ((android.app.Activity) Activity).findViewById(R.id.txtLocatieRestaurant);
        this.txtData4 = (EditText) ((android.app.Activity) Activity).findViewById(R.id.txtNotaNoua);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShow: {
                textView.setText("");

                btnAfisareAdmins.setVisibility(View.VISIBLE);
                btnAfisareClients.setVisibility(View.VISIBLE);
                btnAfisareRestaurante.setVisibility(View.VISIBLE);

                btnDelete.setVisibility(View.INVISIBLE);
                btnShow.setVisibility(View.INVISIBLE);
                btnGetModify.setVisibility(View.INVISIBLE);
                break;
            }
            case R.id.btnAfisareRestaurants: {
                textView.setText("");
                webApiService = ApiUtils.getUserService();
                getEntity("restaurants", false);

                textView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnAfisareAdmin: {
                textView.setText("");
                webApiService = ApiUtils.getUserService();
                getEntity("admins", false);

                textView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnAfisareClients: {
                txtData1.setVisibility(View.VISIBLE);
                btnGetByAchitat.setVisibility(View.VISIBLE);
                btnAfisareClients.setVisibility(View.INVISIBLE);
                btnAfisareAdmins.setVisibility(View.INVISIBLE);
                btnAfisareRestaurante.setVisibility(View.INVISIBLE);

                break;
            }
            case R.id.btnGetClientByAchitat: {
                textView.setText("");
                boolean status = Boolean.parseBoolean(txtData1.getText().toString());
                webApiService = ApiUtils.getUserService();
                getEntity("clients", status);

                textView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnDelete: {
                btnShow.setVisibility(View.INVISIBLE);
                btnGetModify.setVisibility(View.INVISIBLE);
                btnDelete.setVisibility(View.INVISIBLE);

                btnDeleteRestaurants.setVisibility(View.VISIBLE);
                btnDeleteClient.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.btnDeleteByAchitat: {
                webApiService = ApiUtils.getUserService();
                delete("clients");
                break;
            }
            case R.id.btnDeleteAllRestaurants:
            {
                webApiService = ApiUtils.getUserService();
                delete("restaurants");
                break;
            }
            case R.id.btnGetModify:
            {
                btnDelete.setVisibility(View.INVISIBLE);
                btnShow.setVisibility(View.INVISIBLE);
                btnGetModify.setVisibility(View.INVISIBLE);

                btnGetMoney.setVisibility(View.VISIBLE);
                btnModifyStar.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.btnModifyStar:
            {
                btnGetMoney.setVisibility(View.INVISIBLE);
                btnModifyStar.setVisibility(View.INVISIBLE);

                txtData2.setVisibility(View.VISIBLE);
                txtData3.setVisibility(View.VISIBLE);
                txtData4.setVisibility(View.VISIBLE);

                btnModifyNota.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.btnModifyNota:
            {
                textView.setText("");
                String numeRestaurant= txtData2.getText().toString();
                String locatieRestaurant= txtData3.getText().toString();
                int notaNoua= Integer.parseInt(txtData4.getText().toString());

                webApiService = ApiUtils.getUserService();
                modifyNota(numeRestaurant,locatieRestaurant,notaNoua);

                txtData1.setText("");
                txtData2.setText("");
                txtData3.setText("");
                txtData4.setText("");
                break;
            }
            case R.id.btnGetMoney:
            {
                webApiService = ApiUtils.getUserService();
                getMoney();

                textView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnInapoiAdmin: {
                btnDelete.setVisibility(View.VISIBLE);
                btnShow.setVisibility(View.VISIBLE);
                btnGetModify.setVisibility(View.VISIBLE);

                btnAfisareRestaurante.setVisibility(View.INVISIBLE);
                btnAfisareClients.setVisibility(View.INVISIBLE);
                btnAfisareAdmins.setVisibility(View.INVISIBLE);
                btnDeleteClient.setVisibility(View.INVISIBLE);
                btnDeleteRestaurants.setVisibility(View.INVISIBLE);
                btnGetMoney.setVisibility(View.INVISIBLE);
                btnModifyStar.setVisibility(View.INVISIBLE);
                btnGetByAchitat.setVisibility(View.INVISIBLE);
                txtData1.setVisibility(View.INVISIBLE);

                txtData2.setVisibility(View.INVISIBLE);
                txtData3.setVisibility(View.INVISIBLE);
                txtData4.setVisibility(View.INVISIBLE);

                btnGetMoney.setVisibility(View.INVISIBLE);

                btnModifyNota.setVisibility(View.INVISIBLE);

                textView.setVisibility(View.INVISIBLE);

                txtData1.setText("");
                txtData2.setText("");
                txtData3.setText("");
                txtData4.setText("");

                textView.setText("");
                break;
            }
            default: {
                System.out.println("Eroare!");
                break;
            }
        }
    }

    private void getEntity(String entity, boolean status) {
        if (entity.equalsIgnoreCase("restaurants")) {
            Call<List<Restaurant>> call = webApiService.getAllRestaurants();

            call.enqueue(new Callback<List<Restaurant>>() {
                @Override
                public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
                    if (response.isSuccessful()) {
                        List<Restaurant> restaurants = response.body();

                        for (Restaurant restaurant : restaurants) {
                            String content = "";

                            content += "ID: " + restaurant.getIdRestaurant() + '\n';
                            content += "-Nume restaurant: " + restaurant.getNumeRestaurant() + '\n';
                            content += "-Locatie restaurant: " + restaurant.getLocatieRestaurant() + '\n';
                            content += "-Nota restaurant: " + restaurant.getNotaRestaurant() + '\n';
                            content += "-Mese libere: " + restaurant.getMeseLibere() + '\n';
                            content += "-Nume admin: " + restaurant.getNumeAdmin() + "\n\n";

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        } else if (entity.equalsIgnoreCase("admins")) {
            Call<List<Admin>> call = webApiService.getAllAdmins();

            call.enqueue(new Callback<List<Admin>>() {
                @Override
                public void onResponse(Call<List<Admin>> call, Response<List<Admin>> response) {
                    if (response.isSuccessful()) {
                        List<Admin> admins = response.body();

                        for (Admin admin : admins) {
                            String content = "";

                            content += "ID: " + admin.getIdAdmin() + '\n';
                            content += "-Nume admin: " + admin.getNumeAdmin() + '\n';
                            content += "-Restaurante administrate: " + admin.getRestauranteAdministrate() + '\n';

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        } else if (entity.equalsIgnoreCase("clients")) {
            Call<List<Client>> call = webApiService.getClients(status);

            call.enqueue(new Callback<List<Client>>() {
                @Override
                public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                    if (response.isSuccessful()) {
                        List<Client> clients = response.body();

                        for (Client client : clients) {
                            String content = "";

                            content += "ID: " + client.getIdClient() + '\n';
                            content += "-Nume client: " + client.getNumeClient() + '\n';
                            content += "-Restaurant ales: " + client.getRestaurantAles() + '\n';
                            content += "-Suma achitata: " + client.getSumaAchitata() + '\n';

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
    }

    private void delete(String entity)
    {
        if(entity.equalsIgnoreCase("clients")) {
            Call<Void> call = webApiService.deleteByAchitat();

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(Activity, "Stergere cu succes!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Activity, "Mai incearca!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
        else if(entity.equalsIgnoreCase("restaurants"))
        {
            Call<Void> call = webApiService.deleteAllRestaurants();

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(Activity, "Stergere cu succes!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Activity, "Mai incearca!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
    }

    private void modifyNota(String numeRestaurant, String locatieRestaurant, int notaNoua)
    {
        Call<Void> call = webApiService.modifyNoteRestaurant(numeRestaurant,locatieRestaurant,notaNoua);

        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (response.isSuccessful()) {
                    Toast.makeText(Activity, "Modificarea s-a efectuat cu succes", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Activity, "Mai incearca!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                System.out.println("Eroare " + t.getMessage());
            }
        });
    }

    private void getMoney()
    {
        Call<Integer> call = webApiService.getAmountOfMoney();

        call.enqueue(new Callback<Integer>()
        {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response)
            {
                if (response.isSuccessful())
                {
                    Integer money= response.body();

                    String content=String.valueOf(money);
                    textView.append(content);



                } else {
                    Toast.makeText(Activity, "Saracie!!!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                System.out.println("Eroare " + t.getMessage());
            }
        });
    }
}
