package com.example.proiectps.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;

import com.example.proiectps.R;
import com.example.proiectps.logIn.LogIn;

public class Activity_LogIn extends AppCompatActivity
{
    private Button btnAdmin, btnClient, btnLogIn, btnCreare;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity__log_in);

        btnAdmin= findViewById(R.id.btnAdmin);
        btnClient= findViewById(R.id.btnClient);
        btnLogIn= findViewById(R.id.btnLogIn);
        btnCreare= findViewById(R.id.btnCreare);

        btnAdmin.setOnClickListener(new LogIn(this));
        btnClient.setOnClickListener(new LogIn(this));
        btnLogIn.setOnClickListener(new LogIn(this));
        btnCreare.setOnClickListener(new LogIn(this));
    }
}
