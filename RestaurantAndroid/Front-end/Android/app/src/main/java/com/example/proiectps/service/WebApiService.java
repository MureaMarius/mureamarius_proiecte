package com.example.proiectps.service;

import com.example.proiectps.model.Admin;
import com.example.proiectps.model.Client;
import com.example.proiectps.model.Restaurant;

import java.util.List;
import java.util.Optional;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WebApiService
{
    @GET("/getAllRestaurants")
    Call<List<Restaurant>> getAllRestaurants();

    @POST("/createRestaurant/{idRestaurant}/{numeRestaurant}/{locatieRestaurant}/{numarStele}/{meseLibere}/{numeAdmin}")
    Call<Response<Object>> createRestaurant(@Path("idRestaurant") int idRestaurant, @Path("numeRestaurant") String numeRestaurant, @Path("locatieRestaurant") String locatieRestaurant, @Path("numarStele") int numarStele, @Path("meseLibere") int meseLibere, @Path("numeAdmin") String numeAdmin);

    @GET("/getAllRestaurantsByDisponibility")
    Call<List<Restaurant>> getAllRestaurantsByDisponibility();

    @GET("/getRestaurantsByStar/{nota}")
    Call<List<Restaurant>> getAllRestaurantsByNote(@Path("nota") int nota);

    @GET("/findARestaurant/{nume}/{locatie}")
    Call<Restaurant> getRestaurant(@Path("nume") String nume, @Path("locatie") String locatie);

    @DELETE("deleteAllRestaurants")
    Call<Void> deleteAllRestaurants();

    @POST("/modifyNota/{numeRestaurant}/{locatieRestaurant}/{nota}")
    Call<Void> modifyNoteRestaurant(@Path("numeRestaurant") String numeRestaurant, @Path("locatieRestaurant") String locatieRestaurant, @Path("nota") int nota);

    @GET("/getAdminByNameAndPass/{numeAdmin}/{parolaAdmin}")
    Call<Admin> getAdminByNameAndPass(@Path("numeAdmin") String numeAdmin, @Path("parolaAdmin") String parolaAdmin);

    @GET("/getAllAdmins")
    Call<List<Admin>> getAllAdmins();

    @POST("addAdmin/{numeAdmin}/{parolaAdmin}")
    Call<Void> addAdmin(@Path("numeAdmin") String numeAdmin, @Path("parolaAdmin") String parolaAdmin);

    @GET("/getClientByNameAndPass/{numeClient}/{parolaClient}")
    Call<Client> getClientByNameAndPass(@Path("numeClient") String numeClient, @Path("parolaClient") String parolaClient);

    @POST("addClient/{numeClient}/{parolaClient}")
    Call<Void> addClient(@Path("numeClient") String numeClient, @Path("parolaClient") String parolaClient);

    @GET("/getClientsByAchitat/{achitatMasa}")
    Call<List<Client>> getClients(@Path("achitatMasa") boolean achitatMasa);

    @DELETE("/deleteByAchitat")
    Call<Void> deleteByAchitat();

    @GET("/getAmountOfMoney")
    Call<Integer> getAmountOfMoney();

    @POST("/goToRestaurant/{idClient}/{numeRestaurant}/{locatieRestaurant}")
    Call<Void> goToRestaurant(@Path("idClient") int idClient,@Path("numeRestaurant") String numeRestaurant,@Path("locatieRestaurant") String locatieRestaurant);
}
