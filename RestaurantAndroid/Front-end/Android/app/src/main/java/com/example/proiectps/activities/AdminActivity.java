package com.example.proiectps.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.proiectps.R;
import com.example.proiectps.controller.AdminController;
import com.example.proiectps.model.Admin;

public class AdminActivity extends AppCompatActivity
{
    private Button btnAfisareRestaurante, btnAfisareAdmins, btnAfisareClients, btnShow;
    private Button btnDeleteRestaurants, btnDeleteClient, btnDelete;
    private Button btnGetMoney, btnModifyStar, btnGetModify;
    private Button btnInapoi;
    private Button btnGetByAchitat;
    private Button btnModifyNota;
    private TextView txtAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_admin);

        Intent i= getIntent();
        Admin admin=(Admin) i.getSerializableExtra("object");
        txtAdmin= findViewById(R.id.txtAdmin);
        txtAdmin.setText(admin.afisare());

        btnInapoi= findViewById(R.id.btnInapoiAdmin);
        btnAfisareAdmins= findViewById(R.id.btnAfisareAdmin);
        btnAfisareClients= findViewById(R.id.btnAfisareClients);
        btnAfisareRestaurante= findViewById(R.id.btnAfisareRestaurants);
        btnShow= findViewById(R.id.btnShow);
        btnDeleteRestaurants= findViewById(R.id.btnDeleteAllRestaurants);
        btnDeleteClient= findViewById(R.id.btnDeleteByAchitat);
        btnDelete= findViewById(R.id.btnDelete);
        btnGetMoney= findViewById(R.id.btnGetMoney);
        btnModifyStar= findViewById(R.id.btnModifyStar);
        btnGetModify= findViewById(R.id.btnGetModify);
        btnGetByAchitat= findViewById(R.id.btnGetClientByAchitat);
        btnModifyNota= findViewById(R.id.btnModifyNota);

        btnInapoi.setOnClickListener(new AdminController(this));
        btnAfisareAdmins.setOnClickListener(new AdminController(this));
        btnAfisareClients.setOnClickListener(new AdminController(this));
        btnAfisareRestaurante.setOnClickListener(new AdminController(this));
        btnShow.setOnClickListener(new AdminController(this));
        btnDeleteRestaurants.setOnClickListener(new AdminController(this));
        btnDeleteClient.setOnClickListener(new AdminController(this));
        btnDelete.setOnClickListener(new AdminController(this));
        btnGetMoney.setOnClickListener(new AdminController(this));
        btnModifyStar.setOnClickListener(new AdminController(this));
        btnGetModify.setOnClickListener(new AdminController(this));
        btnGetByAchitat.setOnClickListener(new AdminController(this));
        btnModifyNota.setOnClickListener(new AdminController(this));
    }
}
