package com.example.proiectps.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Admin implements Serializable
{
    @SerializedName("idAdmin")
    private int idAdmin;

    @SerializedName("numeAdmin")
    private String numeAdmin;

    @SerializedName("restauranteAdministrate")
    private int restauranteAdministrate;

    @SerializedName("parolaAdmin")
    private String parolaAdmin;

    public Admin()
    {

    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getNumeAdmin() {
        return numeAdmin;
    }

    public void setNumeAdmin(String numeAdmin) {
        this.numeAdmin = numeAdmin;
    }

    public int getRestauranteAdministrate() {
        return restauranteAdministrate;
    }

    public void setRestauranteAdministrate(int restauranteAdministrate) {
        this.restauranteAdministrate = restauranteAdministrate;
    }

    public String getParolaAdmin() {
        return parolaAdmin;
    }

    public void setParolaAdmin(String parolaAdmin) {
        this.parolaAdmin = parolaAdmin;
    }

    public String afisare()
    {
        return "Id: "+idAdmin+'\n'+"Nume: "+numeAdmin;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "idAdmin=" + idAdmin +
                ", numeAdmin='" + numeAdmin + '\'' +
                ", restauranteAdministrate=" + restauranteAdministrate +
                ", parolaAdmin='" + parolaAdmin + '\'' +
                '}';
    }
}
