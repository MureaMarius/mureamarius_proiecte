package com.example.proiectps.controller;

import android.content.Context;
import android.content.Intent;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proiectps.R;
import com.example.proiectps.model.Restaurant;
import com.example.proiectps.service.ApiUtils;
import com.example.proiectps.service.WebApiService;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientController implements View.OnClickListener
{
    private Button btnAfisareRestaurante, btnRestaurantsByDisponibility, btnRestaurantsByStar, btnCautaRestaurant, btnGoToRestaurant, btnCauta, btnRezerva, btnInapoi;
    private TextView textView;
    private EditText txtData1, txtData2, txtData3;
    private Button btnCautaRestaurantSpecific;
    private Context Activity;
    private WebApiService webApiService;

    public ClientController(Context ctx)
    {
        Activity=ctx;

        this.btnAfisareRestaurante=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnAfisareRestaurante);
        this.btnRestaurantsByDisponibility=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnRestaurantsByDisponibility);
        this.btnRestaurantsByStar=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnRestaurantsByNote);
        this.btnCautaRestaurant=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnSearchASpecificRestaurant);
        this.btnGoToRestaurant=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnGoToRestaurant);
        this.btnCauta=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnCauta);
        this.btnRezerva=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnRezerva);
        this.btnInapoi=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnInapoi);
        this.btnCautaRestaurantSpecific=(Button) ((android.app.Activity) Activity).findViewById(R.id.btnCautaSpecificRestaurant);

        this.txtData1=(EditText) ((android.app.Activity) Activity).findViewById(R.id.txtData1);
        this.txtData2=(EditText) ((android.app.Activity) Activity).findViewById(R.id.txtData2);
        this.txtData3=(EditText) ((android.app.Activity) Activity).findViewById(R.id.txtData3);

        this.textView= (TextView)((android.app.Activity) Activity).findViewById(R.id.txtView);

        this.textView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnAfisareRestaurante:
            {
                textView.setText("");
                webApiService= ApiUtils.getUserService();
                getRestaurants("all", 0);
                textView.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.btnRestaurantsByDisponibility:
            {
                textView.setText("");
                webApiService= ApiUtils.getUserService();
                getRestaurants("disponibility", 0);
                textView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnRestaurantsByNote:
            {
                textView.setText("");

                txtData3.setVisibility(View.VISIBLE);
                btnCauta.setVisibility(View.VISIBLE);

                btnAfisareRestaurante.setVisibility(View.INVISIBLE);
                btnGoToRestaurant.setVisibility(View.INVISIBLE);
                btnCautaRestaurant.setVisibility(View.INVISIBLE);
                btnRestaurantsByDisponibility.setVisibility(View.INVISIBLE);
                btnRestaurantsByStar.setVisibility(View.INVISIBLE);

                break;
            }
            case R.id.btnSearchASpecificRestaurant:
            {
                textView.setText("");

                txtData1.setVisibility(View.VISIBLE);
                txtData2.setVisibility(View.VISIBLE);

                btnAfisareRestaurante.setVisibility(View.INVISIBLE);
                btnGoToRestaurant.setVisibility(View.INVISIBLE);
                btnCautaRestaurant.setVisibility(View.INVISIBLE);
                btnRestaurantsByDisponibility.setVisibility(View.INVISIBLE);
                btnRestaurantsByStar.setVisibility(View.INVISIBLE);

                btnCautaRestaurantSpecific.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.btnGoToRestaurant:
            {
                textView.setText("");

                txtData1.setVisibility(View.VISIBLE);
                txtData2.setVisibility(View.VISIBLE);
                txtData3.setVisibility(View.VISIBLE);
                btnRezerva.setVisibility(View.VISIBLE);

                btnAfisareRestaurante.setVisibility(View.INVISIBLE);
                btnGoToRestaurant.setVisibility(View.INVISIBLE);
                btnCautaRestaurant.setVisibility(View.INVISIBLE);
                btnRestaurantsByDisponibility.setVisibility(View.INVISIBLE);
                btnRestaurantsByStar.setVisibility(View.INVISIBLE);

                break;
            }
            case R.id.btnCauta:
            {
                textView.setText("");
                int nota= Integer.parseInt(txtData3.getText().toString());
                webApiService= ApiUtils.getUserService();
                getRestaurants("star", nota);
                textView.setVisibility(View.VISIBLE);
                txtData3.setText("");

                break;
            }
            case R.id.btnRezerva:
            {
                textView.setText("");

                int idClient= Integer.parseInt(txtData3.getText().toString());
                String numeRestaurant= txtData1.getText().toString();
                String locatieRestaurant= txtData2.getText().toString();

                webApiService= ApiUtils.getUserService();
                rezervaMasa(idClient,numeRestaurant,locatieRestaurant);

                txtData3.setText("");
                txtData2.setText("");
                txtData1.setText("");

                break;
            }
            case R.id.btnInapoi:
            {
                txtData3.setVisibility(View.INVISIBLE);
                txtData2.setVisibility(View.INVISIBLE);
                txtData1.setVisibility(View.INVISIBLE);

                textView.setVisibility(View.INVISIBLE);
                btnCauta.setVisibility(View.INVISIBLE);
                btnRezerva.setVisibility(View.INVISIBLE);
                btnCautaRestaurantSpecific.setVisibility(View.INVISIBLE);

                btnAfisareRestaurante.setVisibility(View.VISIBLE);
                btnGoToRestaurant.setVisibility(View.VISIBLE);
                btnCautaRestaurant.setVisibility(View.VISIBLE);
                btnRestaurantsByDisponibility.setVisibility(View.VISIBLE);
                btnRestaurantsByStar.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.btnCautaSpecificRestaurant:
            {
                textView.setText("");
                String numeRestaurant=txtData1.getText().toString();
                String locatieRestaurant= txtData2.getText().toString();

                webApiService= ApiUtils.getUserService();
                cautaRestaurantDupaNumeSiLocatie(numeRestaurant,locatieRestaurant);

                txtData3.setText("");
                txtData2.setText("");
                txtData1.setText("");

                textView.setVisibility(View.VISIBLE);
                break;
            }
            default:
            {
                System.out.println("Eroare!");
                break;
            }
        }
    }

    private void getRestaurants(String type, int nota)
    {
        if (type.equalsIgnoreCase("all"))
        {
            Call<List<Restaurant>> call = webApiService.getAllRestaurants();

            call.enqueue(new Callback<List<Restaurant>>() {
                @Override
                public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
                    if (response.isSuccessful()) {
                        List<Restaurant> restaurants = response.body();

                        for (Restaurant restaurant : restaurants) {
                            String content = "";

                            content += "ID: " + restaurant.getIdRestaurant() + '\n';
                            content += "-Nume restaurant: " + restaurant.getNumeRestaurant() + '\n';
                            content += "-Locatie restaurant: " + restaurant.getLocatieRestaurant() + '\n';
                            content += "-Nota restaurant: " + restaurant.getNotaRestaurant() + '\n';
                            content += "-Mese libere: " + restaurant.getMeseLibere() + '\n';
                            content += "-Nume admin: " + restaurant.getNumeAdmin() + "\n\n";

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
        else if(type.equalsIgnoreCase("disponibility"))
        {
            Call<List<Restaurant>> call = webApiService.getAllRestaurantsByDisponibility();

            call.enqueue(new Callback<List<Restaurant>>()
            {
                @Override
                public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response)
                {
                    if (response.isSuccessful())
                    {
                        List<Restaurant> restaurants = response.body();

                        for (Restaurant restaurant : restaurants)
                        {
                            String content = "";

                            content += "ID: " + restaurant.getIdRestaurant() + '\n';
                            content += "-Nume restaurant: " + restaurant.getNumeRestaurant() + '\n';
                            content += "-Locatie restaurant: " + restaurant.getLocatieRestaurant() + '\n';
                            content += "-Nota restaurant: " + restaurant.getNotaRestaurant() + '\n';
                            content += "-Mese libere: " + restaurant.getMeseLibere() + '\n';
                            content += "-Nume admin: " + restaurant.getNumeAdmin() + "\n\n";

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t)
                {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
        else if(type.equalsIgnoreCase("star"))
        {
            Call<List<Restaurant>> call = webApiService.getAllRestaurantsByNote(nota);

            call.enqueue(new Callback<List<Restaurant>>()
            {
                @Override
                public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response)
                {
                    if (response.isSuccessful())
                    {
                        List<Restaurant> restaurants = response.body();
                        textView.setText("");
                        for (Restaurant restaurant : restaurants)
                        {
                            String content = "";

                            content += "ID: " + restaurant.getIdRestaurant() + '\n';
                            content += "-Nume restaurant: " + restaurant.getNumeRestaurant() + '\n';
                            content += "-Locatie restaurant: " + restaurant.getLocatieRestaurant() + '\n';
                            content += "-Nota restaurant: " + restaurant.getNotaRestaurant() + '\n';
                            content += "-Mese libere: " + restaurant.getMeseLibere() + '\n';
                            content += "-Nume admin: " + restaurant.getNumeAdmin() + "\n\n";

                            textView.append(content);
                        }

                    } else {
                        System.out.println("Eroare");
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t)
                {
                    System.out.println("Eroare " + t.getMessage());
                }
            });
        }
    }

    private void rezervaMasa(int id, String numeRestaurant, String locatieRestaurant)
    {
        Call<Void> call = webApiService.goToRestaurant(id,numeRestaurant,locatieRestaurant);

        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (response.isSuccessful())
                {
                    Toast.makeText(Activity, "Felicitari! Ai rezervat o masa la acest restaurant!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Activity, "Nu s-a putut rezerva masa!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call call, Throwable t)
                {
                System.out.println("Eroare " + t.getMessage());
            }
        });
    }

    private void cautaRestaurantDupaNumeSiLocatie(String numeRestaurant, String locatieRestaurant)
    {
        Call<Restaurant> call = webApiService.getRestaurant(numeRestaurant,locatieRestaurant);

        call.enqueue(new Callback<Restaurant>()
        {
            @Override
            public void onResponse(Call<Restaurant> call, Response<Restaurant> response)
            {
                if (response.isSuccessful())
                {
                    Restaurant restaurant=response.body();

                    String content = "";

                    content += "ID: " + restaurant.getIdRestaurant() + '\n';
                    content += "-Nume restaurant: " + restaurant.getNumeRestaurant() + '\n';
                    content += "-Locatie restaurant: " + restaurant.getLocatieRestaurant() + '\n';
                    content += "-Nota restaurant: " + restaurant.getNotaRestaurant() + '\n';
                    content += "-Mese libere: " + restaurant.getMeseLibere() + '\n';
                    content += "-Nume admin: " + restaurant.getNumeAdmin() + "\n\n";

                    textView.append(content);

                } else {
                    Toast.makeText(Activity, "Nu s-a gasit acest restaurant!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call call, Throwable t)
            {
                System.out.println("Eroare " + t.getMessage());
            }
        });
    }
}
