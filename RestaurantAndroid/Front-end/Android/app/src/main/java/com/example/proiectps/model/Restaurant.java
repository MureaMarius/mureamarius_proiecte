package com.example.proiectps.model;

import com.google.gson.annotations.SerializedName;

public class Restaurant
{
    @SerializedName("idRestaurant")
    private int idRestaurant;

    @SerializedName("numeRestaurant")
    private String numeRestaurant;

    @SerializedName("locatieRestaurant")
    private String locatieRestaurant;

    @SerializedName("notaRestaurant")
    private int notaRestaurant;

    @SerializedName("meseLibere")
    private int meseLibere;

    @SerializedName("numeAdmin")
    private String numeAdmin;

    public Restaurant()
    {

    }

    public int getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public String getNumeRestaurant() {
        return numeRestaurant;
    }

    public void setNumeRestaurant(String numeRestaurant) {
        this.numeRestaurant = numeRestaurant;
    }

    public String getLocatieRestaurant() {
        return locatieRestaurant;
    }

    public void setLocatieRestaurant(String locatieRestaurant) {
        this.locatieRestaurant = locatieRestaurant;
    }

    public int getNotaRestaurant() {
        return notaRestaurant;
    }

    public void setNotaRestaurant(int notaRestaurant) {
        this.notaRestaurant = notaRestaurant;
    }

    public int getMeseLibere() {
        return meseLibere;
    }

    public void setMeseLibere(int meseLibere) {
        this.meseLibere = meseLibere;
    }

    public String getNumeAdmin() {
        return numeAdmin;
    }

    public void setNumeAdmin(String numeAdmin) {
        this.numeAdmin = numeAdmin;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "idRestaurant=" + idRestaurant +
                ", numeRestaurant='" + numeRestaurant + '\'' +
                ", locatieRestaurant='" + locatieRestaurant + '\'' +
                ", notaRestaurant=" + notaRestaurant +
                ", meseLibere=" + meseLibere +
                ", numeAdmin='" + numeAdmin + '\'' +
                '}';
    }
}
