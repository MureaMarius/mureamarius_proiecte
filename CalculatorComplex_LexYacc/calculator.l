%{
	#include "y.tab.h"

	void yyerror (char *s);
	int yylex();
%}

%%

"print"								{return PRINT;}
"exit"				   				{return EXIT_COMMAND;}

"Operatii matematice"				{return Operatii_Matematice;}
"Date calculator"					{return Date_Calculator;}
"Rezolva ecuatia"					{return ECUATIE;}
"Calcul Temperatura"				{return Calcul_Temperatura;}
"Time conversion"					{return Time_Conversion;}
"Derivare expresii"					{return DERIVARE;}
"Calculator complex"				{return COMPLEX;}

"Ianuarie"							{return IANUARIE;}
"Februarie"							{return FEBRUARIE;}
"Martie"							{return MARTIE;}
"Aprilie"							{return APRILIE;}
"Mai"								{return MAI;}
"Iunie"								{return IUNIE;}
"Iulie"								{return IULIE;}
"August"							{return AUGUST;}
"Septembrie"							{return SEPTEMBRIE;}
"Octombrie"							{return OCTOMBRIE;}
"Noiembrie"							{return NOIEMBRIE;}
"Decembrie"							{return DECEMBRIE;}

"C"									{return CELSIUS;}
"F"									{return FAHRENHEIT;}
"K"									{return KELVIN;}


"sin(x)"								{return SIN;}
"cos(x)"								{return COS;}
"e^x"									{return EX;}
"ln(x)"									{return LNX;}

"-"									{return SCADERE;}
"/"									{return IMPARTIRE;}
"pana"									{return PANA_LA;}

"x"									{return X;}		

"i"									{return COMPLEX_I;}
"z1"								{return Z1;}
"z2"								{return Z2;}
"z3"								{return Z3;}
"Real de "							{return REAL;}
"Imaginar de "						{return IMAGINAR;}


"secunde"									{return SECUNDE;}


[a-zA-Z]			   {yylval.id = yytext[0]; return IDENTIFIER;}
[0-9]+                 {yylval.num = atoi(yytext); return NUMBER;}
[+=*^;]           	   {return yytext[0];}				
[<>]				   {return yytext[0];}

[ \t\n]                ;
.                      {ECHO; yyerror ("unexpected character");}


%%

int yywrap (void) 
{
	return 1;
}