%{
	void yyerror (char *s);
	int yylex();

	#include <stdio.h>     /* C declarations used in actions */
	#include <stdlib.h>
	#include <string.h>
	#include <math.h>
	#include <ctype.h>

	float calculOperatiiMatematice(int numar1,int numar2,char operatie);


	char *complex1;
	char *complex2;
	char *complex3;

	char *aComplex1, *bComplex1;
	char *aComplex2, *bComplex2;
	char *aComplex3, *bComplex3;

	int a1, a2, b1, b2, a3, b3;
%}

%union {int num; char* id;}         /* Yacc definitions */

%start line

%token PRINT
%token EXIT_COMMAND

///////Pt numere complexe
%token <id> COMPLEX_I
%token <id> Z1
%token <id> Z2
%token <id> Z3
%token REAL
%token IMAGINAR

//////Derivare
%token DERIVARE
%token SIN
%token COS
%token EX
%token LNX

/////Pentru temperatura
%token CELSIUS
%token FAHRENHEIT
%token KELVIN

/////////Moduri calculator
%token Operatii_Matematice
%token Date_Calculator
%token Calcul_Temperatura
%token Time_Conversion
%token ECUATIE
%token COMPLEX

/////////Lunile anului
%token IANUARIE
%token FEBRUARIE
%token MARTIE
%token APRILIE
%token MAI
%token IUNIE
%token IULIE
%token AUGUST
%token SEPTEMBRIE
%token OCTOMBRIE
%token NOIEMBRIE
%token DECEMBRIE

%token <num> NUMBER
%token <id> IDENTIFIER

%token SCADERE
%token IMPARTIRE
%token PANA_LA

%token X

%type <num> calculOperatie
%type <num> calculatorDate
%type <id> modDerivare
%type <id> calculTime
%type <id> calculComplex

%type <num> luna
%token PUTERE

//////Pentru conversia secundelor in ore minute si secunde
%token SECUNDE

%%

line: EXIT_COMMAND 												{exit(EXIT_SUCCESS);}						
	| Operatii_Matematice calculOperatie            					{;}
	| Date_Calculator calculatorDate									{;}
	| Calcul_Temperatura calculatorTemperatura							{;}
	| ECUATIE calculatorEcuatie											{;}
	| DERIVARE modDerivare												{;}
	| Time_Conversion calculTime 										{;}
	| COMPLEX calculComplex												{;}
	| line calculOperatie												{;}
	| line calculatorDate												{;}
	| line calculatorTemperatura										{;}
	| line calculatorEcuatie											{;}
	| line modDerivare													{;}
	| line calculTime 													{;}
	| line calculComplex												{;}
	| line EXIT_COMMAND											{exit(EXIT_SUCCESS);}
	;

calculOperatie:  NUMBER '+' NUMBER						{float rezultat=calculOperatiiMatematice($1,$3,'+'); printf("Rezultatul adunarii este:  %0.2f\n\n",rezultat);}
				| NUMBER SCADERE NUMBER					{float rezultat=calculOperatiiMatematice($1,$3,'-'); printf("Rezultatul scaderii este:  %0.2f\n\n",rezultat);}
				| NUMBER '*' NUMBER						{float rezultat=calculOperatiiMatematice($1,$3,'*'); printf("Rezultatul inmultirii este:  %0.2f\n\n",rezultat);}
				| NUMBER IMPARTIRE NUMBER				{float rezultat=calculOperatiiMatematice($1,$3,'/'); printf("Rezultatul impartirii este: %0.2f\n\n",rezultat);}
				;

calculatorDate: NUMBER luna	NUMBER PANA_LA NUMBER luna NUMBER		{calculDate($1,$2,$3,$5,$6,$7);}
				;

luna: IANUARIE					{$$=1;}
	| FEBRUARIE					{$$=2;}
	| MARTIE					{$$=3;}
	| APRILIE					{$$=4;}
	| MAI					{$$=5;}
	| IUNIE					{$$=6;}
	| IULIE					{$$=7;}
	| AUGUST					{$$=8;}
	| SEPTEMBRIE					{$$=9;}
	| OCTOMBRIE					{$$=10;}
	| NOIEMBRIE					{$$=11;}
	| DECEMBRIE					{$$=12;}
	;

calculatorTemperatura: NUMBER CELSIUS					{calculTemperatura($1,"Celsius");}
					| NUMBER FAHRENHEIT					{calculTemperatura($1,"Fahrenheit");}
					| NUMBER KELVIN						{calculTemperatura($1,"Kelvin");}
					;

calculatorEcuatie: NUMBER '*' X '+' NUMBER '=' NUMBER								{float x=($7-$5)/(float)$1; printf("Rezultatul ecuatiei de grad 1 este: X = %.02f \n\n", x);}
				| NUMBER '*' X '-' NUMBER '=' NUMBER 								{float x=($7+$5)/(float)$1; printf("Rezultatul ecuatiei de grad 1 este: X = %.02f \n\n", x);}
				| NUMBER '*' X '*' X '+' NUMBER '*' X '+' NUMBER					{calculEcuatieDeGrad2($1,$7,$11);}
				;

modDerivare: NUMBER							{printf("Rezultatul este: 0\n");}
			| NUMBER '*' X 					{printf("Rezultatul este: %d\n",$1);}
			| X 							{printf("Rezultatul este: 1\n");}
			| NUMBER '*' X '^' NUMBER		{printf("Rezultatul este: %d*x^%d\n",$1*$5,$5-1);}
			| SIN 							{printf("Rezultatul este: cos(x)\n");}
			| COS 							{printf("Rezultatul este: -sin(x)\n");}
			| EX 							{printf("Rezultatul este: e^x\n");}
			| LNX 							{printf("Rezultatul este: 1/x\n");}
			;											

calculTime: NUMBER SECUNDE						{convertSeconds($1);}							
 		   ;

calculComplex: Z1 '=' NUMBER '+' NUMBER '*' COMPLEX_I		{strcpy(complex1,"z1="); 
															sprintf(aComplex1,"%d",$3); a1=$3;
															strcat(complex1,aComplex1);  
														    strcat(complex1,"+");
														    sprintf(bComplex1,"%d",$5);  b1=$5;
														 	strcat(complex1,bComplex1);
														 	strcat(complex1,"*i");}

 			  | Z2 '=' NUMBER '+' NUMBER '*' COMPLEX_I		{strcpy(complex2,"z2="); 
															sprintf(aComplex2,"%d",$3); a2=$3;
															strcat(complex2,aComplex2);  
														    strcat(complex2,"+");
														    sprintf(bComplex2,"%d",$5);  b2=$5;
														 	strcat(complex2,bComplex2);
														 	strcat(complex2,"*i");}

			  | PRINT Z1 									{printf("%s\n",complex1);}
			  | PRINT Z2 									{printf("%s\n",complex2);}
			  | PRINT Z3									{printf("%s\n",complex3);}

			  | Z1 '+' Z2 									{strcpy(complex3,"z3=");
			  												sprintf(aComplex3,"%d",a1+a2);
			  												strcat(complex3,aComplex3);
			  												if((b1+b2)<0)
			  												{
			  													sprintf(bComplex3,"%d",b1+b2);
			  													strcat(complex3,bComplex3);
			  													strcat(bComplex3,"*i");
			  												}
			  												else 
			  												{
			  													sprintf(bComplex3,"%d",b1+b2);
			  													strcat(complex3,"+");
			  													strcat(complex3,bComplex3);
			  													strcat(complex3,"*i");
			  												}}

			 | Z1 SCADERE Z2									{strcpy(complex3,"z3=");
			  												sprintf(aComplex3,"%d",a1-a2);
			  												strcat(complex3,aComplex3);
			  												if((b1-b2)<0)
			  												{
			  													sprintf(bComplex3,"%d",b1-b2);
			  													strcat(complex3,bComplex3);
			  													strcat(complex3,"*i");
			  												}
			  												else 
			  												{
			  													sprintf(bComplex3,"%d",b1+b2);
			  													strcat(complex3,"+");
			  													strcat(complex3,bComplex3);
			  													strcat(complex3,"*i");
			  												}}

			| Z1 '*' Z2										{strcpy(complex3,"z3=");
			  												sprintf(aComplex3,"%d",a1*a2);
			  												strcat(complex3,aComplex3);
			  												if((b1*b2)<0)
			  												{
			  													sprintf(bComplex3,"%d",b1*b2*(-1));
			  													strcat(complex3,bComplex3);
			  												}
			  												else 
			  												{
			  													sprintf(bComplex3,"%d",b1+b2*(-1));
			  													strcat(complex3,"+");
			  													strcat(complex3,bComplex3);
			  												}}

			| REAL Z1 										{printf("Partea reala a lui %s este: %d\n",complex1, a1);}
			| REAL Z2 										{printf("Partea reala a lui %s este: %d\n",complex2, a2);}
			| IMAGINAR Z1 									{printf("Partea imaginara a lui %s este: %d\n",complex1, b1);}
			| IMAGINAR Z2 									{printf("Partea imaginara a lui %s este: %d\n",complex2, b2);}
			;
%%

void convertSeconds(int secunde)
{
	int day;
	int hour;
	int minute;

	day=secunde/(24*3600);
	secunde=secunde%(24*3600);
	hour=secunde/3600;
	secunde=secunde%3600;
	minute=secunde/60;
	secunde=secunde%60;

	printf("Zile: %d, ore: %d, minute: %d, secunde: %d\n",day,hour,minute,secunde);
}

void calculEcuatieDeGrad2(int a, int b, int c)
{
	int delta;
	delta=(b*b)-4*a*c;

	if(delta==0)
	{
		float x= -b/(float)(2*a);

		printf("Solutia este: X = %0.2f \n\n", x);
		return;
	}
	else if(delta<0)
	{
		printf("Ecuatia nu are radacini reale!\n\n");
		return;
	}
	else 
	{
		float x1=(-b+sqrt(delta))/(float)(2*a);
		float x2=(-b-sqrt(delta))/(float)(2*a);

		printf("Solutiile sunt: X1 = %0.2f si X2 = %0.2f \n\n", x1,x2);
		return;
	}
}

void calculTemperatura(int valoare, char* tip)
{
	float valoareFahrenheit;
	float valoareKelvin;
	float valoareCelsius;

	if(strcmp(tip,"Celsius")==0)
	{
		valoareFahrenheit=valoare*(9/5.0)+32;
		valoareKelvin=(float)valoare+273.15;

		printf("Temperatura convertita in Fahrenheit si in Kelvin: %0.2f F, %0.2f K\n\n",valoareFahrenheit,valoareKelvin);
	}
	else if(strcmp(tip,"Fahrenheit")==0)
	{
		valoareCelsius=(float)(valoare-32)*(5/9.0);
		valoareKelvin=(float)(valoare+459.67)*(5/9.0);

		printf("Temperatura convertita in Celsius si in Kelvin: %0.2f C, %0.2f K\n\n",valoareCelsius,valoareKelvin);
	}
	else if(strcmp(tip,"Kelvin")==0)
	{
		valoareCelsius=(float)(valoare-273.15);
		valoareFahrenheit=(float)(valoare*(5/9.0)-459.67);

		printf("Temperatura convertita in Celsius si in Fahrenheit: %0.2f C, %0.2f F\n\n",valoareCelsius,valoareFahrenheit);
	}
}

int validDate(int day, int month, int year)
{
	int valid=1;
	int bisect=0;

	if(year>=1800 && year <= 9999)
	{
		if((year%4==0 && year %100 !=0) || (year %400 ==0))
		{
			bisect=1;
		}

		if(month >=1 && month<=12)
		{
			if(month==2)
			{
				if(bisect && day==29)
				{
					valid=1;
				}
				else if(day>28)
				{
					valid=0;
				}
			}
			else if(month==4 || month==6 || month==9 || month==11)
			{
				if(day>30)
				{
					valid=0;
				}
			}
			else if(day>31)
			{
				valid=0;
			}
		}
		else 
		{
			valid=0;
		}
	}
	else 
	{
		valid=0;
	}

	return valid;
}

void calculDate(int zi1, int luna1, int an1, int zi2, int luna2, int an2)
{
	int dayDiff;
	int monDiff;
	int yearDiff;

	if(!validDate(zi1,luna1,an1))
	{
		printf("Prima data este invalida!!!!\n");
		return;
	}

	if(!validDate(zi2,luna2,an2))
	{
		printf("A doua data este invalida!!!!\n");
		return;
	}

	if(zi2<zi1)
	{
		if(luna2==3)
		{
			if((an2 %4 ==0 && an2%100!=0) || (an2%400==0))
			{
				zi2+=29;
			}
			else 
			{
				zi2+=28;
			}
		}
		else if(luna2==5 || luna2==7 || luna2==10 || luna2==12)
		{
			zi2+=30;
		}
		else 
		{
			zi2+=31;
		}

		luna2=luna2-1;
	}

	if(luna2<luna1)
	{
		luna2+=12;
		an2-=1;
	}

	dayDiff=zi2-zi1;
	monDiff=luna2-luna1;
	yearDiff=an2-an1;

	printf("Diferenta: %d years %02d months and %02d days!\n\n",yearDiff,monDiff,dayDiff);
}

float calculOperatiiMatematice(int numar1, int numar2, char operatie)
{
	float rezultat=-1;

	switch(operatie)
	{
		case '+':
		{
			rezultat=numar1+numar2;
			break;
		}
		case '-':
		{
			rezultat=numar1-numar2;
			break;
		}
		case '*':
		{
			rezultat=numar1*numar2;
			break;
		}
		case '/':
		{
			rezultat=numar1/(float)numar2;
			break;
		}
		default:
		{
			rezultat=-1;
			break;
		}
	}

	return rezultat;
}


void main() 
{
	printf("Ce se poate face cu acest calculator?\n\n");

	printf("1) Operatii matematice \n");
	printf("2) Date calculator \n");
	printf("3) Rezolva ecuatia \n");
	printf("4) Calcul Temperatura \n");
	printf("5) Time conversion \n");
	printf("6) Derivare expresii \n");
	printf("7) Calculator de numere complexe\n\n");

	complex1=(char*)malloc(sizeof(char)*200);
	complex2=(char*)malloc(sizeof(char)*200);
	complex3=(char*)malloc(sizeof(char)*200);

	aComplex1=(char*)malloc(sizeof(char)*200);
	aComplex2=(char*)malloc(sizeof(char)*200);
	aComplex3=(char*)malloc(sizeof(char)*200);

	bComplex1=(char*)malloc(sizeof(char)*200);
	bComplex2=(char*)malloc(sizeof(char)*200);
	bComplex3=(char*)malloc(sizeof(char)*200);


	
	yyparse();
}

void yyerror (char *s) {fprintf (stderr, "%s\n", s);}